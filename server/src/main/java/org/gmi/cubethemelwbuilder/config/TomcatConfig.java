package org.gmi.cubethemelwbuilder.config;

import org.apache.catalina.connector.Connector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by pablo on 12/12/14.
 */
@SpringBootApplication
public class TomcatConfig {

    private static Logger LOGGER = LoggerFactory.getLogger(TomcatConfig.class);

    private static final int HTTP_SERVER_PORT = 9999;

    @Autowired
    private ApplicationContext context;

    @Bean
    protected ServletContextListener listener() {
        return new ServletContextListener() {
            @Override
            public void contextInitialized(ServletContextEvent sce) {
                LOGGER.info("ServletContext initialized");
            }
            @Override
            public void contextDestroyed(ServletContextEvent sce) {
                LOGGER.info("ServletContext destroyed");
            }
        };
    }

    @Bean
    public EmbeddedServletContainerFactory servletContainer() {
        TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();

        //configure the default connector (http) port
        factory.addConnectorCustomizers(new TomcatConnectorCustomizer() {
            @Override
            public void customize(Connector connector) {
                connector.setPort(HTTP_SERVER_PORT);
            }
        });

        return factory;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(TomcatConfig.class, args);
    }
}