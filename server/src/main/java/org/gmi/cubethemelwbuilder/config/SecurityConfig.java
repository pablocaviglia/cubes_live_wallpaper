package org.gmi.cubethemelwbuilder.config;

import org.gmi.cubethemelwbuilder.service.ApiUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by pablo on 12/12/14.
 */
@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = SecurityConfig.class)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/resources/**", "/assets/**", "/files/**").permitAll()
                .antMatchers("/auth", "/").permitAll()
                .anyRequest().authenticated() //every request requires the user to be authenticated
                .and()
                .formLogin()
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                .defaultSuccessUrl("/user/main")
                .loginProcessingUrl("/j_spring_security_check")
                .loginPage("/auth/login")
                .failureHandler(
                        (httpServletRequest, httpServletResponse, e) -> {
                            httpServletRequest.setAttribute("error", e.getMessage());
                            httpServletRequest.getRequestDispatcher("/auth/login").forward(httpServletRequest, httpServletResponse);
                        }
                )
                .permitAll()
                .and()
                .logout()
                .permitAll();

        http.exceptionHandling().accessDeniedPage("/auth/denied");

    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new ApiUserDetailsService();
    }

    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService());
    }
}