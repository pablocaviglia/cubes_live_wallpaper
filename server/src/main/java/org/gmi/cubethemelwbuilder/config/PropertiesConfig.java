package org.gmi.cubethemelwbuilder.config;

import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

/**
 * Created by pablo on 11/04/15.
 */
@Configuration
@ComponentScan(basePackages = { "config" })
public class PropertiesConfig {

    @Bean(name = "system")
    public PropertiesFactoryBean systemProperties() {
        PropertiesFactoryBean bean = new PropertiesFactoryBean();
        bean.setLocation(new ClassPathResource("config/system.properties"));
        return bean;
    }
}
