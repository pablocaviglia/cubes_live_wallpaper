package org.gmi.cubethemelwbuilder.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSelectInfo;
import org.apache.commons.vfs2.FileSelector;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.cubethemelwbuilder.dto.ProjectDTO;
import org.gmi.cubethemelwbuilder.dto.SearchResult;
import org.gmi.cubethemelwbuilder.exception.BusinessException;
import org.gmi.cubethemelwbuilder.filter.ProjectFilter;
import org.gmi.cubethemelwbuilder.form.CreateProjectForm;
import org.gmi.cubethemelwbuilder.form.ModifyProjectForm;
import org.gmi.cubethemelwbuilder.model.Project;
import org.gmi.cubethemelwbuilder.model.ProjectStatus;
import org.gmi.cubethemelwbuilder.model.User;
import org.gmi.cubethemelwbuilder.model.UserRole;
import org.gmi.cubethemelwbuilder.service.ProjectService;
import org.gmi.cubethemelwbuilder.service.ProjectServiceImpl;
import org.gmi.cubethemelwbuilder.util.ShellClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ProjectController extends GenericController {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	public static final String ICON_NAME = "icon.png";
	
	// path to the content folder
	private @Value("#{system['PROJECT_CONTENT_PATH']}")
	String PROJECT_CONTENT_PATH;
	
	private @Value("#{system['PROJECT_GENERATION_PATH']}") 
	String PROJECT_GENERATION_PATH;
	
	private FileSelector allFileSelector = new FileSelector() {
		@Override
		public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
		@Override
		public boolean traverseDescendents(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
	};
	
    @Resource(name = "messageSource")
    private MessageSource messageSource;
    
	@Autowired
	private ProjectService projectService;
	
	@RequestMapping(value = "/project/search", method = {RequestMethod.GET, RequestMethod.POST}, produces="application/json")
	@ResponseBody
	public SearchResult<ProjectDTO> search(
			@RequestParam(required=false) Integer offset,
			@RequestParam(required=false) String name,
			@RequestParam(required=false) ProjectStatus status) {
		
		logger.debug("Getting projects...  pageNumber: " + offset + ", name: " + name + ", status: " + status);
		
		//filtro de busqueda
		ProjectFilter filter = new ProjectFilter();
		filter.setFirstResult(offset);
		filter.setName((name != null && !name.trim().equals("")) ? name : null );
		filter.setStatus(status != null ? status : null);
		
		int total = projectService.countFindProjects(filter);
		List<Project> projects = new ArrayList<Project>();
		if(total > 0) {
			projects = projectService.findProjects(filter);	
		}
		
		//creamos resultado a devolver
		SearchResult<ProjectDTO> result = new SearchResult<ProjectDTO>();
		result.setTotal(total);
		for(Project project : projects) {
			result.getElements().add(new ProjectDTO(project));	
		}
		
		return result;
	}
	
	@RequestMapping(value = "/project/list", method = { RequestMethod.GET, RequestMethod.POST })
	public String list(Model model) {
		
		User loggedUser = getLoggedUser();
		
		//if the user is admin or content 
		//manager list all the projects
		if(loggedUser.getRole() == UserRole.ADMIN || loggedUser.getRole() == UserRole.CONTENT_MANAGER) {
			List<Project> projects = projectService.findProjects(new ProjectFilter());
			model.addAttribute("projects", projects);
		}
		else {
			List<Project> projects = projectService.findProjects(new ProjectFilter(getLoggedUser()));
			model.addAttribute("projects", projects);
		}
		
		return "views/project/list";
	}
	
    @RequestMapping(value="/project/get/{id}", method = {RequestMethod.GET, RequestMethod.POST})
    public String get(@PathVariable Long id, Model model) {
    	
    	String retView = getLoginView(); //by default if not enough rights
    	
    	//get the project
    	Project project = projectService.findById(id);
    	
    	//get the logged in user
    	User loggedUser = getLoggedUser();
    	
    	//can edit the project if...
    	if(isAdmin() || 
    	   isContentManager() ||
    	   (isPublisher() && loggedUser.getId() == project.getPublisher().getId())) {
    		model.addAttribute("project", project);
    		model.addAttribute("projectForm", new ModifyProjectForm(project));
    		
			try {
	    		//check if exists icon
				FileSystemManager fsManager = VFS.getManager();
				FileObject pagePath = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + id + "/" + ICON_NAME);
				if(pagePath.exists()) {
					model.addAttribute("existsIcon", "true");
				}
				else {
					model.addAttribute("existsIcon", "false");
				}
			} 
			catch (FileSystemException e) {
				e.printStackTrace();
			}
    		
    		retView = "views/project/view";
    	}
    	
    	return retView;
    }
	
    @RequestMapping(value="/project/delete/{id}", method = {RequestMethod.GET, RequestMethod.POST})
    public String delete(@PathVariable Long id, Model model) throws BusinessException {
    	
    	String retView = getLoginView(); //by default if not enough rights
    	
    	//get the project
    	Project project = projectService.findById(id);
    	
    	//get the logged in user
    	User loggedUser = getLoggedUser();
    	
    	//can edit the project if...
    	if(isAdmin() || 
    	   isContentManager() ||
    	   (isPublisher() && loggedUser.getId() == project.getPublisher().getId())) {
    		
    		projectService.deleteProject(id);
	        retView = "redirect:/project/list";
    	}
    	
    	return retView;
    }
	
	@RequestMapping(value = "/project/modify", method = RequestMethod.POST)
    public String modify(@Valid @ModelAttribute(value="projectForm") ModifyProjectForm projectForm, BindingResult result, Model model){
		
    	String retView = getLoginView(); //by default if not enough rights
    	
    	//get the project
    	Project project = projectService.findById(projectForm.getId());
    	
    	//get the logged in user
    	User loggedUser = getLoggedUser();
    	
    	//can edit the project if...
    	if(isAdmin() || 
    	   isContentManager() ||
    	   (isPublisher() && loggedUser.getId() == project.getPublisher().getId())) {
    		
    		//set the project object again
    		model.addAttribute("project", project);
    		
    		if (result.hasErrors()) {
    			return "views/project/view";
    		} 
    		else {
    			
    			//do the modification
    			project.update(projectForm);
    			projectService.store(project);
    			
    	        return "redirect:/project/get/" + project.getId();
    		}
    	}

    	return retView;
	}
	
	@RequestMapping(value = "/project/create", method = RequestMethod.POST)
	public String create(@Valid CreateProjectForm createProjectForm, BindingResult result, ModelMap model) throws BusinessException {
		
		//validate code
		if(projectService.existsByCode(createProjectForm.getCode())) {
	        Locale locale = LocaleContextHolder.getLocale();                        
	        String projectNameAlreadyExistsErrorMessage = messageSource.getMessage("project.code.already_exists", new Object[0], locale);
			result.addError(new FieldError("createProjectForm", "code", projectNameAlreadyExistsErrorMessage));
		}
		
		if (result.hasErrors()) {
			return "views/project/create";
		}
		else {
			//create the project
			long projectId = projectService.createProject(createProjectForm.getCode(), createProjectForm.getNameEnglish(), createProjectForm.getNameSpanish(), getLoggedUser());
	        return "redirect:/project/get/" + projectId;
		}
	}
	
	@RequestMapping(value = "/project/upload_pages/{projectId}", method = RequestMethod.GET)
	public String displayUploadPages(@PathVariable Long projectId, ModelMap model) {
		
    	//get the project
    	Project project = projectService.findById(projectId);
    	model.addAttribute("project", project);
    	
		try {
	    	//delete the tmp upload folder
			FileSystemManager fsManager = VFS.getManager();
			FileObject pageUploadPath = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + projectId + "/tmp/");
			pageUploadPath.delete(allFileSelector);
		} 
		catch (FileSystemException e) {
			e.printStackTrace();
		}
    	
		return "views/project/upload_pages";
	}
	
	@RequestMapping(value = "/project/display_upload_icon_image/{projectId}", method = RequestMethod.GET)
	public String displayUploadIconImage(@PathVariable Long projectId, ModelMap model) {
		
    	//get the project
    	Project project = projectService.findById(projectId);
    	model.addAttribute("project", project);
    	
		try {
    		//check if exists icon
			FileSystemManager fsManager = VFS.getManager();
			FileObject pagePath = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + projectId + "/" + ICON_NAME);
			if(pagePath.exists()) {
				model.addAttribute("existsIcon", "true");
			}
			else {
				model.addAttribute("existsIcon", "false");
			}
		}
		catch (FileSystemException e) {
			e.printStackTrace();
		}
		
		return "views/project/upload_icon_image";
	}
	
	@RequestMapping(value = "/project/display_create", method = RequestMethod.GET)
	public String displayCreateProjectForm(ModelMap model) {
		model.addAttribute("createProjectForm", new CreateProjectForm());
		return "views/project/create";
	}
	
	@RequestMapping(value = "/project/display_generateAPK/{projectId}", method = RequestMethod.GET)
	public String displayGenerateAPK(@PathVariable Long projectId, ModelMap model) {
    	//get the project
    	Project project = projectService.findById(projectId);
    	model.addAttribute("project", project);
		return "views/project/generateAPK";
	}
	
	@RequestMapping("/api/project/uploadIcon")
	public @ResponseBody
	String uploadIcon(@RequestParam MultipartFile file,
					  @RequestParam(required = true) Long projectId, HttpServletRequest request) {

		try {

			// get the logged in user
			User loggedUser = getLoggedUser();

			// get the project
			Project project = projectService.findById(projectId);

			// can upload pages to the project if...
			if (isAdmin() || isContentManager() || 
				(isPublisher() && loggedUser.getId() == project.getPublisher().getId())) {
				
				//get the fs manager to create the tmp file
				FileSystemManager fsManager = VFS.getManager();
				FileObject tmpIconPath = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + projectId + "/tmp_icon");
				tmpIconPath.createFile();

				// read from multipart inputstream and
				// write into file output stream
				BufferedOutputStream bos = new BufferedOutputStream(tmpIconPath.getContent().getOutputStream(), 1024);
				BufferedInputStream bis = new BufferedInputStream(file.getInputStream());
				byte[] buffer = new byte[1024];
				int i = -1;
				while ((i = bis.read(buffer)) != -1) {
					bos.write(buffer, 0, i);
				}
				
				bos.flush();
				bos.close();
				bis.close();
				
				//convert the image 
				ShellClient.exec(new String[]{
						"cd " + tmpIconPath.getParent().getName().getPath(), 
						"convert -resize 512X512! " + tmpIconPath.getName().getPath() + " " + ICON_NAME,
						"rm " + tmpIconPath.getName().getPath()}, 
						null);
				
				return "{\"status\":\"File was uploaded successfuly!\"}";
			} 
			else {
				return null;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	@RequestMapping(value = "/project/getIcon/{projectId}", method = RequestMethod.GET)
	public void getIcon(@PathVariable Long projectId, 
						     HttpServletResponse response) throws IOException {
	    
		response.setContentType("image/jpeg");
		
		//get the logged in user
		User loggedUser = getLoggedUser();
		
		//get the project
		Project project = projectService.findById(projectId);
		
		//can upload pages to the project if...
		if (isAdmin() || isContentManager() || 
			(isPublisher() && loggedUser.getId() == project.getPublisher().getId())) {
			
			//get the fs manager
			FileSystemManager fsManager = VFS.getManager();
			FileObject pagePath = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + projectId + "/" + ICON_NAME);
			if(pagePath.exists()) {
				//copy the file stream into the response
			    IOUtils.copy(pagePath.getContent().getInputStream(), response.getOutputStream());
			}
		}
	}
	
	@RequestMapping(
			value = "/api/project/downloadAPK/{projectId}-{type}.apk", 
			method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public void downloadAPK(@PathVariable Long projectId,
							@PathVariable String type,
						    HttpServletResponse response) throws IOException {
	    
		//get the logged in user
		User loggedUser = getLoggedUser();
		
		//get the project
		Project project = projectService.findById(projectId);
		
		//can upload pages to the project if...
		if (isAdmin() || isContentManager() || 
			(isPublisher() && loggedUser.getId() == project.getPublisher().getId())) {
			
			//get the fs manager
			FileSystemManager fsManager = VFS.getManager();
			FileObject apkPath = fsManager.resolveFile(
					PROJECT_GENERATION_PATH + "/" + 
					project.getId() + "/" + 
					"app/build/outputs/apk/app-" + type + ".apk");
			
			if(apkPath.exists()) {
				//copy the file stream into the response
			    IOUtils.copy(apkPath.getContent().getInputStream(), response.getOutputStream());
			}
			
		    //response
            response.setContentType("application/force-download");
            response.setHeader("Content-Disposition", "attachment; filename=CubeTheme" + project.getId() + "-" + type + "_v" + project.getVersionCode()  + ".apk"); 
		    response.flushBuffer();
		}
	}
}