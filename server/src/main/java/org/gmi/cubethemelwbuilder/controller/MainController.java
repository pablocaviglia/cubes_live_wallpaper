package org.gmi.cubethemelwbuilder.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles and retrieves the common or admin page depending on the URI template.
 * A user must be log-in first he can access these pages. Only the admin can see
 * the adminpage, however.
 */
@Controller
public class MainController extends GenericController {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Handles and retrieves the common JSP page that everyone can see
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = {"/main", "/"}, method = RequestMethod.GET)
	public String getMainPage() {
		return getUserMainPage();
	}

	/**
	 * Handles and retrieves the common JSP page that everyone can see
	 *
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/user/main", method = RequestMethod.GET)
	public String getUserMainPage() {
        if(getLoggedUser() == null) {
            return getLoginView();
        }
		return "views/user/main";
	}
}