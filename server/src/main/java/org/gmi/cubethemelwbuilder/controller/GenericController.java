package org.gmi.cubethemelwbuilder.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.gmi.cubethemelwbuilder.model.User;
import org.gmi.cubethemelwbuilder.model.UserRole;
import org.gmi.cubethemelwbuilder.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public abstract class GenericController {

	@Autowired
	private UserService userService;
	
	protected boolean isAdmin() {
		return isRole(UserRole.ADMIN) ? true : false;
	}
	
	protected boolean isContentManager() {
		return isRole(UserRole.CONTENT_MANAGER) ? true : false;
	}
	
	protected boolean isPublisher() {
		return isRole(UserRole.PUBLISHER) ? true : false;
	}
	
	protected String getLoginView() {
		return "views/loginpage";
	}
	
	protected boolean isRole(UserRole userRole) {
		Object userObj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(userObj instanceof UserDetails) {
			UserDetails userDetails = (UserDetails) userObj;
			@SuppressWarnings("unchecked")
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>((Collection<GrantedAuthority>)userDetails.getAuthorities());
			if(authorities.get(0).getAuthority().equals(userRole.toString())) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	/**
	 * Get the logged in user or if no
	 * user is logged null
	 */
	protected User getLoggedUser() {
		Object userObj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(userObj instanceof UserDetails) {
			UserDetails userDetails = (UserDetails) userObj;
			User user = userService.findByUsername(userDetails.getUsername());
			return user;
		}
		else {
			return null;
		}
	}	
}