package org.gmi.cubethemelwbuilder.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSelectInfo;
import org.apache.commons.vfs2.FileSelector;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.cubethemelwbuilder.exception.BusinessException;
import org.gmi.cubethemelwbuilder.form.CreateCubeForm;
import org.gmi.cubethemelwbuilder.form.ModifyCubeForm;
import org.gmi.cubethemelwbuilder.model.Cube;
import org.gmi.cubethemelwbuilder.model.Project;
import org.gmi.cubethemelwbuilder.service.CubeService;
import org.gmi.cubethemelwbuilder.service.ProjectService;
import org.gmi.cubethemelwbuilder.util.ImageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class CubeController extends GenericController {
	
	@SuppressWarnings("unused")
	private final Logger logger = LoggerFactory.getLogger(getClass());

	// path to the content folder
	private @Value("#{system['PROJECT_CONTENT_PATH']}")
	String PROJECT_CONTENT_PATH;
	
	private FileSelector allFileSelector = new FileSelector() {
		@Override
		public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
		@Override
		public boolean traverseDescendents(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
	};
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private CubeService cubeService;
	
	@RequestMapping(value = "/cube/display_create/{projectId}", method = RequestMethod.GET)
	public String displayCreateProjectForm(ModelMap model, @PathVariable Long projectId) {
		Project project = projectService.findById(projectId);
		CreateCubeForm createCubeForm = new CreateCubeForm();
		createCubeForm.setProjectId(projectId);
		model.addAttribute("project", project);
		model.addAttribute("createCubeForm", createCubeForm);
		return "views/cube/create";
	}
	
	@RequestMapping(value = "/cube/create", method = RequestMethod.POST)
	public String create(@Valid CreateCubeForm createCubeForm, 
						 BindingResult result, 
						 ModelMap model) throws BusinessException {
		
		if (result.hasErrors()) {
			return "views/cube/create";
		}
		else {
			//create the project
			long cubeId = cubeService.createCube(createCubeForm.getProjectId(), createCubeForm.getNameEnglish(), createCubeForm.getNameSpanish());
	        return "redirect:/cube/get/" + cubeId;
		}
	}
	
	@RequestMapping(value = "/cube/modify", method = RequestMethod.POST)
    public String modify(@Valid @ModelAttribute(value="cubeForm") ModifyCubeForm cubeForm, BindingResult result, Model model){
		
		//get the project
    	Cube cube = cubeService.findById(cubeForm.getId());
    	
		//set the cube object again
		model.addAttribute("cube", cube);
		
		if (result.hasErrors()) {
			return "views/cube/view";
		} 
		else {
			
			//do the modification
			cube.update(cubeForm);
			cubeService.store(cube);
			
	        return "redirect:/cube/get/" + cube.getId();
		}
	}
	
	@RequestMapping(value = "/cube/list/{projectId}", method = { RequestMethod.GET, RequestMethod.POST })
	public String list(Model model, @PathVariable Long projectId) {
		Project project = projectService.findById(projectId);
		List<Cube> cubes = cubeService.findByProjectId(projectId);
		model.addAttribute("project", project);
		model.addAttribute("cubes", cubes);
		return "views/cube/list";
	}

	@RequestMapping(value = "/cube/get/{cubeId}", method = { RequestMethod.GET, RequestMethod.POST })
	public String get(Model model, @PathVariable Long cubeId) {
		
		Cube cube = cubeService.findById(cubeId);
		model.addAttribute("cube", cube);
		model.addAttribute("cubeForm", new ModifyCubeForm(cube));
		
		return "views/cube/view";
	}

	@RequestMapping(value = "/cube/display_images/{cubeId}/{cubeSide}", method = { RequestMethod.GET, RequestMethod.POST })
	public String displayImages(Model model, 
			@PathVariable Long cubeId,
			@PathVariable Integer cubeSide) {
		
		//default value for cube side
		if(cubeSide == null) {
			cubeSide = 0;
		}
		
		Cube cube = cubeService.findById(cubeId);
		model.addAttribute("cube", cube);
		model.addAttribute("cubeSide", cubeSide);
		
		try {
    		//check if exists icon
			FileSystemManager fsManager = VFS.getManager();
			FileObject pagePath = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + cube.getProject().getId() + "/cube" + cubeId + "_side" + cubeSide);
			if(pagePath.exists()) {
				try {
					//set flag
					model.addAttribute("existsImage", "true");
					//get image size
					ImageInfo imageInfo = new ImageInfo(pagePath);
					model.addAttribute("imageWidth", imageInfo.getWidth());
					model.addAttribute("imageHeight", imageInfo.getHeight());
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			else {
				//set flag
				model.addAttribute("existsImage", "false");
				//set null image sizes
				model.addAttribute("imageWidth", "0");
				model.addAttribute("imageHeight", "0");
			}
		} 
		catch (FileSystemException e) {
			e.printStackTrace();
		}
		
		return "views/cube/display_images";
	}
	
	@RequestMapping("/api/cube/uploadImage")
	public @ResponseBody
	String uploadImage(@RequestParam MultipartFile file,
					  @RequestParam(required = true) Long cubeId, 
					  @RequestParam(required = true) Integer cubeSide, 
					  HttpServletRequest request) {

		try {
			
			//get the cube
			Cube cube = cubeService.findById(cubeId);
			
			//get the fs manager to create the tmp file
			FileSystemManager fsManager = VFS.getManager();
			FileObject pagePath = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + cube.getProject().getId() + "/cube" + cubeId + "_side" + cubeSide);
			pagePath.createFile();

			// read from multipart inputstream and
			// write into file output stream
			BufferedOutputStream bos = new BufferedOutputStream(pagePath.getContent().getOutputStream(), 1024);
			BufferedInputStream bis = new BufferedInputStream(file.getInputStream());
			byte[] buffer = new byte[1024];
			int i = -1;
			while ((i = bis.read(buffer)) != -1) {
				bos.write(buffer, 0, i);
			}
			
			bos.flush();
			bos.close();
			bis.close();
			
			return "{\"status\":\"File was uploaded successfuly!\"}";
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = "/cube/image/{cubeId}/{cubeSide}", method = RequestMethod.GET)
	public void getCubeImage(@PathVariable Long cubeId,
							 @PathVariable Integer cubeSide, 
						     HttpServletResponse response) throws IOException {
	    
		response.setContentType("image/jpeg");
		
		//get the cube
		Cube cube = cubeService.findById(cubeId);
		
		//get the fs manager
		FileSystemManager fsManager = VFS.getManager();
		FileObject pagePath = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + cube.getProject().getId() + "/cube" + cubeId + "_side" + cubeSide);
		
		//copy the file stream into the response
	    IOUtils.copy(pagePath.getContent().getInputStream(), response.getOutputStream());
		    
	}
	
	@RequestMapping(value = "/api/cube/image/save_selection/{cubeId}/{cubeSide}", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public String imageSaveSelection(
			@PathVariable Long cubeId,
			@PathVariable Integer cubeSide,
			@RequestParam(required=true) Integer x,
			@RequestParam(required=true) Integer y,
			@RequestParam(required=true) Integer w,
			@RequestParam(required=true) Integer h) {
		
		cubeService.updateCubeSideSelection(cubeId, cubeSide, x, y, w, h);
		return "ok";
	}
}