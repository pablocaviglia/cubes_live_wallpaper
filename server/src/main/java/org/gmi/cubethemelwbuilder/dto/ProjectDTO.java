package org.gmi.cubethemelwbuilder.dto;

import java.io.Serializable;

import org.gmi.cubethemelwbuilder.model.Project;
import org.gmi.cubethemelwbuilder.model.ProjectStatus;

public class ProjectDTO implements Serializable {

	private static final long serialVersionUID = 325727590328050275L;
	
	private long id;
	private ProjectStatus status;
	
	public ProjectDTO(long id, ProjectStatus status) {
		this.id = id;
		this.status = status;
	}

	public ProjectDTO(Project project) {
		this.id = project.getId();
		this.status = project.getStatus();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ProjectStatus getStatus() {
		return status;
	}

	public void setStatus(ProjectStatus status) {
		this.status = status;
	}
}