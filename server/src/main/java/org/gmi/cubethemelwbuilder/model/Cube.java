package org.gmi.cubethemelwbuilder.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.gmi.cubethemelwbuilder.form.ModifyCubeForm;
import org.hibernate.annotations.Index;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "Cube.findByProject", 
			query = "select c from Cube c where c.project.id = :projectId"),
	@NamedQuery(name = "Cube.deleteById", 
			query = "delete from Cube c where c.id = :cubeId"),
})
@Table(name = "CUBE")
public class Cube implements Serializable {
	
	private static final long serialVersionUID = 4496345362318831889L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	@Index(name="pageIdIndex")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="PROJECT")
	@Index(name="pageProjectIndex")
	private Project project;
	
	@Column(name = "NAME_ENGLISH", nullable = false, length=512)
	private String nameEnglish;
	
	@Column(name = "NAME_SPANISH", nullable = false, length=512)
	private String nameSpanish;
	
	@Column(name = "DESC_ENGLISH", nullable = true, length=16384)
	private String descEnglish;
	
	@Column(name = "DESC_SPANISH", nullable = true, length=16384)
	private String descSpanish;

	@Column(name = "CUBE_SIDE1_SELECTION_X")
	private int cubeSide1SelectionX;
	@Column(name = "CUBE_SIDE1_SELECTION_Y")
	private int cubeSide1SelectionY;
	@Column(name = "CUBE_SIDE1_SELECTION_WIDTH")
	private int cubeSide1SelectionWidth;
	@Column(name = "CUBE_SIDE1_SELECTION_HEIGHT")
	private int cubeSide1SelectionHeight;
	
	@Column(name = "CUBE_SIDE2_SELECTION_X")
	private int cubeSide2SelectionX;
	@Column(name = "CUBE_SIDE2_SELECTION_Y")
	private int cubeSide2SelectionY;
	@Column(name = "CUBE_SIDE2_SELECTION_WIDTH")
	private int cubeSide2SelectionWidth;
	@Column(name = "CUBE_SIDE2_SELECTION_HEIGHT")
	private int cubeSide2SelectionHeight;
	
	@Column(name = "CUBE_SIDE3_SELECTION_X")
	private int cubeSide3SelectionX;
	@Column(name = "CUBE_SIDE3_SELECTION_Y")
	private int cubeSide3SelectionY;
	@Column(name = "CUBE_SIDE3_SELECTION_WIDTH")
	private int cubeSide3SelectionWidth;
	@Column(name = "CUBE_SIDE3_SELECTION_HEIGHT")
	private int cubeSide3SelectionHeight;
	
	@Column(name = "CUBE_SIDE4_SELECTION_X")
	private int cubeSide4SelectionX;
	@Column(name = "CUBE_SIDE4_SELECTION_Y")
	private int cubeSide4SelectionY;
	@Column(name = "CUBE_SIDE4_SELECTION_WIDTH")
	private int cubeSide4SelectionWidth;
	@Column(name = "CUBE_SIDE4_SELECTION_HEIGHT")
	private int cubeSide4SelectionHeight;
	
	@Column(name = "CUBE_SIDE5_SELECTION_X")
	private int cubeSide5SelectionX;
	@Column(name = "CUBE_SIDE5_SELECTION_Y")
	private int cubeSide5SelectionY;
	@Column(name = "CUBE_SIDE5_SELECTION_WIDTH")
	private int cubeSide5SelectionWidth;
	@Column(name = "CUBE_SIDE5_SELECTION_HEIGHT")
	private int cubeSide5SelectionHeight;
	
	@Column(name = "CUBE_SIDE6_SELECTION_X")
	private int cubeSide6SelectionX;
	@Column(name = "CUBE_SIDE6_SELECTION_Y")
	private int cubeSide6SelectionY;
	@Column(name = "CUBE_SIDE6_SELECTION_WIDTH")
	private int cubeSide6SelectionWidth;
	@Column(name = "CUBE_SIDE6_SELECTION_HEIGHT")
	private int cubeSide6SelectionHeight;
	
	@Column(name = "CUBE_BACKGROUND_SELECTION_X")
	private int cubeBackgroundSelectionX;
	@Column(name = "CUBE_BACKGROUND_SELECTION_Y")
	private int cubeBackgroundSelectionY;
	@Column(name = "CUBE_BACKGROUND_SELECTION_WIDTH")
	private int cubeBackgroundSelectionWidth;
	@Column(name = "CUBE_BACKGROUND_SELECTION_HEIGHT")
	private int cubeBackgroundSelectionHeight;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getNameEnglish() {
		return nameEnglish;
	}

	public void setNameEnglish(String nameEnglish) {
		this.nameEnglish = nameEnglish;
	}

	public String getNameSpanish() {
		return nameSpanish;
	}

	public void setNameSpanish(String nameSpanish) {
		this.nameSpanish = nameSpanish;
	}

	public String getDescEnglish() {
		return descEnglish;
	}

	public void setDescEnglish(String descEnglish) {
		this.descEnglish = descEnglish;
	}

	public String getDescSpanish() {
		return descSpanish;
	}

	public void setDescSpanish(String descSpanish) {
		this.descSpanish = descSpanish;
	}
	
	public int getCubeSide1SelectionX() {
		return cubeSide1SelectionX;
	}

	public void setCubeSide1SelectionX(int cubeSide1SelectionX) {
		this.cubeSide1SelectionX = cubeSide1SelectionX;
	}

	public int getCubeSide1SelectionY() {
		return cubeSide1SelectionY;
	}

	public void setCubeSide1SelectionY(int cubeSide1SelectionY) {
		this.cubeSide1SelectionY = cubeSide1SelectionY;
	}

	public int getCubeSide1SelectionWidth() {
		return cubeSide1SelectionWidth;
	}

	public void setCubeSide1SelectionWidth(int cubeSide1SelectionWidth) {
		this.cubeSide1SelectionWidth = cubeSide1SelectionWidth;
	}

	public int getCubeSide1SelectionHeight() {
		return cubeSide1SelectionHeight;
	}

	public void setCubeSide1SelectionHeight(int cubeSide1SelectionHeight) {
		this.cubeSide1SelectionHeight = cubeSide1SelectionHeight;
	}

	public int getCubeSide2SelectionX() {
		return cubeSide2SelectionX;
	}

	public void setCubeSide2SelectionX(int cubeSide2SelectionX) {
		this.cubeSide2SelectionX = cubeSide2SelectionX;
	}

	public int getCubeSide2SelectionY() {
		return cubeSide2SelectionY;
	}

	public void setCubeSide2SelectionY(int cubeSide2SelectionY) {
		this.cubeSide2SelectionY = cubeSide2SelectionY;
	}

	public int getCubeSide2SelectionWidth() {
		return cubeSide2SelectionWidth;
	}

	public void setCubeSide2SelectionWidth(int cubeSide2SelectionWidth) {
		this.cubeSide2SelectionWidth = cubeSide2SelectionWidth;
	}

	public int getCubeSide2SelectionHeight() {
		return cubeSide2SelectionHeight;
	}

	public void setCubeSide2SelectionHeight(int cubeSide2SelectionHeight) {
		this.cubeSide2SelectionHeight = cubeSide2SelectionHeight;
	}

	public int getCubeSide3SelectionX() {
		return cubeSide3SelectionX;
	}

	public void setCubeSide3SelectionX(int cubeSide3SelectionX) {
		this.cubeSide3SelectionX = cubeSide3SelectionX;
	}

	public int getCubeSide3SelectionY() {
		return cubeSide3SelectionY;
	}

	public void setCubeSide3SelectionY(int cubeSide3SelectionY) {
		this.cubeSide3SelectionY = cubeSide3SelectionY;
	}

	public int getCubeSide3SelectionWidth() {
		return cubeSide3SelectionWidth;
	}

	public void setCubeSide3SelectionWidth(int cubeSide3SelectionWidth) {
		this.cubeSide3SelectionWidth = cubeSide3SelectionWidth;
	}

	public int getCubeSide3SelectionHeight() {
		return cubeSide3SelectionHeight;
	}

	public void setCubeSide3SelectionHeight(int cubeSide3SelectionHeight) {
		this.cubeSide3SelectionHeight = cubeSide3SelectionHeight;
	}

	public int getCubeSide4SelectionX() {
		return cubeSide4SelectionX;
	}

	public void setCubeSide4SelectionX(int cubeSide4SelectionX) {
		this.cubeSide4SelectionX = cubeSide4SelectionX;
	}

	public int getCubeSide4SelectionY() {
		return cubeSide4SelectionY;
	}

	public void setCubeSide4SelectionY(int cubeSide4SelectionY) {
		this.cubeSide4SelectionY = cubeSide4SelectionY;
	}

	public int getCubeSide4SelectionWidth() {
		return cubeSide4SelectionWidth;
	}

	public void setCubeSide4SelectionWidth(int cubeSide4SelectionWidth) {
		this.cubeSide4SelectionWidth = cubeSide4SelectionWidth;
	}

	public int getCubeSide4SelectionHeight() {
		return cubeSide4SelectionHeight;
	}

	public void setCubeSide4SelectionHeight(int cubeSide4SelectionHeight) {
		this.cubeSide4SelectionHeight = cubeSide4SelectionHeight;
	}

	public int getCubeSide5SelectionX() {
		return cubeSide5SelectionX;
	}

	public void setCubeSide5SelectionX(int cubeSide5SelectionX) {
		this.cubeSide5SelectionX = cubeSide5SelectionX;
	}

	public int getCubeSide5SelectionY() {
		return cubeSide5SelectionY;
	}

	public void setCubeSide5SelectionY(int cubeSide5SelectionY) {
		this.cubeSide5SelectionY = cubeSide5SelectionY;
	}

	public int getCubeSide5SelectionWidth() {
		return cubeSide5SelectionWidth;
	}

	public void setCubeSide5SelectionWidth(int cubeSide5SelectionWidth) {
		this.cubeSide5SelectionWidth = cubeSide5SelectionWidth;
	}

	public int getCubeSide5SelectionHeight() {
		return cubeSide5SelectionHeight;
	}

	public void setCubeSide5SelectionHeight(int cubeSide5SelectionHeight) {
		this.cubeSide5SelectionHeight = cubeSide5SelectionHeight;
	}

	public int getCubeSide6SelectionX() {
		return cubeSide6SelectionX;
	}

	public void setCubeSide6SelectionX(int cubeSide6SelectionX) {
		this.cubeSide6SelectionX = cubeSide6SelectionX;
	}

	public int getCubeSide6SelectionY() {
		return cubeSide6SelectionY;
	}

	public void setCubeSide6SelectionY(int cubeSide6SelectionY) {
		this.cubeSide6SelectionY = cubeSide6SelectionY;
	}

	public int getCubeSide6SelectionWidth() {
		return cubeSide6SelectionWidth;
	}

	public void setCubeSide6SelectionWidth(int cubeSide6SelectionWidth) {
		this.cubeSide6SelectionWidth = cubeSide6SelectionWidth;
	}

	public int getCubeSide6SelectionHeight() {
		return cubeSide6SelectionHeight;
	}

	public void setCubeSide6SelectionHeight(int cubeSide6SelectionHeight) {
		this.cubeSide6SelectionHeight = cubeSide6SelectionHeight;
	}

	public int getCubeBackgroundSelectionX() {
		return cubeBackgroundSelectionX;
	}

	public void setCubeBackgroundSelectionX(int cubeBackgroundSelectionX) {
		this.cubeBackgroundSelectionX = cubeBackgroundSelectionX;
	}

	public int getCubeBackgroundSelectionY() {
		return cubeBackgroundSelectionY;
	}

	public void setCubeBackgroundSelectionY(int cubeBackgroundSelectionY) {
		this.cubeBackgroundSelectionY = cubeBackgroundSelectionY;
	}

	public int getCubeBackgroundSelectionWidth() {
		return cubeBackgroundSelectionWidth;
	}

	public void setCubeBackgroundSelectionWidth(int cubeBackgroundSelectionWidth) {
		this.cubeBackgroundSelectionWidth = cubeBackgroundSelectionWidth;
	}

	public int getCubeBackgroundSelectionHeight() {
		return cubeBackgroundSelectionHeight;
	}

	public void setCubeBackgroundSelectionHeight(int cubeBackgroundSelectionHeight) {
		this.cubeBackgroundSelectionHeight = cubeBackgroundSelectionHeight;
	}

	@Override
	public String toString() {
		return id != null ? id.toString() : "-1";
	}
	
	public void update(ModifyCubeForm cubeForm) {
		this.id = cubeForm.getId();
		this.nameEnglish = cubeForm.getNameEnglish();
		this.nameSpanish = cubeForm.getNameSpanish();
		this.descEnglish = cubeForm.getDescEnglish();
		this.descSpanish = cubeForm.getDescSpanish();
	}
}