package org.gmi.cubethemelwbuilder.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.gmi.cubethemelwbuilder.form.ModifyProjectForm;
import org.hibernate.annotations.Index;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "Project.findByCode", 
	query = "select p from Project p where p.code = :code") 
})
@Table(name = "PROJECT")
public class Project implements Serializable {
	
	private static final long serialVersionUID = 2415617745731820277L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	@Index(name="projectIdIndex")
	private Long id;
	
	@Column(name = "STATUS", nullable = false)
	@Enumerated(value = EnumType.STRING)
	private ProjectStatus status;
	
	@ManyToOne
	@JoinColumn(name="PUBLISHER", columnDefinition="")
	@Index(name="publisherIndex")
	private User publisher;
	
	/**
	 * Start with letters. No spaces.
	 * May contain uppercase or lowercase letters  
	 * ('A' through 'Z'), numbers, and underscores ('_'). 
	 */
	@Column(name = "CODE", nullable = false, unique = true)
	@Index(name="projectCodeIndex")
	private String code;
	
    @OneToMany(fetch=FetchType.LAZY, mappedBy="project")
    private List<Cube> cubes = new ArrayList<Cube>();
    
	/**
	 * Application name
	 */
	@Column(name = "NAME_ENGLISH", nullable = false, length=512)
	private String nameEnglish;
	
	@Column(name = "NAME_SPANISH", nullable = false, length=512)
	private String nameSpanish;

	@Column(name = "DESC_ENGLISH", nullable = true, length=16384)
	private String descEnglish;
	
	@Column(name = "DESC_SPANISH", nullable = true, length=16384)
	private String descSpanish;

	@Column(name = "ADMOB_BANNER_ID", nullable = true)
	private String admobBannerId;

	@Column(name = "ADMOB_INTERSTITIAL_ID", nullable = true)
	private String admobInterstitialId;
	
	/**
	 * Generated at the moment of the project creation.
	 * It is retrieved from the system configuration
	 */
	@Column(name = "ANDROID_PACKAGE", nullable = false)
	private String androidPackage;
	
	@Column(name = "VERSION_CODE", nullable = false)
	private String versionCode;

	@Column(name = "VERSION_NAME", nullable = false)
	private String versionName;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProjectStatus getStatus() {
		return status;
	}

	public void setStatus(ProjectStatus status) {
		this.status = status;
	}

	public User getPublisher() {
		return publisher;
	}

	public void setPublisher(User publisher) {
		this.publisher = publisher;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNameEnglish() {
		return nameEnglish;
	}

	public void setNameEnglish(String nameEnglish) {
		this.nameEnglish = nameEnglish;
	}

	public String getNameSpanish() {
		return nameSpanish;
	}

	public void setNameSpanish(String nameSpanish) {
		this.nameSpanish = nameSpanish;
	}

	public String getDescEnglish() {
		return descEnglish;
	}

	public void setDescEnglish(String descEnglish) {
		this.descEnglish = descEnglish;
	}

	public String getDescSpanish() {
		return descSpanish;
	}

	public void setDescSpanish(String descSpanish) {
		this.descSpanish = descSpanish;
	}

	public String getAdmobBannerId() {
		return admobBannerId;
	}

	public void setAdmobBannerId(String admobBannerId) {
		this.admobBannerId = admobBannerId;
	}

	public String getAdmobInterstitialId() {
		return admobInterstitialId;
	}

	public void setAdmobInterstitialId(String admobInterstitialId) {
		this.admobInterstitialId = admobInterstitialId;
	}

	public String getAndroidPackage() {
		return androidPackage;
	}

	public void setAndroidPackage(String androidPackage) {
		this.androidPackage = androidPackage;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	
	public List<Cube> getCubes() {
		return cubes;
	}

	public void setCubes(List<Cube> cubes) {
		this.cubes = cubes;
	}

	@Override
	public String toString() {
		return id != null ? id.toString() : "-1";
	}
	
	public void update(ModifyProjectForm projectForm) {
		this.id = projectForm.getId();
		this.nameEnglish = projectForm.getNameEnglish();
		this.nameSpanish = projectForm.getNameSpanish();
		this.descEnglish = projectForm.getDescEnglish();
		this.descSpanish = projectForm.getDescSpanish();
		this.admobBannerId = projectForm.getAdmobBannerId();
		this.versionCode = projectForm.getVersionCode();
		this.versionName = projectForm.getVersionName();
		this.admobInterstitialId = projectForm.getAdmobInterstitialId();
	}
}