package org.gmi.cubethemelwbuilder.model;

public enum ProjectStatus {
	FIRST_REGISTRATION,
	PENDING_APPROVAL,
	APPROVED;
}