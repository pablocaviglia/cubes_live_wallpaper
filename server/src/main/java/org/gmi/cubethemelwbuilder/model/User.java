package org.gmi.cubethemelwbuilder.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "User.findByName", 
				query = "select u from User u where u.name = :name") 
})
@Table(name = "USER")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	@Index(name="userIdIndex")
	private Long id;
	
	@Column(name = "ROLE", nullable = false)
	@Enumerated(value = EnumType.STRING)
	private UserRole role;
	
	@Column(name = "NAME", nullable = false, unique = true)
	@Index(name="userNameIndex")
	private String name;
	
	@Column(name = "PASSWORD", nullable = false)
	private String password;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}