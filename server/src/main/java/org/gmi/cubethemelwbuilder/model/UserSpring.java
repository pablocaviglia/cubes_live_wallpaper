package org.gmi.cubethemelwbuilder.model;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by pablo on 03/04/15.
 */
public class UserSpring extends org.springframework.security.core.userdetails.User {

    private Long id;
    private UserRole role;
    private String name;

    public UserSpring(User user, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.id = user.getId();
        this.role = user.getRole();
        this.name = user.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}