package org.gmi.cubethemelwbuilder.model;

public enum UserRole {
	ADMIN,
	CONTENT_MANAGER,
	PUBLISHER
}