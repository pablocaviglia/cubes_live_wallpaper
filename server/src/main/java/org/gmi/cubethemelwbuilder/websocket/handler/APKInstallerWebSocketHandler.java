package org.gmi.cubethemelwbuilder.websocket.handler;

import org.gmi.cubethemelwbuilder.service.ProjectService;
import org.gmi.cubethemelwbuilder.util.ShellClientListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Service
@Scope("singleton")
public class APKInstallerWebSocketHandler extends TextWebSocketHandler implements Runnable {

	public static final String HANDLE_PATH = "/installAPK";

	@Autowired
	private ProjectService projectService;

	//list of client sessions
	private List<WebSocketSession> sessions = new ArrayList<WebSocketSession>();

	//pending builds
	private LinkedList<ExecuteInstallTask> buildsTasksList = new LinkedList<ExecuteInstallTask>();
	private static Object buildsTasksLock = new Object();

	public APKInstallerWebSocketHandler() {
		new Thread(this).start();
	}
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessions.add(session);
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessions.remove(session);
	}
	
	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		synchronized (buildsTasksLock) {
			
			String payload = message.getPayload();
			String[] params = payload.split("/");
			Long projectId = Long.valueOf(params[0]);

			buildsTasksList.addLast(new ExecuteInstallTask(session.getId(), projectId));
		}
	}
	
	private class ExecuteInstallTask extends Thread {
		
		private String sessionId;
		private long projectId;

		public ExecuteInstallTask(String sessionId, long projectId) {
			this.projectId = projectId;
			this.sessionId = sessionId;
		}
		
		@Override
		public void run() {
			try {
				//generate the main code
				projectService.installAPK(projectId, new ShellClientListener() {
					@Override
					public void shellMessage(String msg) {
						try {
							System.out.println(msg);
                            if(null != msg && msg.trim().length() > 0) {
                                getSessionById(sessionId).sendMessage(new TextMessage(msg));
                            }
						}
						catch(IllegalStateException e) {
							//TODO Verificar porque pasa esto
						}
						catch (IOException e) {
							e.printStackTrace();
						}
					}
					@Override
					public void executionFinished() {
						try {
							getSessionById(sessionId).sendMessage(new TextMessage("apk_installed"));
						}
						catch(IllegalStateException e) {
							//TODO Verificar porque pasa esto
						}
						catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
			} 
			catch (Exception e) {
				e.printStackTrace();
				try {
					getSessionById(sessionId).sendMessage(new TextMessage("An unexpected system error has occurred while installing the APK, please contact the system admin. ERROR: " + e.getMessage()));
				} 
				catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	private WebSocketSession getSessionById(String sessionId) {
		for(WebSocketSession session : sessions) {
			if(session.getId().equals(sessionId)) {
				return session;
			}
		}
		return null;
	}

	@Override
	public void run() {
		while(true) {
			try {
				
				if(buildsTasksList.size() > 0) {
					Iterator<ExecuteInstallTask> buildsTasksListIt = buildsTasksList.iterator();
					while(buildsTasksListIt.hasNext()) {
						ExecuteInstallTask currentExecuteInstallTask = null;
						synchronized (buildsTasksLock) {
							currentExecuteInstallTask = buildsTasksListIt.next();
							buildsTasksListIt.remove();
						}
						currentExecuteInstallTask.start();
					}
				}
				
				//sleep
				Thread.sleep(1000);
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}