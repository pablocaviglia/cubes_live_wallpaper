package org.gmi.cubethemelwbuilder.websocket;

import org.gmi.cubethemelwbuilder.websocket.handler.APKBuilderWebSocketHandler;
import org.gmi.cubethemelwbuilder.websocket.handler.APKInstallerWebSocketHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebMvc
@EnableWebSocket
public class WebsocketConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer {
	
	@Autowired
	private APKBuilderWebSocketHandler apkBuilderWebSocketHandler;

    @Autowired
    private APKInstallerWebSocketHandler apkInstallerWebSocketHandler;
	
	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(apkBuilderWebSocketHandler, APKBuilderWebSocketHandler.HANDLE_PATH);
		registry.addHandler(apkInstallerWebSocketHandler, APKInstallerWebSocketHandler.HANDLE_PATH);
	}
	
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
}