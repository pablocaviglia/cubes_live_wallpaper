package org.gmi.cubethemelwbuilder.websocket.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.gmi.cubethemelwbuilder.service.ProjectService;
import org.gmi.cubethemelwbuilder.util.ShellClientListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Service
@Scope("singleton")
public class APKBuilderWebSocketHandler extends TextWebSocketHandler implements Runnable {
	
	public static final String HANDLE_PATH = "/generateAPK";
	
	@Autowired
	private ProjectService projectService;
	
	//list of client sessions
	private List<WebSocketSession> sessions = new ArrayList<WebSocketSession>();
	
	//pending builds
	private LinkedList<ExecuteBuildTask> buildsTasksList = new LinkedList<ExecuteBuildTask>();
	private static Object buildsTasksLock = new Object();
	
	public APKBuilderWebSocketHandler() {
		new Thread(this).start();
	}
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessions.add(session);
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessions.remove(session);
	}
	
	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		synchronized (buildsTasksLock) {
			
			String payload = message.getPayload();
			String[] params = payload.split("/");
			Long projectId = Long.valueOf(params[0]);
			String buildParameters = params.length == 2 ? params[1] : null;
			
			buildsTasksList.addLast(new ExecuteBuildTask(session.getId(), projectId, buildParameters));
		}
	}
	
	private class ExecuteBuildTask extends Thread {
		
		private String sessionId;
		private long projectId;
		private String buildParameters;
		
		public ExecuteBuildTask(String sessionId, long projectId, String buildParameters) {
			this.projectId = projectId;
			this.sessionId = sessionId;
			this.buildParameters = buildParameters;
		}
		
		@Override
		public void run() {
			try {
				//generate the main code
				projectService.buildProject(projectId, buildParameters, new ShellClientListener() {
					@Override
					public void shellMessage(String msg) {
						try {
							System.out.println(msg);
                            if(null != msg && msg.trim().length() > 0) {
                                getSessionById(sessionId).sendMessage(new TextMessage(msg));
                            }
						}
						catch(IllegalStateException e) {
							//TODO Verificar porque pasa esto
						}
						catch (IOException e) {
							e.printStackTrace();
						}
					}
					@Override
					public void executionFinished() {
						try {
							getSessionById(sessionId).sendMessage(new TextMessage("apk_generated"));
						}
						catch(IllegalStateException e) {
							//TODO Verificar porque pasa esto
						}
						catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
			} 
			catch (Exception e) {
				e.printStackTrace();
				try {
					getSessionById(sessionId).sendMessage(new TextMessage("An unexpected system error has occurred while building the project, please contact the system admin. ERROR: " + e.getMessage()));
				} 
				catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	private WebSocketSession getSessionById(String sessionId) {
		for(WebSocketSession session : sessions) {
			if(session.getId().equals(sessionId)) {
				return session;
			}
		}
		return null;
	}

	@Override
	public void run() {
		while(true) {
			try {
				
				if(buildsTasksList.size() > 0) {
					Iterator<ExecuteBuildTask> buildsTasksListIt = buildsTasksList.iterator();
					while(buildsTasksListIt.hasNext()) {
						ExecuteBuildTask currentExecuteBuildTask = null;
						synchronized (buildsTasksLock) {
							currentExecuteBuildTask = buildsTasksListIt.next();
							buildsTasksListIt.remove();
						}
						currentExecuteBuildTask.start();
					}
				}
				
				//sleep
				Thread.sleep(1000);
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}