package org.gmi.cubethemelwbuilder.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class ShellClient {
	
	public static String exec(String[] commands, final ShellClientListener listener) throws IOException, InterruptedException {

		for(String command: commands) {
			System.out.print(" " + command);
		}
		System.out.println();
		
		final StringBuffer strBuf = new StringBuffer();

		final Process proc = Runtime.getRuntime().exec("/bin/bash", null, new File("/bin"));

		PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(proc.getOutputStream())), true);
		for(int i=0; i<commands.length; i++) {
			out.println(commands[i]);
		}
		out.println("exit");
		out.close();

		try {
			
			Thread t1 = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
						String line = "";
						do {
							if(listener != null) {
								listener.shellMessage(line);
							}
							else {
								System.out.println(line);								
							}
							strBuf.append(line);
						}
						while ((line = in.readLine()) != null);
						in.close();
					} 
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			Thread t2 = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						BufferedReader inError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
						String line = "";
						do {
							if(listener != null) {
								listener.shellMessage(line);;
							}
							else {
								System.out.println(line);
							}
							strBuf.append(line);
						}
						while ((line = inError.readLine()) != null);
						inError.close();
					} 
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			
			t1.start();
			t2.start();
			
			t1.join();
			t2.join();
			
			proc.waitFor();
			proc.destroy();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		if(listener != null) {
			listener.executionFinished();
		}

		return strBuf.toString();
	}
}