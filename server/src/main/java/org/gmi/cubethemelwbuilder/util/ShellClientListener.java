package org.gmi.cubethemelwbuilder.util;

public interface ShellClientListener {

	void shellMessage(String msg);
	void executionFinished();
	
}
