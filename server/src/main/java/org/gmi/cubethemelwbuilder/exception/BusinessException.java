package org.gmi.cubethemelwbuilder.exception;

public class BusinessException extends Exception {

	private static final long serialVersionUID = -6049394567651332329L;

	public BusinessException(String msg) {
		super(msg);
	}
}