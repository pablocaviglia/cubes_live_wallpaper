package org.gmi.cubethemelwbuilder.form;

import javax.validation.constraints.Pattern;

import org.gmi.cubethemelwbuilder.model.Cube;
import org.hibernate.validator.constraints.NotEmpty;

public class ModifyCubeForm {
	
	private Long id;
	
	@NotEmpty
	@Pattern(regexp="[ A-Za-z0-9áéíóúÁÉÍÓÚ&/#._-]*") //alpha or number
	private String nameEnglish;
	
	@NotEmpty
	@Pattern(regexp="[ A-Za-z0-9áéíóúÁÉÍÓÚ&/#._-]*") //alpha or number
	private String nameSpanish;
	
	private String descEnglish;
	
	private String descSpanish;
	
	public ModifyCubeForm() {
	}
	
	public ModifyCubeForm(Cube cube) {
		this.id = cube.getId();
		this.nameEnglish = cube.getNameEnglish();
		this.nameSpanish = cube.getNameSpanish();
		this.descEnglish = cube.getDescEnglish();
		this.descSpanish = cube.getDescSpanish();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameEnglish() {
		return nameEnglish;
	}

	public void setNameEnglish(String nameEnglish) {
		this.nameEnglish = nameEnglish;
	}

	public String getNameSpanish() {
		return nameSpanish;
	}

	public void setNameSpanish(String nameSpanish) {
		this.nameSpanish = nameSpanish;
	}

	public String getDescEnglish() {
		return descEnglish;
	}

	public void setDescEnglish(String descEnglish) {
		this.descEnglish = descEnglish;
	}

	public String getDescSpanish() {
		return descSpanish;
	}

	public void setDescSpanish(String descSpanish) {
		this.descSpanish = descSpanish;
	}
}