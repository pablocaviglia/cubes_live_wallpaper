package org.gmi.cubethemelwbuilder.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

public class CreateCubeForm {
	
	@NotNull
	private Long projectId;
	
	@NotEmpty
	@Pattern(regexp="[ A-Za-z0-9áéíóúÁÉÍÓÚ&/#._-]*") //alpha or number
	private String nameEnglish;
	
	@NotEmpty
	@Pattern(regexp="[ A-Za-z0-9áéíóúÁÉÍÓÚ&/#._-]*") //alpha or number
	private String nameSpanish;
	
	public CreateCubeForm() {
		
	}
	
	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getNameEnglish() {
		return nameEnglish;
	}

	public void setNameEnglish(String nameEnglish) {
		this.nameEnglish = nameEnglish;
	}

	public String getNameSpanish() {
		return nameSpanish;
	}

	public void setNameSpanish(String nameSpanish) {
		this.nameSpanish = nameSpanish;
	}
}