package org.gmi.cubethemelwbuilder.form;

import javax.validation.constraints.Pattern;

import org.gmi.cubethemelwbuilder.model.Project;
import org.hibernate.validator.constraints.NotEmpty;

public class CreateProjectForm {

	@NotEmpty
 	@Pattern(regexp="[a-z]([a-z]|[0-9]?)*") //start with alpha then alpha or number
	private String code;

	@NotEmpty
	@Pattern(regexp="[A-Za-z0-9 ._-]*") //alpha or number
	private String nameEnglish;
	
	@NotEmpty
	@Pattern(regexp="[A-Za-z0-9 ._-]*") //alpha or number
	private String nameSpanish;
	
	public CreateProjectForm() {
		
	}
	
	public CreateProjectForm(Project project) {
		this.code = project.getCode();
		this.nameEnglish = project.getNameEnglish();
		this.nameSpanish = project.getNameSpanish();
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNameEnglish() {
		return nameEnglish;
	}

	public void setNameEnglish(String nameEnglish) {
		this.nameEnglish = nameEnglish;
	}

	public String getNameSpanish() {
		return nameSpanish;
	}

	public void setNameSpanish(String nameSpanish) {
		this.nameSpanish = nameSpanish;
	}
}