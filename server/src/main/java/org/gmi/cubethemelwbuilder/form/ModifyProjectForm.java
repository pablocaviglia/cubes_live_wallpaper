package org.gmi.cubethemelwbuilder.form;

import javax.validation.constraints.Pattern;

import org.gmi.cubethemelwbuilder.model.Project;
import org.hibernate.validator.constraints.NotEmpty;

public class ModifyProjectForm {

	private Long id;
	
	@NotEmpty
	@Pattern(regexp="[ A-Za-z0-9áéíóúÁÉÍÓÚ&/#._-]*") //alpha or number
	private String nameEnglish;

	@NotEmpty
	@Pattern(regexp="[ A-Za-z0-9áéíóúÁÉÍÓÚ&/#._-]*") //alpha or number
	private String nameSpanish;

	private String descEnglish;

	private String descSpanish;

	@Pattern(regexp="[a-z0-9/-]*") //alpha or number
	private String admobBannerId;
	
	@Pattern(regexp="[a-z0-9/-]*") //alpha or number
	private String admobInterstitialId;
	
	@NotEmpty
	@Pattern(regexp="[0-9]*") //number
	private String versionCode;

	@NotEmpty
	@Pattern(regexp="[a-z0-9 ._-]*") //alpha or number
	private String versionName;

	public ModifyProjectForm() {
	}
	
	public ModifyProjectForm(Project project) {
		this.id = project.getId();
		this.nameEnglish = project.getNameEnglish();
		this.nameSpanish = project.getNameSpanish();
		this.descEnglish = project.getDescEnglish();
		this.descSpanish = project.getDescSpanish();
		this.admobBannerId = project.getAdmobBannerId();
		this.admobInterstitialId = project.getAdmobInterstitialId();
		this.versionCode = project.getVersionCode();
		this.versionName = project.getVersionName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameEnglish() {
		return nameEnglish;
	}

	public void setNameEnglish(String nameEnglish) {
		this.nameEnglish = nameEnglish;
	}

	public String getNameSpanish() {
		return nameSpanish;
	}

	public void setNameSpanish(String nameSpanish) {
		this.nameSpanish = nameSpanish;
	}

	public String getDescEnglish() {
		return descEnglish;
	}

	public void setDescEnglish(String descEnglish) {
		this.descEnglish = descEnglish;
	}

	public String getDescSpanish() {
		return descSpanish;
	}

	public void setDescSpanish(String descSpanish) {
		this.descSpanish = descSpanish;
	}

	public String getAdmobBannerId() {
		return admobBannerId;
	}

	public void setAdmobBannerId(String admobBannerId) {
		this.admobBannerId = admobBannerId;
	}

	public String getAdmobInterstitialId() {
		return admobInterstitialId;
	}

	public void setAdmobInterstitialId(String admobInterstitialId) {
		this.admobInterstitialId = admobInterstitialId;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
}