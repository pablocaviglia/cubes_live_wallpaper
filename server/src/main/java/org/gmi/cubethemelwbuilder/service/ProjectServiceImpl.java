package org.gmi.cubethemelwbuilder.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSelectInfo;
import org.apache.commons.vfs2.FileSelector;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.cubethemelwbuilder.controller.ProjectController;
import org.gmi.cubethemelwbuilder.dao.ProjectDao;
import org.gmi.cubethemelwbuilder.exception.BusinessException;
import org.gmi.cubethemelwbuilder.filter.ProjectFilter;
import org.gmi.cubethemelwbuilder.model.Cube;
import org.gmi.cubethemelwbuilder.model.Project;
import org.gmi.cubethemelwbuilder.model.ProjectStatus;
import org.gmi.cubethemelwbuilder.model.User;
import org.gmi.cubethemelwbuilder.util.ShellClient;
import org.gmi.cubethemelwbuilder.util.ShellClientListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProjectServiceImpl implements ProjectService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	//values read from properties files
	private @Value("#{system['PROJECT_SOURCE_PATH']}") String PROJECT_SOURCE_PATH;
	private @Value("#{system['PROJECT_GENERATION_PATH']}") String PROJECT_GENERATION_PATH;
	private @Value("#{system['PROJECT_CONTENT_PATH']}") String PROJECT_CONTENT_PATH;
    private final String ANDROID_PROJECT_BASE_PACKAGE = "com.gmi.cubetheme";


	private FileSelector allFileSelector = new FileSelector() {
		@Override
		public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
		@Override
		public boolean traverseDescendents(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
	};
	
	private FileSelector allFileSelectorNotIconPng = new FileSelector() {
		@Override
		public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
			if(fileInfo.getFile().getName().getBaseName().equalsIgnoreCase(ProjectController.ICON_NAME)) {return false;}else {return true;}
		}
		@Override
		public boolean traverseDescendents(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
	};
	
	private FileSelector projectFileSelector = new FileSelector() {
		@Override
		public boolean traverseDescendents(FileSelectInfo fileInfo) throws Exception {
			return isValidFile(fileInfo);
		}
		@Override
		public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
			return isValidFile(fileInfo);
		}
		
		private boolean isValidFile(FileSelectInfo fileInfo) {
			if(fileInfo.getFile().getName().getBaseName().equals(".svn") || 
			   fileInfo.getFile().getName().getBaseName().equals("bin") ||
			   fileInfo.getFile().getName().getBaseName().equals("gen")) {
				return false;
			}
			else {
				return true;
			}
		}
	};
	
	@Autowired
	private ProjectDao projectDao;
	
	@Override
	@Transactional(readOnly = true)
	public Project findById(long projectId) {
		logger.debug("Find project by id");
		Project project = projectDao.findById(projectId);
		if(project != null) {
			//initialize needed collections
			project.getCubes().size();
		}
		return project;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Project findByIdPlain(long projectId) {
		logger.debug("Find project by id");
		Project project = projectDao.findById(projectId);
		return project;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Project> findProjects(ProjectFilter filter) {
		logger.debug("Find projects");
		return projectDao.findProjects(filter);
	}
	
	@Override
	@Transactional(readOnly = true)
	public int countFindProjects(ProjectFilter filter) {
		logger.debug("Count find projects");
		return projectDao.countFindProjects(filter);
	}
	
	@Override
	public void store(Project project) {
		projectDao.store(project);
	}

    @Override
    @Transactional(readOnly = true)
    public void installAPK(long projectId, ShellClientListener listener) throws IOException, InterruptedException, BusinessException {

        //find the project
        Project project = findByIdPlain(projectId);

        //get the fs manager
        FileSystemManager fsManager = VFS.getManager();

        //get the project build folder
        FileObject generatedProjectRoot = fsManager.resolveFile(PROJECT_GENERATION_PATH + projectId + "/");

        //execute the ant task
        ShellClient.exec(new String[]{
                "cd " + generatedProjectRoot.getName().getPath() + "/app/build/outputs/apk/ && " +
                        "adb install -r app-release.apk &&" +
                        "adb shell am start -n " + project.getAndroidPackage() + "/" + project.getAndroidPackage() + ".MainActivity"}, listener);
    }

    @Override
	@Transactional(readOnly = true)
	public void buildProject(long projectId, String buildParameters, ShellClientListener listener) throws IOException, InterruptedException, BusinessException {
		
		//find the project
		Project project = findByIdPlain(projectId);
		
		listener.shellMessage("***************** Build process for project '" + project.getNameEnglish() + "' has started " + (buildParameters != null ? " with parameters: '" + buildParameters + "'" : ""));
		
    	//get the fs manager
		FileSystemManager fsManager = VFS.getManager();
		
		//create the basic fs structure
		createProjectFilesystemStructure(project);
		
		//create and change the configuration and
		//source files related to the project itself
		//like main activity, manifest, etc
		addProjectFilesystemResourcesStructure(project, listener);
		
		//fix the build parameters
		buildParameters = buildParameters == null ? "release" : buildParameters;
		
		//get the project build folder
		FileObject generatedProjectRoot = fsManager.resolveFile(PROJECT_GENERATION_PATH + project.getId() + "/");

		//execute the ant task
		ShellClient.exec(new String[]{"cd " + generatedProjectRoot.getName().getPath() + " && gradle assembleRelease"}, listener);
		
	}

	public long createProject(String code, String nameEnglish, String nameSpanish, User publisher) throws BusinessException {
		
		long generatedProjectId = -1;
		
		//create the project object
		Project project = new Project();
		project.setCode(code);
		project.setNameEnglish(nameEnglish);
		project.setNameSpanish(nameSpanish);
		project.setStatus(ProjectStatus.FIRST_REGISTRATION);
		project.setAndroidPackage(ANDROID_PROJECT_BASE_PACKAGE + "." + code);
		project.setVersionCode("1");
		project.setVersionName("1.0");
		project.setPublisher(publisher);
		
		try {
			//store it on the database
			generatedProjectId = projectDao.store(project).getId();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
		
		return generatedProjectId;
	}
	
	/**
	 * Create the filesystem structure 
	 * of the android project
	 * @throws BusinessException
	 */
	public void createProjectFilesystemStructure(Project project) throws BusinessException {
		
		try {
			
			//get the fs manager
			FileSystemManager fsManager = VFS.getManager();
			
			//get the root folders of the project
			FileObject projectSourceRoot = fsManager.resolveFile(PROJECT_SOURCE_PATH);

			//create a destiny project folder
			FileObject generatedProjectRoot = fsManager.resolveFile(PROJECT_GENERATION_PATH + project.getId() + "/");
			generatedProjectRoot.delete(allFileSelector);
			generatedProjectRoot.createFolder();

			//copy all files
			generatedProjectRoot.copyFrom(projectSourceRoot, projectFileSelector);
		}
		catch (FileSystemException e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
	}
	
	public void addProjectFilesystemResourcesStructure(Project project, ShellClientListener listener) throws BusinessException, InterruptedException {
		
		//get the project attributes
		String projectAndroidPackage = project.getAndroidPackage();
		
		try {
			
			//get the fs manager
			FileSystemManager fsManager = VFS.getManager();
			
			//get the project folder
			FileObject generatedProjectRoot = fsManager.resolveFile(PROJECT_GENERATION_PATH + project.getId() + "/");
			
			//get the project content folder
			FileObject projectContentFolder = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + project.getId() + "/");
			
			//get the destiny resources raw folder
			FileObject generatedResourcesRAWFolder = fsManager.resolveFile(generatedProjectRoot.getName().getPath() + "/app/src/main/res/raw/");
			
			//delete content (sample data)
			generatedResourcesRAWFolder.delete(allFileSelector);
			
			//copy GFX resources to the 
			//specific project assets folder
			generatedResourcesRAWFolder.copyFrom(projectContentFolder, allFileSelectorNotIconPng);
			
			//generate the thumbnail files
			listener.shellMessage("Generating cube cropped images...");
			
			//iterate the list of cubes
			for(Cube cube : project.getCubes()) {
				
				//crop background
				listener.shellMessage("Cropping cube '" + cube.getNameEnglish() + "' background image");
				
				//get the path to the original cube background image
				FileObject cubeBackgroundImage = fsManager.resolveFile(generatedResourcesRAWFolder.getName().getPath() + "/cube" + cube.getId() + "_side0");
				FileObject cubeBackgroundImageCropped = fsManager.resolveFile(generatedResourcesRAWFolder.getName().getPath() + "/cube" + cube.getId() + "_side0_cr");

				//generate the cropped cube background image 
				ShellClient.exec(new String[]{
						"cd " + cubeBackgroundImage.getParent().getName().getPath(), 
						"convert -crop " + 
								cube.getCubeBackgroundSelectionWidth() + "x" + cube.getCubeBackgroundSelectionHeight() +
								"+" + cube.getCubeBackgroundSelectionX() + "+" + cube.getCubeBackgroundSelectionY() + " " +
								cubeBackgroundImage.getName().getBaseName() + " " + 
								cubeBackgroundImageCropped.getName().getBaseName()
						},
						null);
				
				//resize the cropped background  
				//image to the final size (1024x512)
				//generate the thumb image 
				ShellClient.exec(new String[]{
						"cd " + cubeBackgroundImage.getParent().getName().getPath(), 
						"convert -resize 1024X512! " + cubeBackgroundImageCropped.getName().getBaseName() + " " + cubeBackgroundImage.getName().getBaseName()
						},
						null);
				
				//delete the cropped image
				cubeBackgroundImageCropped.delete();
				
				for(int j=0; j<6; j++) {
					
					int cubeSideCropX = 0;
					int cubeSideCropY = 0;
					int cubeSideCropW = 0;
					int cubeSideCropH = 0;
					switch(j) {
					case 0:
						cubeSideCropX = cube.getCubeSide1SelectionX();
						cubeSideCropY = cube.getCubeSide1SelectionY();
						cubeSideCropW = cube.getCubeSide1SelectionWidth();
						cubeSideCropH = cube.getCubeSide1SelectionHeight();
						break;
					case 1:
						cubeSideCropX = cube.getCubeSide2SelectionX();
						cubeSideCropY = cube.getCubeSide2SelectionY();
						cubeSideCropW = cube.getCubeSide2SelectionWidth();
						cubeSideCropH = cube.getCubeSide2SelectionHeight();
						break;
					case 2:
						cubeSideCropX = cube.getCubeSide3SelectionX();
						cubeSideCropY = cube.getCubeSide3SelectionY();
						cubeSideCropW = cube.getCubeSide3SelectionWidth();
						cubeSideCropH = cube.getCubeSide3SelectionHeight();
						break;
					case 3:
						cubeSideCropX = cube.getCubeSide4SelectionX();
						cubeSideCropY = cube.getCubeSide4SelectionY();
						cubeSideCropW = cube.getCubeSide4SelectionWidth();
						cubeSideCropH = cube.getCubeSide4SelectionHeight();
						break;
					case 4:
						cubeSideCropX = cube.getCubeSide5SelectionX();
						cubeSideCropY = cube.getCubeSide5SelectionY();
						cubeSideCropW = cube.getCubeSide5SelectionWidth();
						cubeSideCropH = cube.getCubeSide5SelectionHeight();
						break;
					case 5:
						cubeSideCropX = cube.getCubeSide6SelectionX();
						cubeSideCropY = cube.getCubeSide6SelectionY();
						cubeSideCropW = cube.getCubeSide6SelectionWidth();
						cubeSideCropH = cube.getCubeSide6SelectionHeight();
						break;
					}
					
					int sideIndex = j+1;
					
					//crop side
					listener.shellMessage("Cropping cube side image " + (j+1));
					
					//get the path to the original cube background image
					FileObject cubeSideImage = fsManager.resolveFile(generatedResourcesRAWFolder.getName().getPath() + "/cube" + cube.getId() + "_side" + sideIndex);
					FileObject cubeSideImageCropped = fsManager.resolveFile(generatedResourcesRAWFolder.getName().getPath() + "/cube" + cube.getId() + "_side" + sideIndex + "_cr");
					
					//generate the cropped cube side image 
					ShellClient.exec(new String[]{
							"cd " + cubeBackgroundImage.getParent().getName().getPath(), 
							"convert -crop " + 
									cubeSideCropW + "x" + cubeSideCropH +
									"+" + cubeSideCropX + "+" + cubeSideCropY + " " +
									cubeSideImage.getName().getBaseName() + " " + 
									cubeSideImageCropped.getName().getBaseName()
							},
							null);
					
					//resize the cropped side  
					//image to the final size (512x512)
					//generate the thumb image 
					ShellClient.exec(new String[]{
							"cd " + cubeSideImage.getParent().getName().getPath(), 
							"convert -resize 512X512! " + cubeSideImageCropped.getName().getBaseName() + " " + cubeSideImage.getName().getBaseName()
							},
							null);
					
					//delete the cropped image
					cubeSideImageCropped.delete();
					
					
				}
			}

            {
                //get a reference to the existent
                //AndroidManifest.xml file
                FileObject generatedAndroidManifestFile = fsManager.resolveFile(generatedProjectRoot + "/app/src/main/AndroidManifest.xml");

                //get the manifest file content
                String androidManifestContent = new String(readFileContent(generatedAndroidManifestFile));

                //AndroidManifest.xml -> change package
                androidManifestContent = androidManifestContent.replaceFirst("package=\"com.gmi.cube.livewallpaper\"", "package=\"" + projectAndroidPackage + "\"");

                //save the manifest file with the new content
                createFileWithContent(generatedAndroidManifestFile, androidManifestContent.getBytes(), false);
            }

            {
                //get a reference to the existent
                //build.gradle file
                FileObject generatedBuildGradleFile = fsManager.resolveFile(generatedProjectRoot + "/app/build.gradle");

                //get the manifest file content
                String buildGradleContent = new String(readFileContent(generatedBuildGradleFile));

                //build.gradle -> change package
                buildGradleContent = buildGradleContent.replaceFirst("applicationId \"com.gmi.cube.livewallpaper\"", "applicationId \"" + projectAndroidPackage + "\"");

                //build.gradle -> change version code
                buildGradleContent = buildGradleContent.replaceFirst("versionCode 1", "versionCode " + project.getVersionCode());

                //build.gradle -> change version name
                buildGradleContent = buildGradleContent.replaceFirst("versionName \"1.0\"", "versionName \"" + project.getVersionName() + "\"");

                //save the build file with the new content
                createFileWithContent(generatedBuildGradleFile, buildGradleContent.getBytes(), false);
            }

            {
                //get a reference to the existent
                //source folder to delete it
                FileObject generatedMainActivitySourceFile = fsManager.resolveFile(generatedProjectRoot + "/app/src/main/java/");
                generatedMainActivitySourceFile.delete(allFileSelector);

                //create the 'MainActivity' source code file
                String projectAndroidPackageFolder = projectAndroidPackage.replaceAll("\\.", "/") + "/";
                FileObject mainActivitySourcePath = fsManager.resolveFile(generatedProjectRoot.getName().getPath() + "/app/src/main/java/" + projectAndroidPackageFolder + "MainActivity.java");
                createFileWithContent(mainActivitySourcePath, createMainActivityContent(project).getBytes(), false);
            }

            {
                //create the 'packages_definition.bin' file
                FileObject packagesDefinitionPath = fsManager.resolveFile(generatedProjectRoot.getName().getPath() + "/app/src/main/res/raw/packages_definition.bin");
                createFileWithContent(packagesDefinitionPath, createPackagesDefinitionContent(project).getBytes(), false);

                //create the English 'strings.xml' file
                FileObject stringsEnglishXmlPath = fsManager.resolveFile(generatedProjectRoot.getName().getPath() + "/app/src/main/res/values/strings.xml");
                createFileWithContent(stringsEnglishXmlPath, createStringXmlContent(project, true).getBytes(), false);

                //create the Spahish 'strings.xml' file
                FileObject stringsSpanishXmlPath = fsManager.resolveFile(generatedProjectRoot.getName().getPath() + "/app/src/main/res/values-es/strings.xml");
                createFileWithContent(stringsSpanishXmlPath, createStringXmlContent(project, false).getBytes(), false);

                //if exists an application icon
                //replace the default one with it
                FileObject iconProjectContentPath = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + project.getId() + "/" + ProjectController.ICON_NAME);
                if(iconProjectContentPath.exists()) {
                    //create the project icon file
                    FileObject iconSpecificProjectPath = fsManager.resolveFile(generatedProjectRoot.getName().getPath() + "/app/src/main/res/drawable/icon.png");
                    createFileWithContent(iconSpecificProjectPath, iconProjectContentPath.getContent().getInputStream(), false);
                }
            }
		}
		catch (FileSystemException e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
		catch (IOException e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
	}
	
	private String createStringXmlContent(Project project, boolean english) {
		
		String name = null;
		String description = null;
		
		if(english) {
			name = project.getNameEnglish();
			description = project.getDescEnglish();
		}
		else {
			name = project.getNameSpanish();
			description = project.getDescSpanish();
		}
		
		//escape '&' character
		name = name.replaceAll("&", "&amp;");
		if(description != null) {
			description = description.replaceAll("&", "&amp;");	
		}
		else {
			description = "";
		}
		
		String stringsXmlContent = "";
		stringsXmlContent += "<resources>\n";
		stringsXmlContent += "\t<string name=\"app_name\">" + name + "</string>\n";
		stringsXmlContent += "\t<string name=\"wallpaperDescription\">" + description + "</string>\n";
		
		for(Cube cube : project.getCubes()) {
			String cubeName = "";
			String cubeDesc = "";
			if(english) {
				cubeName = cube.getNameEnglish();	
				cubeDesc = cube.getDescEnglish();
			}
			else {
				cubeName = cube.getNameSpanish();
				cubeDesc = cube.getDescSpanish();
			}
			stringsXmlContent += "\t<string name=\"themeName"+ cube.getId() + "\">" + cubeName + "</string>\n";
			stringsXmlContent += "\t<string name=\"themeDescription"+ cube.getId() + "\">" + cubeDesc + "</string>\n";
		}
		
		stringsXmlContent += "</resources>\n";
		
		return stringsXmlContent;
	}
	
	private String createMainActivityContent(Project project) {
		
		String mainActivityContent = "";
		mainActivityContent += "package " + project.getAndroidPackage() + ";\n\n";
		mainActivityContent += "import com.gmi.cube.base.cubethemegenericlw.CubeThemeGenericLiveWallpaperActivity;\n";
		mainActivityContent += "import com.gmi.cube.base.util.AppContants;\n";
		mainActivityContent += "public class MainActivity extends CubeThemeGenericLiveWallpaperActivity {\n\n";
		mainActivityContent += "	static {\n";
		
		if(project.getAdmobBannerId() != null && !project.getAdmobBannerId().trim().equals("")) {
			mainActivityContent += "		\t\tAppContants.ADMOB_BANNER_UNIT_ID = \"" + project.getAdmobBannerId() + "\";\n";	
		}
		
		if(project.getAdmobInterstitialId() != null && !project.getAdmobInterstitialId().trim().equals("")) {
			mainActivityContent += "		\t\tAppContants.ADMOB_INTERSTITIAL_UNIT_ID = \"" + project.getAdmobInterstitialId() + "\";\n";	
		}
		
		mainActivityContent += "	}\n";
		mainActivityContent += "}\n";
		
		return mainActivityContent;
	}
	
	private String createPackagesDefinitionContent(Project project) {
		String packagesDefinitionBinContent = "";
		for(Cube cube : project.getCubes()) {
			packagesDefinitionBinContent += cube.getId() + ","; 
			packagesDefinitionBinContent += "themeName" + cube.getId() + ",";
			packagesDefinitionBinContent += "themeDescription" + cube.getId() + ",";
			packagesDefinitionBinContent += "/raw/cube" + cube.getId() + "_side0,";
			packagesDefinitionBinContent += "/raw/cube" + cube.getId() + "_side1@";
			packagesDefinitionBinContent += "/raw/cube" + cube.getId() + "_side2@";
			packagesDefinitionBinContent += "/raw/cube" + cube.getId() + "_side3@";
			packagesDefinitionBinContent += "/raw/cube" + cube.getId() + "_side4@";
			packagesDefinitionBinContent += "/raw/cube" + cube.getId() + "_side5@";
			packagesDefinitionBinContent += "/raw/cube" + cube.getId() + "_side6\n";
		}
		return packagesDefinitionBinContent;
	}
	
	private void createFileWithContent(FileObject file, InputStream content, boolean append) throws IOException {
		BufferedOutputStream bosAntProperties = new BufferedOutputStream(file.getContent().getOutputStream(append));
		
		byte[] buffer = new byte[1024];
		int i = -1;
		while((i = content.read(buffer)) != -1) {
			bosAntProperties.write(buffer, 0, i);
		}
		
		bosAntProperties.flush();
		bosAntProperties.close();
		content.close();
	}

	private void createFileWithContent(FileObject file, byte[] content, boolean append) throws IOException {
		BufferedOutputStream bosAntProperties = new BufferedOutputStream(file.getContent().getOutputStream(append));
		bosAntProperties.write(content);
		bosAntProperties.flush();
		bosAntProperties.close();
	}
	
	private byte[] readFileContent(FileObject file) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedInputStream bis = new BufferedInputStream(file.getContent().getInputStream());
		byte[] buffer = new byte[1024];
		int i = -1;
		while((i = bis.read(buffer)) != -1) {
			baos.write(buffer, 0, i);
		}
		
		return baos.toByteArray();
	}

	@Override
	public boolean existsByName(String name) {
		return projectDao.existsByName(name);
	}
	
	@Override
	public boolean existsByCode(String code) {
		return projectDao.existsByCode(code);
	}

	@Override
	public boolean existsByNameExcluding(String name, String nameToExclude) {
		return projectDao.existsByNameExcluding(name, nameToExclude);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void deleteProject(long projectId) throws BusinessException {
		
		Project project = findById(projectId);
		
		try {
			
			//get the fs manager
			FileSystemManager fsManager = VFS.getManager();

			//delete fs project resources 
			FileObject projectContentFolder = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + project.getId() + "/");
			projectContentFolder.delete(allFileSelector);
			
			//delete the project
			projectDao.delete(projectId);
		
		} 
		catch (FileSystemException e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
	}
}