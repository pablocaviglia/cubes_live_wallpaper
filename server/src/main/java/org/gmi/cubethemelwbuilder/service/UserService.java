package org.gmi.cubethemelwbuilder.service;

import org.gmi.cubethemelwbuilder.model.User;

public interface UserService {

	public User findByUsername(String username);

	void verifyInitialData();
}
