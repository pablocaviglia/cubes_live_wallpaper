package org.gmi.cubethemelwbuilder.service;

import org.gmi.cubethemelwbuilder.dao.UserDao;
import org.gmi.cubethemelwbuilder.model.User;
import org.gmi.cubethemelwbuilder.model.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao userDao;
	
	@Override
	public User findByUsername(String username) {
        try {
            return userDao.findByUsername(username);
        }
        catch(NoResultException e) {
            return null;
        }
	}

	@Override
	public void verifyInitialData() {

		User user = findByUsername("pablo");
		if(user == null) {
			user = new User();
			user.setName("pablo");
			user.setPassword("pablo");
			user.setRole(UserRole.ADMIN);
			userDao.store(user);
		}
	}
}