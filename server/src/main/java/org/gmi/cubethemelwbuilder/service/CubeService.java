package org.gmi.cubethemelwbuilder.service;

import java.util.List;

import org.gmi.cubethemelwbuilder.exception.BusinessException;
import org.gmi.cubethemelwbuilder.model.Cube;

public interface CubeService {

	Cube findById(long cubeId);
	List<Cube> findByProjectId(long projectId);
	long createCube(long projectId, String nameEnglish, String nameSpanish) throws BusinessException;
	long modifyCube(Cube cube) throws BusinessException;
	void deleteCube(long cubeId) throws BusinessException;
	void store(Cube cube);
	void updateCubeSideSelection(Long cubeId, Integer cubeSide, Integer x, Integer y, Integer w, Integer h);
	
}