package org.gmi.cubethemelwbuilder.service;

import java.util.List;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.cubethemelwbuilder.dao.CubeDao;
import org.gmi.cubethemelwbuilder.dao.ProjectDao;
import org.gmi.cubethemelwbuilder.exception.BusinessException;
import org.gmi.cubethemelwbuilder.model.Cube;
import org.gmi.cubethemelwbuilder.model.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CubeServiceImpl implements CubeService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	// path to the content folder
	private @Value("#{system['PROJECT_CONTENT_PATH']}")
	String PROJECT_CONTENT_PATH;
	
	@Autowired
	private ProjectDao projectDao;
	
	@Autowired
	private CubeDao cubeDao;
	
	@Override
	public Cube findById(long cubeId) {
		return cubeDao.findById(cubeId);
	}
	
	@Override
	public List<Cube> findByProjectId(long projectId) {
		return cubeDao.findByProjectId(projectId);
	}

	@Override
	public long createCube(long projectId, String nameEnglish, String nameSpanish) throws BusinessException {
		
		//get the project to associate
		//to the cube
		Project project = projectDao.findById(projectId);
		
		//create the cube
		Cube cube = new Cube();
		cube.setProject(project);
		cube.setNameEnglish(nameEnglish);
		cube.setNameSpanish(nameSpanish);
		
		try {
			//store it on the database
			return cubeDao.store(cube).getId();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public long modifyCube(Cube cube) throws BusinessException {
		return 0;
	}

	@Override
	public void deleteCube(long cubeId) throws BusinessException {
		Cube cube = findById(cubeId);
		cubeDao.delete(cubeId);
		try {
			//delete the image from the FS
			FileSystemManager fsManager = VFS.getManager();
			FileObject pageImagePath = fsManager.resolveFile(PROJECT_CONTENT_PATH + "/" + cube.getProject().getId() + "/" + cubeId);
			pageImagePath.delete();
		} 
		catch (FileSystemException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void store(Cube cube) {
		cubeDao.store(cube);
	}

	@Override
	public void updateCubeSideSelection(Long cubeId, Integer cubeSide, Integer x, Integer y, Integer w, Integer h) {
		
		Cube cube = findById(cubeId);
		switch(cubeSide) {
		case 0:
			cube.setCubeBackgroundSelectionX(x);
			cube.setCubeBackgroundSelectionY(y);
			cube.setCubeBackgroundSelectionWidth(w);
			cube.setCubeBackgroundSelectionHeight(h);
			break;
		case 1:
			cube.setCubeSide1SelectionX(x);
			cube.setCubeSide1SelectionY(y);
			cube.setCubeSide1SelectionWidth(w);
			cube.setCubeSide1SelectionHeight(h);
			break;
		case 2:
			cube.setCubeSide2SelectionX(x);
			cube.setCubeSide2SelectionY(y);
			cube.setCubeSide2SelectionWidth(w);
			cube.setCubeSide2SelectionHeight(h);
			break;
		case 3:
			cube.setCubeSide3SelectionX(x);
			cube.setCubeSide3SelectionY(y);
			cube.setCubeSide3SelectionWidth(w);
			cube.setCubeSide3SelectionHeight(h);
			break;
		case 4:
			cube.setCubeSide4SelectionX(x);
			cube.setCubeSide4SelectionY(y);
			cube.setCubeSide4SelectionWidth(w);
			cube.setCubeSide4SelectionHeight(h);
			break;
		case 5:
			cube.setCubeSide5SelectionX(x);
			cube.setCubeSide5SelectionY(y);
			cube.setCubeSide5SelectionWidth(w);
			cube.setCubeSide5SelectionHeight(h);
			break;
		case 6:
			cube.setCubeSide6SelectionX(x);
			cube.setCubeSide6SelectionY(y);
			cube.setCubeSide6SelectionWidth(w);
			cube.setCubeSide6SelectionHeight(h);
			break;
		}
		
		cubeDao.store(cube);
	}
}