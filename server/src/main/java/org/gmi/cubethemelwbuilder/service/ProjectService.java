package org.gmi.cubethemelwbuilder.service;

import java.io.IOException;
import java.util.List;

import org.gmi.cubethemelwbuilder.exception.BusinessException;
import org.gmi.cubethemelwbuilder.filter.ProjectFilter;
import org.gmi.cubethemelwbuilder.model.Project;
import org.gmi.cubethemelwbuilder.model.User;
import org.gmi.cubethemelwbuilder.util.ShellClientListener;
import org.springframework.transaction.annotation.Transactional;

public interface ProjectService {

	Project findById(long projectId);
	Project findByIdPlain(long projectId);

	List<Project> findProjects(ProjectFilter filter);

	int countFindProjects(ProjectFilter filter);

	@Transactional(readOnly = true)
	void installAPK(long projectId, ShellClientListener listener) throws IOException, InterruptedException, BusinessException;

	void buildProject(long projectId, String buildParameters, ShellClientListener listener) throws IOException, InterruptedException, BusinessException;
	long createProject(String code, String nameEnglish, String nameSpanish, User publisher) throws BusinessException;
	
	void deleteProject(long projectId) throws BusinessException;
	
	void store(Project project);
	boolean existsByName(String name);
	boolean existsByNameExcluding(String name, String nameToExclude);
	boolean existsByCode(String code);
	
}