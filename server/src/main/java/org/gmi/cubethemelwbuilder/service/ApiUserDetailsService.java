package org.gmi.cubethemelwbuilder.service;

import org.gmi.cubethemelwbuilder.dao.UserDao;
import org.gmi.cubethemelwbuilder.model.User;
import org.gmi.cubethemelwbuilder.model.UserSpring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 12/12/14.
 */
public class ApiUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("You have entered an invalid username or password!");
        } else {
            List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
            roles.add(new SimpleGrantedAuthority(user.getRole().name()));
            //transform it into Spring Security model
            UserDetails userDetails = new UserSpring(user, username, user.getPassword(), roles);
            return userDetails;
        }
    }
}