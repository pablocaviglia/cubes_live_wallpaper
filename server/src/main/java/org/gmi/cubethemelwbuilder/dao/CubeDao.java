package org.gmi.cubethemelwbuilder.dao;

import java.util.List;

import org.gmi.cubethemelwbuilder.model.Cube;

public interface CubeDao extends GenericDao<Cube> {

	boolean existsByName(String name);
	boolean existsByNameExcluding(String name, String nameToExclude);
	List<Cube> findByProjectId(long projectId);

}