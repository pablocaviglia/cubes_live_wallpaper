package org.gmi.cubethemelwbuilder.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.gmi.cubethemelwbuilder.filter.ProjectFilter;
import org.gmi.cubethemelwbuilder.model.Project;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectDaoImpl extends GenericDaoImpl<Project> implements ProjectDao {
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Project> findProjects(ProjectFilter filter) {
		logger.debug("Find projects");
		Query query = buildQueryfindProjects(filter, false);
		if (filter.getFirstResult() != null) {
			query.setFirstResult(filter.getFirstResult());
		}
		if (filter.getMaxResults() != null) {
			query.setMaxResults(filter.getMaxResults());
		}

		return query.getResultList();
	}
	
	@Override
	public int countFindProjects(ProjectFilter filter) {
		logger.debug("Count find projects");
		Query query = buildQueryfindProjects(filter, true);
		long result = (Long) query.getSingleResult();
		return (int) result;
	}
	
	/**
	 * This method build the findWallpaper query.
	 * 
	 * @param filter
	 *            Is a pojo with the fields to filter the Wallpapers
	 * @param count
	 *            Is a boolean to indicate if the query should return the count,
	 *            or the elements
	 * @return Query
	 */
	private Query buildQueryfindProjects(ProjectFilter filter, boolean count) {
		
		StringBuilder sb = new StringBuilder();
		
		//status
		if (filter.getStatus() != null) {
			sb.append(" where p.status = :status");
		}
		
		//name
		if (filter.getName() != null) {
			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}
			sb.append(" p.name = :name ");
		}

		//publisher
		if (filter.getPublisher() != null) {
			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}
			sb.append(" p.publisher.id = :publisherId ");
		}

		sb.insert(0, " from " + Project.class.getName() + " p ");
		
		if (count) {
			sb.insert(0, " select count(p) as qntyProjects ");
		}

		Query query = entityManager.createQuery(sb.toString());
		
		//status
		if (filter.getStatus() != null) {
			query.setParameter("status", filter.getStatus());
		}
		
		//name
		if (filter.getName() != null) {
			query.setParameter("name", filter.getName());
		}
		
		//publisher
		if (filter.getPublisher() != null) {
			query.setParameter("publisherId", filter.getPublisher().getId());
		}		
		
		return query;
	}
	
	@Override
	public boolean existsByName(String name) {
		
		//get the user from the database
		Query query = entityManager.createQuery("select p.name from Project p where p.name = :name");
		query.setParameter("name", name);
		
		try {
			query.getSingleResult();
			return true;
		} 
		catch (NoResultException e) {
			return false;
		}
	}
	
	@Override
	public boolean existsByCode(String code) {
		
		//get the user from the database
		Query query = entityManager.createQuery("select p.code from Project p where p.code = :code");
		query.setParameter("code", code);
		
		try {
			query.getSingleResult();
			return true;
		} 
		catch (NoResultException e) {
			return false;
		}
	}

	@Override
	public boolean existsByNameExcluding(String name, String nameToExclude) {
		
		//get the user from the database
		Query query = entityManager.createQuery("select p.name from Project p where p.name = :name and p.name != :nameToExclude");
		query.setParameter("name", name);
		query.setParameter("nameToExclude", nameToExclude);
		
		try {
			query.getSingleResult();
			return true;
		} 
		catch (NoResultException e) {
			return false;
		}
	}
}