package org.gmi.cubethemelwbuilder.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.gmi.cubethemelwbuilder.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		//get the user from the database
		TypedQuery<User> namedQuery = entityManager.createNamedQuery("User.findByName", User.class);
		namedQuery.setParameter("name", username);
		User user = namedQuery.getSingleResult();
		
		List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
		roles.add(new SimpleGrantedAuthority(user.getRole().toString()));
		
		//transform it into Spring Security model
		UserDetails userDetails = new org.springframework.security.core.userdetails.User(username, user.getPassword(), roles);
		
		return userDetails;
	}

	@Override
	public User findByUsername(String username) {
		
		//get the user from the database
		TypedQuery<User> namedQuery = entityManager.createNamedQuery("User.findByName", User.class);
		namedQuery.setParameter("name", username);
		
		try {
			User user = namedQuery.getSingleResult();
			return user;
		} 
		catch (NoResultException e) {
			return null;
		}
	}
}