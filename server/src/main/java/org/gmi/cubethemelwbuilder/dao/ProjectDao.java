package org.gmi.cubethemelwbuilder.dao;

import java.util.List;

import org.gmi.cubethemelwbuilder.filter.ProjectFilter;
import org.gmi.cubethemelwbuilder.model.Project;

public interface ProjectDao extends GenericDao<Project> {

	List<Project> findProjects(ProjectFilter filter);

	int countFindProjects(ProjectFilter filter);
	
	boolean existsByName(String name);
	boolean existsByNameExcluding(String name, String nameToExclude);
	boolean existsByCode(String code);
	
}