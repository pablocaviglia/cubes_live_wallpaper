package org.gmi.cubethemelwbuilder.dao;

import org.gmi.cubethemelwbuilder.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserDao extends GenericDao<User>, UserDetailsService {

	public User findByUsername(String username);
	
}