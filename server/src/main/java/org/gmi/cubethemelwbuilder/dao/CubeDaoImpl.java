package org.gmi.cubethemelwbuilder.dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.gmi.cubethemelwbuilder.model.Cube;
import org.springframework.stereotype.Repository;

@Repository
public class CubeDaoImpl extends GenericDaoImpl<Cube> implements CubeDao {
	
	@Override
	public boolean existsByName(String name) {
		
		//get the user from the database
		Query query = entityManager.createQuery("select c.name from Cube c where c.name = :name");
		query.setParameter("name", name);
		
		try {
			query.getSingleResult();
			return true;
		} 
		catch (NoResultException e) {
			return false;
		}
	}
	
	@Override
	public boolean existsByNameExcluding(String name, String nameToExclude) {
		
		//get the user from the database
		Query query = entityManager.createQuery("select c.name from Cube c where c.name = :name and c.name != :nameToExclude");
		query.setParameter("name", name);
		query.setParameter("nameToExclude", nameToExclude);
		
		try {
			query.getSingleResult();
			return true;
		} 
		catch (NoResultException e) {
			return false;
		}
	}

	@Override
	public List<Cube> findByProjectId(long projectId) {
		try {
			//get the user from the database
			TypedQuery<Cube> namedQuery = entityManager.createNamedQuery("Cube.findByProject", Cube.class);
			namedQuery.setParameter("projectId", projectId);
			return namedQuery.getResultList();
		} 
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}