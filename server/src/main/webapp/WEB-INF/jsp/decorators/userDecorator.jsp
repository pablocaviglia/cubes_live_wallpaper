<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
<title><sitemesh:write property='title' /></title>
<sitemesh:write property='head' />
<style type="text/css">
/* Reset body padding and margins */
body { margin:0; padding:0; }
 
/* Make Header Sticky */
header #header_container { line-height:60px; margin:0 auto; width:940px; text-align:center; }
header { background:#eee; border:1px solid #666; height:60px; left:0; position:fixed; width:100%; top:0; z-index: 2; }

.right{ float:right; }
.left{ float:left; }

/* CSS for the content of page. I am giving top and bottom padding of 80px to make sure the header and footer do not overlap the content.*/
#container { margin:0 auto; overflow:auto; padding:80px 0;}
#content { }
 
/* Make Footer Sticky */
footer #footer_container { line-height:60px; margin:0 auto; width:940px; text-align:center; }
footer { background:#eee; border:1px solid #666; bottom:0; height:60px; left:0; position:fixed; width:100%; z-index: 2;}

.error { color: #ff0000; }
.errorblock { color: #000; background-color: #ffEEEE; border: 3px solid #ff0000; padding: 8px; margin: 16px; }

</style>
</head>
<body>

<header>
	<div id="header_container">
 		<a href="<%=request.getContextPath()%>/main">MAIN MENU</a>
		<div style="right: 50px; top: 0; position: absolute;">
			<a class="right" style="margin-left: 5px" href="<%=request.getContextPath()%>/auth/logout"> Logout </a>
		    <a class="right">(<sec:authentication property="principal.username"/>)</a>
		</div>
	</div>
</header>

<!-- BEGIN: Page Content -->
<div id="container">
    <div id="content">
       <sitemesh:write property='body' />
    </div>
</div>
<!-- END: Page Content -->

<footer>
	<div id="footer_container">
        Footer Content
	</div>
</footer> 

</body>
</html>