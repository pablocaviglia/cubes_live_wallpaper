<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>
	<script>
	function deleteCube(id, name) {
		var confirmMsg = "Are you sure you want to delete cube '" + name + "'?";
		if(confirm(confirmMsg)) {
			window.location = "${pageContext.request.contextPath}/cube/delete/" + id;
		}
	}
	</script>
</head>

<body>
	<h2 style="text-align: center;">project '<a href="${pageContext.request.contextPath}/project/get/${project.id}">${project.nameEnglish}</a>' / cubes </h2>
	<table style="margin: auto;">
		<tr style="font-weight: bolder;">
			<td>
				Name
			</td>
			<td align="right" width="100px">
				Delete
			</td>
		</tr>
		<c:forEach var="cube" items="${cubes}">
			<tr>
				<td>
					<a href="${pageContext.request.contextPath}/cube/get/${cube.id}"><c:out value="${cube.nameEnglish}"></c:out></a>
				</td>
				<td align="right">
					<a href="javascript:deleteCube(${cube.id}, '${cube.nameEnglish}')"><img src="${pageContext.request.contextPath}/resources/img/delete.jpg" style="width: 18px; height: 18px" ></a>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
