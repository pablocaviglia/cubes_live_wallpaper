<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<body>
	<h2 style="text-align: center;">project '<a href="${pageContext.request.contextPath}/project/get/${project.id}">${project.nameEnglish}</a>' / create cube </h2>
 	<form:form method="POST" modelAttribute="createCubeForm" action="${pageContext.request.contextPath}/cube/create">
		<table style="margin: auto;">
			<tr valign="top">
				<form:hidden path="projectId" />
				<td>Name (English):</td>
				<td><form:input path="nameEnglish" /></td>
				<td><form:errors path="nameEnglish" cssClass="error" /></td>
			</tr>
			<tr valign="top">
				<td>Name (Spanish):</td>
				<td><form:input path="nameSpanish" /></td>
				<td><form:errors path="nameSpanish" cssClass="error" /></td>
			</tr>
			<tr>
				<td colspan="3" align="center"><input type="submit" /></td>
			</tr>
		</table>
	</form:form>
</body>