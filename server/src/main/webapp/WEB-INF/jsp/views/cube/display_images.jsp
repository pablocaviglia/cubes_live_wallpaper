<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/styles.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/img_upload_dropbox.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/imgareaselect-animated.css" />

<script>
var uploadUrl = '${pageContext.request.contextPath}/api/cube/uploadImage?cubeId=${cube.id}&cubeSide=${cubeSide}';
var redirectUrl = '${pageContext.request.contextPath}/cube/display_images/${cube.id}/${cubeSide}';
var uploadMaxFiles = 1;
var uploadMaxFileSize = 5; //mb
</script>

<script src="${pageContext.request.contextPath}/resources/js/jquery-1.9.1.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.imgareaselect.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.filedrop.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/upload_single_page.js"></script>

<script>

	var originalAspectRatio;
	var imageOriginalWidth = ${imageWidth};
	var imageOriginalHeight = ${imageHeight};
	
	var originalSelectionX = 0;
	var originalSelectionY = 0;
	var originalSelectionW = 0;
	var originalSelectionH = 0;
	
	<c:choose>
		<c:when test="${cubeSide eq 0}">originalSelectionX = ${cube.cubeBackgroundSelectionX};</c:when>
		<c:when test="${cubeSide eq 1}">originalSelectionX = ${cube.cubeSide1SelectionX};</c:when>
		<c:when test="${cubeSide eq 2}">originalSelectionX = ${cube.cubeSide2SelectionX};</c:when>
		<c:when test="${cubeSide eq 3}">originalSelectionX = ${cube.cubeSide3SelectionX};</c:when>
		<c:when test="${cubeSide eq 4}">originalSelectionX = ${cube.cubeSide4SelectionX};</c:when>
		<c:when test="${cubeSide eq 5}">originalSelectionX = ${cube.cubeSide5SelectionX};</c:when>
		<c:when test="${cubeSide eq 6}">originalSelectionX = ${cube.cubeSide6SelectionX};</c:when>
	</c:choose>
	
	<c:choose>
		<c:when test="${cubeSide eq 0}">originalSelectionY = ${cube.cubeBackgroundSelectionY};</c:when>
		<c:when test="${cubeSide eq 1}">originalSelectionY = ${cube.cubeSide1SelectionY};</c:when>
		<c:when test="${cubeSide eq 2}">originalSelectionY = ${cube.cubeSide2SelectionY};</c:when>
		<c:when test="${cubeSide eq 3}">originalSelectionY = ${cube.cubeSide3SelectionY};</c:when>
		<c:when test="${cubeSide eq 4}">originalSelectionY = ${cube.cubeSide4SelectionY};</c:when>
		<c:when test="${cubeSide eq 5}">originalSelectionY = ${cube.cubeSide5SelectionY};</c:when>
		<c:when test="${cubeSide eq 6}">originalSelectionY = ${cube.cubeSide6SelectionY};</c:when>
	</c:choose> 
	
	<c:choose>
		<c:when test="${cubeSide eq 0}">originalSelectionW = ${cube.cubeBackgroundSelectionWidth};</c:when>
		<c:when test="${cubeSide eq 1}">originalSelectionW = ${cube.cubeSide1SelectionWidth};</c:when>
		<c:when test="${cubeSide eq 2}">originalSelectionW = ${cube.cubeSide2SelectionWidth};</c:when>
		<c:when test="${cubeSide eq 3}">originalSelectionW = ${cube.cubeSide3SelectionWidth};</c:when>
		<c:when test="${cubeSide eq 4}">originalSelectionW = ${cube.cubeSide4SelectionWidth};</c:when>
		<c:when test="${cubeSide eq 5}">originalSelectionW = ${cube.cubeSide5SelectionWidth};</c:when>
		<c:when test="${cubeSide eq 6}">originalSelectionW = ${cube.cubeSide6SelectionWidth};</c:when>
	</c:choose>

	<c:choose>
		<c:when test="${cubeSide eq 0}">originalSelectionH = ${cube.cubeBackgroundSelectionHeight};</c:when>
		<c:when test="${cubeSide eq 1}">originalSelectionH = ${cube.cubeSide1SelectionHeight};</c:when>
		<c:when test="${cubeSide eq 2}">originalSelectionH = ${cube.cubeSide2SelectionHeight};</c:when>
		<c:when test="${cubeSide eq 3}">originalSelectionH = ${cube.cubeSide3SelectionHeight};</c:when>
		<c:when test="${cubeSide eq 4}">originalSelectionH = ${cube.cubeSide4SelectionHeight};</c:when>
		<c:when test="${cubeSide eq 5}">originalSelectionH = ${cube.cubeSide5SelectionHeight};</c:when>
		<c:when test="${cubeSide eq 6}">originalSelectionH = ${cube.cubeSide6SelectionHeight};</c:when>
	</c:choose>
	
	$(function() {
		
		//disable the save button
		$("#saveChangesButton").prop("disabled", true);
		$('#saveChangesLoadingImg').hide();
		
		$('#xSelection').html(originalSelectionX);
		$('#ySelection').html(originalSelectionY);
		$('#wSelection').html(originalSelectionW);
		$('#hSelection').html(originalSelectionH);
		
		<c:choose>
			<c:when test="${cubeSide eq 0}">
			originalAspectRatio = '2:1';
			</c:when>
			<c:otherwise>
			originalAspectRatio = '1:1';
			</c:otherwise>
		</c:choose>
		
		if(originalSelectionW > 0 && originalSelectionH > 0) {
			$('#cubeImage').imgAreaSelect({
				aspectRatio: originalAspectRatio, 
				handles: true,
				fadeSpeed: 200,
				onSelectEnd: setCubeImageSelection,
				imageWidth: imageOriginalWidth,
				imageHeight: imageOriginalHeight,
				x1: originalSelectionX, 
			  	y1: originalSelectionY, 
			  	x2: originalSelectionX + originalSelectionW, 
			  	y2: originalSelectionY + originalSelectionH 
			});
		}
		else {
			$('#cubeImage').imgAreaSelect({
				aspectRatio: originalAspectRatio, 
				handles: true,
				fadeSpeed: 200,
				onSelectEnd: setCubeImageSelection,
				imageWidth: imageOriginalWidth,
				imageHeight: imageOriginalHeight 
			});
		}
	});
	
	function saveChanges() {
		
		//disable the save button
		$("#saveChangesButton").prop("disabled", true);
		$('#saveChangesLoadingImg').fadeIn();

		$.ajax({
			type : "POST",
			url : "${pageContext.request.contextPath}/api/cube/image/save_selection/${cube.id}/${cubeSide}",
	        data: "x=" + currentSelectionX + "&y=" +currentSelectionY + "&w=" + currentSelectionW + "&h=" + currentSelectionH,
			success : function(response) {
				$('#saveChangesLoadingImg').fadeOut();
			},
			error : function(e) {
				console.log('Error: ' + e);
				$('#saveChangesLoadingImg').fadeOut();
			}
		});
	}
	
	var currentSelectionX;
	var currentSelectionY;
	var currentSelectionW;
	var currentSelectionH;
		
	function setCubeImageSelection(img, selection) {
		
		//enable the save button
		$("#saveChangesButton").prop("disabled", false);
		
		currentSelectionX = selection.x1;
		currentSelectionY = selection.y1;
		currentSelectionW = selection.width;
		currentSelectionH = selection.height;
	
		$('#xSelection').html(currentSelectionX);
		$('#ySelection').html(currentSelectionY);
		$('#wSelection').html(currentSelectionW);
		$('#hSelection').html(currentSelectionH);
		
	}
	
	function changeAspectRatioSelection() {
		
		var newAspectRatio= '';
		var isChecked = $('#freeAspectRatioCheckbox:checked').val()?true:false;
		if(!isChecked) {
			newAspectRatio = originalAspectRatio;
		}
		
		$('#cubeImage').imgAreaSelect({
			aspectRatio: newAspectRatio, 
			handles: true,
			fadeSpeed: 200,
			onSelectEnd: setCubeImageSelection,
			imageWidth: imageOriginalWidth,
			imageHeight: imageOriginalHeight
		});
	}
	
</script>

<h2 style="text-align: center;">project '<a href="${pageContext.request.contextPath}/project/get/${cube.project.id}">${cube.project.nameEnglish}</a>' / <a href="${pageContext.request.contextPath}/cube/list/${cube.project.id}">cubes</a> / ${cube.nameEnglish} </h2>
<p>
<table style="margin: auto;">
	<tr>
		<td align="center">
			<c:forEach begin="0" end="6" var="currentSideStep">
				<c:choose>
					<c:when test="${cubeSide ne currentSideStep}">
						<a style="font-size: large;" href="${pageContext.request.contextPath}/cube/display_images/${cube.id}/${currentSideStep}">
							<c:choose>
								<c:when test="${currentSideStep > 0}">Side ${currentSideStep}</c:when>
								<c:otherwise>Background</c:otherwise>
							</c:choose>
						</a>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${currentSideStep > 0}">
								(SIDE ${currentSideStep})
							</c:when>
							<c:otherwise>
								(BACKGROUND)
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
				&nbsp;&nbsp;
			</c:forEach>
		</td>
	</tr>
</table>
<p>
<table style="margin: auto;">
	<tr>
		<td align="center" valign="top">
			<table style="margin: auto;">
				<tr>
					<td align="center">
						<div id="imgUploadDropbox" style="width:500px; margin: auto; text-align: center;">
							<c:choose>
								<c:when test="${existsImage}">
									<div class="preview">
										<span class="imageHolder">
											<img width="500px" id="cubeImage" src="${pageContext.request.contextPath}/cube/image/${cube.id}/${cubeSide}">
										</span>
									</div>
								</c:when>
								<c:otherwise>
									<span class="message" style="margin: 50% 50% 50% 50%; text-align: center; font-size: 24px; ">Drop image here to upload.</span>
								</c:otherwise>								
							</c:choose>
						</div>
					</td>
				</tr>
			</table>
		</td>
		<td valign="top">
			<div style="float: left; width: 100%; margin-left: 5px">
			    <p style="font-size: 110%; font-weight: bold; text-align: left;">
			      CUBE DATA
			    </p>
			</div>
			<table>
				<tr>
					<td>
						Free aspect ratio: <input type="checkbox" id="freeAspectRatioCheckbox" onchange="javascript:changeAspectRatioSelection()">
					</td>
				</tr>
			</table>
			<table>
				<tr>
				<td>X:</td>
				<td> 
				<div id="xSelection"></div>
				</td>
				</tr>

				<tr>
				<td>Y:</td>
				<td> 
				<div id="ySelection"> 
				</div>
				</td>
				</tr>

				<tr>
				<td>W:</td>
				<td> 
				<div id="wSelection"></div>
				</td>
				</tr>

				<tr>
				<td>H:</td>
				<td> 
				<div id="hSelection"></div>
				</td>
				</tr>
			</table>
			<table>
				<tr>
					<td>
						<input type="button" id="saveChangesButton" value="Save changes" onclick="javascript:saveChanges()">
						<img height="24px" id="saveChangesLoadingImg" src="${pageContext.request.contextPath}/resources/img/ajax-loader.gif">
					</td>
				</tr>
			</table>
			</div>
  		</td>	
	</tr>
</table>
