<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<head>
	<script src="${pageContext.request.contextPath}/resources/js/jquery-1.9.0.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/tooltip.js"></script>
    <link href="${pageContext.request.contextPath}/resources/css/tooltip.css" rel="stylesheet" type="text/css" />
</head>

<form:form method="POST" modelAttribute="cubeForm" action="${pageContext.request.contextPath}/cube/modify">

<h2 style="text-align: center;">project '<a href="${pageContext.request.contextPath}/project/get/${cube.project.id}">${cube.project.nameEnglish}</a>' / cube '${cube.nameEnglish}' </h2>
<p>

<table style="margin: auto;">
	<tr>
		<td valign="top">
			<table style="margin: auto;">
				<tr>
					<td style="font-weight: bolder;"> Name (English) </td>
					<td>
						<form:hidden path="id" />
						<form:input path="nameEnglish" size="36" />
					</td>
					<td>
						<img onmouseover="tooltip.pop(this, '#nameEnglish_tip')" src="${pageContext.request.contextPath}/resources/img/help.jpg" width="32">
				        <div style="display:none;">
				            <div id="nameEnglish_tip">
				            	<table>
				            		<tr>
				            			<td valign="top">
				            				<b>Name (English)</b><br /><br />
				                			The name of the cube as it appears on application themes menu list.
				            			</td>
				            			<td valign="top">
				            				<img style="width: 250px" src="${pageContext.request.contextPath}/resources/img/tooltip/tooltip_name.png" style="float:right;margin-left:12px;" alt="" />
				            			</td>
				            		</tr>
				            	</table>
				            </div>
				        </div>
					</td>
					</td>
					<td>
						<form:errors path="nameEnglish" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;"> Name (Spanish) </td>
					<td>
						<form:input path="nameSpanish" size="36" />
					</td>
					<td>
						<img onmouseover="tooltip.pop(this, '#nameSpanish_tip')" src="${pageContext.request.contextPath}/resources/img/help.jpg" width="32">
				        <div style="display:none;">
				            <div id="nameSpanish_tip">
				            	<table>
				            		<tr>
				            			<td valign="top">
							                <b>Name (Spanish)</b><br /><br />
							                The name of the cube as it appears on application themes menu list.
				            			</td>
				            			<td valign="top">
							                <img style="width: 250px" src="${pageContext.request.contextPath}/resources/img/tooltip/tooltip_name.png" style="float:right;margin-left:12px;" alt="" />
				            			</td>
				            		</tr>
				            	</table>
				            </div>
				        </div>
					</td>
					<td>
						<form:errors path="nameSpanish" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;" valign="top"> Description (English) </td>
					<td>
						<form:textarea path="descEnglish" cols="51" rows="5" />
					</td>
					<td>
						<img onmouseover="tooltip.pop(this, '#descEnglish_tip')" src="${pageContext.request.contextPath}/resources/img/help.jpg" width="32">
				        <div style="display:none;">
				            <div id="descEnglish_tip">
				            	<table>
				            		<tr>
				            			<td valign="top">
							                <b>Description (English)</b><br /><br />
							                The description of the cube as it appears on application themes menu list. 
				            			</td>
				            			<td valign="top">
							                <img style="width: 250px" src="${pageContext.request.contextPath}/resources/img/tooltip/tooltip_chapter_name.png" style="float:right;margin-left:12px;" alt="" />
				            			</td>
				            		</tr>
				            	</table>
				            </div>
				        </div>
					</td>
					<td>
						<form:errors path="descEnglish" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;" valign="top"> Description (Spanish) </td>
					<td>
						<form:textarea path="descSpanish" cols="51" rows="5" />
					</td>
					<td>
						<img onmouseover="tooltip.pop(this, '#descSpanish_tip')" src="${pageContext.request.contextPath}/resources/img/help.jpg" width="32">
				        <div style="display:none;">
				            <div id="descSpanish_tip">
				            	<table>
				            		<tr>
				            			<td valign="top">
							                <b>Description (Spanish)</b><br /><br />
							                The description of the cube as it appears on application themes menu list. 
				            			</td>
				            			<td valign="top">
							                <img style="width: 250px" src="${pageContext.request.contextPath}/resources/img/tooltip/tooltip_chapter_name.png" style="float:right;margin-left:12px;" alt="" />
				            			</td>
				            		</tr>
				            	</table>
				            </div>
				        </div>
					</td>
					<td>
						<form:errors path="descSpanish" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;">
						Images
					</td>
					<td>
						<a href="${pageContext.request.contextPath}/cube/display_images/${cube.id}/0">(View)</a>
					</td>
					<td>
						<img onmouseover="tooltip.pop(this, '#images_tip')" src="${pageContext.request.contextPath}/resources/img/help.jpg" width="32">
				        <div style="display:none;">
				            <div id="images_tip">
				            	<table>
				            		<tr>
				            			<td valign="top">
							                <b>Images</b><br /><br />
							                The images that will be used as background and cube sides. <br>
							                On the application generation process, the selected parts of the images are resized to the following dimensions:<br>
							                <br>
							                Background: 1024 x 512<br>
							                Cube sides: 512 x 512<br>
							                <br>
							                It is recommended to upload images larger than those dimensions to work with so they
							                dont get stretched during the redimensioning process.
				            			</td>
				            		</tr>
				            	</table>
				            </div>
				        </div>
					</td>
				</tr>
				<tr><td> <br> </td></tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="Modify" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form:form>