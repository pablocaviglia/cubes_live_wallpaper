<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>
	<script>
	function deleteProject(id, name) {
		var confirmMsg = "Are you sure you want to delete project '" + name + "'?";
		if(confirm(confirmMsg)) {
			window.location = "${pageContext.request.contextPath}/project/delete/" + id;
		}
	}
	</script>
</head>

<body>
	<h2 style="text-align: center;">List Projects</h2>
	<table style="margin: auto;">
		<tr style="font-weight: bolder;">
			<td>
				Project Name
			</td>
			<td>
				Delete
			</td>
		</tr>
		<c:forEach var="project" items="${projects}">
			<tr>
				<td>
					<a href="${pageContext.request.contextPath}/project/get/${project.id}"><c:out value="${project.nameEnglish}"></c:out></a>
				</td>
				<td align="center">
					<a href="javascript:deleteProject(${project.id}, '${project.nameEnglish}')"><img src="${pageContext.request.contextPath}/resources/img/delete.jpg" style="width: 18px; height: 18px" ></a>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
