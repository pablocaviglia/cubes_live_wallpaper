<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/img_upload_dropbox.css" />
    <script>
	var uploadUrl = '${pageContext.request.contextPath}/api/project/uploadIcon?projectId=${project.id}';
	var downloadUrl = '${pageContext.request.contextPath}/project/getIcon/${project.id}';
	var reloadImageWidth = 300;
	var reloadImageHeight = 300;
	var uploadMaxFiles = 1;
	var uploadMaxFileSize = 5; //mb
		
    </script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery-1.9.0.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.filedrop.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/upload_image.js"></script>
</head>

<body>

	<h2 style="text-align: center;">project '<a href="${pageContext.request.contextPath}/project/get/${project.id}">${project.nameEnglish}</a>' / upload icon</h2>
	<br>
	<div id="imgUploadDropbox" style="width:300px; height: 300px; margin: auto; text-align: center;">
		<c:choose>
			<c:when test="${existsIcon}">
				<div class="preview">
					<span class="imageHolder">
						<img style="width:300px; height: 300px; margin-left: auto; margin-right: auto;" src="${pageContext.request.contextPath}/project/getIcon/${project.id}" />
					</span>
				</div>
			</c:when>
			<c:otherwise>
				<span class="message" style="margin: 50% 50% 50% 50%; text-align: center; font-size: 24px; ">Drop icon here to upload.</span>
			</c:otherwise>								
		</c:choose>
	</div>
</body>