<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<head>
	<script src="${pageContext.request.contextPath}/resources/js/jquery-1.9.0.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/tooltip.js"></script>
    <link href="${pageContext.request.contextPath}/resources/css/tooltip.css" rel="stylesheet" type="text/css" />
</head>

<form:form method="POST" modelAttribute="projectForm" action="${pageContext.request.contextPath}/project/modify">
<table style="margin: auto;">
	<tr>
		<td valign="top">
			<h2 style="text-align: center;">${project.nameEnglish}</h2>
			<table style="margin: auto;">
				<tr>
					<td style="font-weight: bolder;"> Name (English) </td>
					<td>
						<form:hidden path="id" />
						<form:input path="nameEnglish" size="36" />
					</td>
					</td>
					<td>
						<form:errors path="nameEnglish" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;"> Name (Spanish) </td>
					<td>
						<form:input path="nameSpanish" size="36" />
					</td>
					<td>
						<form:errors path="nameSpanish" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;" valign="top"> Description (English) </td>
					<td>
						<form:textarea path="descEnglish" cols="51" rows="5" />
					</td>
					<td>
						<form:errors path="descEnglish" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;" valign="top"> Description (Spanish) </td>
					<td>
						<form:textarea path="descSpanish" cols="51" rows="5" />
					</td>
					<td>
						<form:errors path="descSpanish" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;">
						Version Code
					</td>
					<td>
						<form:input path="versionCode" size="36" />
					</td>
					<td>
						<form:errors path="versionCode" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;">
						Version Name
					</td> 
					<td>
						<form:input path="versionName" size="36" />
					</td>
					<td>
						<form:errors path="versionName" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;"> Admob Banner Id </td>
					<td>
						<form:input path="admobBannerId" size="36" />
					</td>
					<td>
						<form:errors path="admobBannerId" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;"> Admob Interstitial Id </td>
					<td>
						<form:input path="admobInterstitialId" size="36" />
					</td>
					<td>
						<form:errors path="admobInterstitialId" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;">
						Icon
					</td>
					<td>
						<a href="${pageContext.request.contextPath}/project/display_upload_icon_image/${project.id}">(Upload)</a>
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;">
						Cubes
					</td>
					<td colspan="2">
						${fn:length(project.cubes)} &nbsp; 
						<a href="${pageContext.request.contextPath}/cube/list/${project.id}">(List)</a> &nbsp;&nbsp;
						<a href="${pageContext.request.contextPath}/cube/display_create/${project.id}">(Create)</a> &nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;">
						Publisher
					</td>
					<td>
						<c:if test="${project.publisher ne null}">
							${project.publisher.name}
						</c:if>
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;">
						Package
					</td>
					<td>
						${project.androidPackage}
					</td>
				</tr>
				<tr><td> <br> </td></tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="Modify" />
						&nbsp;&nbsp;&nbsp;
						<input type="button" value="Generate APK" onclick="window.location = '${pageContext.request.contextPath}/project/display_generateAPK/${project.id}'" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form:form>