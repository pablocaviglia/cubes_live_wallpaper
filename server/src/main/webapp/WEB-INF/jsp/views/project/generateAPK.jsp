<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css" />
    <script type="text/javascript">
        
    	var ws = null;
    	var connected = false;
		
    	function generateAPK(install) {
    		
    		ws = new WebSocket('ws://' + window.location.host + '/${pageContext.request.contextPath}/generateAPK');
    		
    		ws.onopen = function () {
    			connected = true;
                log('Info: connection opened.');
                document.getElementById("generateAPK").disabled = true;
                var buildParameters = document.getElementById("buildParameters").value;
                ws.send('${project.id}/'+buildParameters);
            };
            ws.onmessage = function (event) {
            	var data = event.data;
            	if(data == 'apk_generated') {
            		document.getElementById("generateAPK").disabled = false;
            		if(install) {
            		    installAPK();
            		}
            	}
            	else {
            		log(event.data);
            	}
            };
            ws.onclose = function (event) {
            	connected = false;
                log('Info: connection closed.');
                log(event);
            };
    	}

    	function installAPK() {

			ws = new WebSocket('ws://' + window.location.host + '/${pageContext.request.contextPath}/installAPK');

			ws.onopen = function () {
				connected = true;
				log('Info: connection opened.');
				document.getElementById("installAPK").disabled = true;
				ws.send('${project.id}');
			};
			ws.onmessage = function (event) {
				var data = event.data;
				if(data == 'apk_installed') {
					document.getElementById("installAPK").disabled = false;
				}
				else {
					log(event.data);
				}
			};
			ws.onclose = function (event) {
				connected = false;
				log('Info: connection closed.');
				log(event);
			};
		}
        
        function log(message) {
            var console = document.getElementById('console');
            var p = document.createElement('p');
            p.style.wordWrap = 'break-word';
            p.appendChild(document.createTextNode(message));
            console.appendChild(p);
            console.scrollTop = console.scrollHeight;
        }
    </script>
</head>
<body>
<div>
	<h2 style="text-align: center;">project '<a href="${pageContext.request.contextPath}/project/get/${project.id}">${project.nameEnglish}</a>'</h2>
	<br>
	
	<table style="width: 100%;">
		<tr>
			<td width="100%">
	            Build parameters <input type="text" id="buildParameters" value="release">
				<button id="generateAPK" onclick="generateAPK(false);">Generate APK</button> &nbsp;&nbsp;&nbsp;
				<button id="installAPK" onclick="installAPK();">Install APK</button> &nbsp;&nbsp;&nbsp;
				<button id="generateAndInstallAPK" onclick="generateAPK(true);">Generate & Install</button>
			</td>
		</tr>
		<tr>
			<td width="100%">
			    <div class="contentBox">
			        <div id="console"></div>
			    </div>
			</td>
		</tr>
		<tr>
			<td align="center">
				<a href="${pageContext.request.contextPath}/api/project/downloadAPK/${project.id}-release.apk">Download APK</a> <br />
			</td>
		</tr>
	</table>
</div>
</body>
</html>