<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<body>
	<h2 style="text-align: center;">Create Project</h2>
 	<form:form method="POST" modelAttribute="createProjectForm" action="${pageContext.request.contextPath}/project/create">
		<table style="margin: auto;">
			<tr valign="top">
				<td>Name (English):</td>
				<td><form:input path="nameEnglish" /></td>
				<td><form:errors path="nameEnglish" cssClass="error" /></td>
			</tr>
			<tr valign="top">
				<td>Name (Spanish):</td>
				<td><form:input path="nameSpanish" /></td>
				<td><form:errors path="nameSpanish" cssClass="error" /></td>
			</tr>
			<tr valign="top">
				<td>Code:</td>
				<td><form:input path="code" /></td>
				<td><form:errors path="code" cssClass="error" /></td>
			</tr>
			<tr>
				<td colspan="3" align="center"><input type="submit" value="Create" /></td>
			</tr>
		</table>
	</form:form>
</body>