$(function(){
	
	var dropbox = $('#imgUploadDropbox'),
		message = $('.message', dropbox);
	
	dropbox.filedrop({
		// The name of the $_FILES entry:
		paramname:'pic',
		maxfiles: uploadMaxFiles,
    	maxfilesize: uploadMaxFileSize,
		url: uploadUrl,
		downloadUrl: downloadUrl,
		reloadImageWidth: reloadImageWidth,
		reloadImageHeight: reloadImageHeight,
		
		uploadFinished:function(i,file,response) {
			$('.progressHolder').remove();
			$('.imageHolder img').attr("src", downloadUrl + "?force=" + new Date().getTime());
			$(".imageHolder img").width(reloadImageWidth).height(reloadImageHeight);
		},
		
    	error: function(err, file) {
			switch(err) {
				case 'BrowserNotSupported':
					showMessage('Your browser does not support HTML5 file uploads!');
					break;
				case 'TooManyFiles':
					alert('Too many files! Please select ' + uploadMaxFiles + ' at most!');
					break;
				case 'FileTooLarge':
					alert(file.name+' is too large! Please upload files up to ' + uploadMaxFileSize + ' mb.');
					break;
				default:
					break;
			}
		},
		
		// Called before each upload is started
		beforeEach: function(file) {
			return true;
		},
		
		uploadStarted:function(i, file, len){
			$('.preview').remove();
			createImage(file);
		},
		
		progressUpdated: function(i, file, progress) {
			$.data(file).find('.progress').width(progress);	
		}
	});
	
	var template = '<div class="preview">'+
						'<span class="imageHolder">'+
							'<img />'+
							'<span class="uploaded"></span>'+
						'</span>'+
						'<div class="progressHolder">'+
							'<div class="progress"></div>'+
						'</div>'+
					'</div>'; 
	
	
	function createImage(file){
		
		var preview = $(template), 
			image = $('img', preview);
			
		var reader = new FileReader();
		
		image.width = reloadImageWidth;
		image.height = reloadImageHeight;
		
		reader.onload = function(e){
			// e.target.result holds the DataURL which
			// can be used as a source of the image:
			image.attr('width', reloadImageWidth);
			image.attr('height', reloadImageHeight);
			image.attr('src',e.target.result);
		};
		
		// Reading the file as a DataURL. When finished,
		// this will trigger the onload function above:
		reader.readAsDataURL(file);
		
		message.hide();
		preview.appendTo(dropbox);
		
		// Associating a preview container
		// with the file, using jQuery's $.data():
		
		$.data(file,preview);
	}

	function showMessage(msg){
		message.html(msg);
	}
});