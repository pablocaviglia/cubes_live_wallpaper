package com.gmi.cube.base.cubethemegenericlw;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.gmi.cube.base.cubethemegenericlw.state.CubeThemeGenericLiveWallpaperState;
import com.gmi.cube.base.util.AbstractLiveWallpaper;
import com.gmi.cube.base.util.AppContants;

public class CubeThemeGenericLiveWallpaperService extends AbstractLiveWallpaper implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor accelerometerSensor;

	public CubeThemeGenericLiveWallpaperService() {
		
		super(new CubeThemeGenericLiveWallpaperState());
		state.liveWallpaper = this;
	}
	
	@Override
	public void onCreate() {
		
		super.onCreate();

		//seteamos constante con el nombre del paquete
		AppContants.packageName = getApplicationContext().getPackageName();
		
		//obtenemos el sensor acelerometro
		sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
		accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		
		//registramos los eventos del sensor
		sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
		state.liveWallpaper = this;

	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		state.finalizarRecursos();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			state.accelerometerEvent(event.values[0], event.values[1], event.values[2]);
		}
	}

	@Override
	public void onPause() {
		sensorManager.unregisterListener(this);
		state.liveWallpaper = this;

	}

	@Override
	public void onResume() {
		sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
		state.liveWallpaper = this;

	}
}