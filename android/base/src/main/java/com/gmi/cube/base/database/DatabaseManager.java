package com.gmi.cube.base.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gmi.cube.base.util.AppContants;

public class DatabaseManager {

	private static final int DATABASE_VERSION = 2;
	public SQLiteDatabase db = null;
	
	//table names
	private static final String TABLE_CONFIGURATION = "CONFIGURATION";
	
	//ids de valores en tabla config
	public static int CONFIGURATION_USOS_DISPONIBLES = 1;
	
    //statements for creating tables
    private static final String CREATE_TABLE_CONFIG = 
    			"CREATE TABLE " + 
    			TABLE_CONFIGURATION + " ( " +
            	"ID INTEGER PRIMARY KEY NOT NULL, " + 
            	"VALUE VARCHAR(512));";

    public DatabaseManager(Context context) {
		SQLiteOpenHelper o = new PersistenceOpenHelper(context, "cubethemelw" + context.getPackageName() + ".db");
		this.db = o.getWritableDatabase();
	}
    
	// Rest of standard DataHelper class
	private class PersistenceOpenHelper extends SQLiteOpenHelper {
		
		PersistenceOpenHelper(Context context, String databaseName) {
			super(context, databaseName, null, DATABASE_VERSION);
		}
		
		@Override
		public void onCreate(SQLiteDatabase db) {
			
		    //drop and create tableS
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONFIGURATION);
	        db.execSQL(CREATE_TABLE_CONFIG);
	        
	        //insert default 'CONFIG' values
	        db.execSQL("INSERT INTO " + TABLE_CONFIGURATION + "(ID, VALUE) VALUES(" + CONFIGURATION_USOS_DISPONIBLES + ", '" + AppContants.MAXIMA_CANTIDAD_USO_GRATUITA_THEMES + "');");
	        
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			onCreate(db);
		}
	}
	
	public String obtenerValorConfig(int key) {
		
		String value = null;
	    Cursor cursor = db.query(TABLE_CONFIGURATION, 
				 new String[] {"VALUE"}, 
				 "ID=?",
				 new String[] { String.valueOf(key) }, null, null, null, null);

	    if (cursor != null && cursor.moveToFirst()) {
	    	value = cursor.getString(0);
	    }
	    
		return value;
	}
	
	public void actualizarValorConfig(int key, String value) {
		ContentValues args = new ContentValues();
	    args.put("VALUE", value);
	    db.update(TABLE_CONFIGURATION, args, "ID="+key, null);
	}
}