package com.gmi.cube.base.threed;

import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.TextureManager;

public class TexturedFacesCube extends Object3D {

	private static final long serialVersionUID = -1438961433647913005L;
	public int size;
	
	public TexturedFacesCube(int size, String texture1, String texture2, String texture3, String texture4, String texture5, String texture6, int x_offset, int y_offset, int z_offset) {
		
		super(12);
		this.size = size;

		SimpleVector upperLeftFront = new SimpleVector(-size - x_offset, -size - y_offset, -size - z_offset);
		SimpleVector upperRightFront = new SimpleVector(size - x_offset, -size - y_offset, -size - z_offset);
		SimpleVector lowerLeftFront = new SimpleVector(-size - x_offset, size - y_offset, -size - z_offset);
		SimpleVector lowerRightFront = new SimpleVector(size - x_offset, size - y_offset, -size - z_offset);

		SimpleVector upperLeftBack = new SimpleVector(-size - x_offset, -size - y_offset, size - z_offset);
		SimpleVector upperRightBack = new SimpleVector(size - x_offset, -size - y_offset, size - z_offset);
		SimpleVector lowerLeftBack = new SimpleVector(-size - x_offset, size - y_offset, size - z_offset);
		SimpleVector lowerRightBack = new SimpleVector(size - x_offset, size - y_offset, size - z_offset);

		// Front
		addTriangle(upperLeftFront, 0, 0, lowerLeftFront, 0, 1, upperRightFront, 1, 0, TextureManager.getInstance().getTextureID(texture1));
		addTriangle(upperRightFront, 1, 0, lowerLeftFront, 0, 1, lowerRightFront, 1, 1, TextureManager.getInstance().getTextureID(texture1));

		// Back
		addTriangle(upperLeftBack, 0, 0, upperRightBack, 1, 0, lowerLeftBack, 0, 1, TextureManager.getInstance().getTextureID(texture2));
		addTriangle(upperRightBack, 1, 0, lowerRightBack, 1, 1, lowerLeftBack, 0, 1, TextureManager.getInstance().getTextureID(texture2));

		// Upper
		addTriangle(upperLeftBack, 0, 0, upperLeftFront, 0, 1, upperRightBack, 1, 0, TextureManager.getInstance().getTextureID(texture3));
		addTriangle(upperRightBack, 1, 0, upperLeftFront, 0, 1, upperRightFront, 1, 1, TextureManager.getInstance().getTextureID(texture3));

		// Lower
		addTriangle(lowerLeftBack, 0, 0, lowerRightBack, 1, 0, lowerLeftFront, 0, 1, TextureManager.getInstance().getTextureID(texture4));
		addTriangle(lowerRightBack, 1, 0, lowerRightFront, 1, 1, lowerLeftFront, 0, 1, TextureManager.getInstance().getTextureID(texture4));

		// Left
		addTriangle(upperLeftFront, 0, 0, upperLeftBack, 1, 0, lowerLeftFront, 0, 1, TextureManager.getInstance().getTextureID(texture5));
		addTriangle(upperLeftBack, 1, 0, lowerLeftBack, 1, 1, lowerLeftFront, 0, 1, TextureManager.getInstance().getTextureID(texture5));

		// Right
		addTriangle(upperRightFront, 0, 0, lowerRightFront, 0, 1, upperRightBack, 1, 0, TextureManager.getInstance().getTextureID(texture6));
		addTriangle(upperRightBack, 1, 0, lowerRightFront, 0, 1, lowerRightBack, 1, 1, TextureManager.getInstance().getTextureID(texture6));

	}
}