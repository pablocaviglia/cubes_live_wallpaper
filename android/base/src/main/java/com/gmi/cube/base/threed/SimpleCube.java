package com.gmi.cube.base.threed;

import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;

public class SimpleCube extends Object3D {
	
	private static final long serialVersionUID = -1888857543736152010L;

	public SimpleCube(int size) {
		super(12);
		
	    SimpleVector upperLeftFront=new SimpleVector(-size,-size,-size);
	    SimpleVector upperRightFront=new SimpleVector(size,-size,-size);
	    SimpleVector lowerLeftFront=new SimpleVector(-size,size,-size);
	    SimpleVector lowerRightFront=new SimpleVector(size,size,-size);
	    
	    SimpleVector upperLeftBack = new SimpleVector( -size, -size, size);
	    SimpleVector upperRightBack = new SimpleVector(size, -size, size);
	    SimpleVector lowerLeftBack = new SimpleVector( -size, size, size);
	    SimpleVector lowerRightBack = new SimpleVector(size, size, size);

	    // Front
	    addTriangle(upperLeftFront,0,0, lowerLeftFront,0,1, upperRightFront,1,0);
	    addTriangle(upperRightFront,1,0, lowerLeftFront,0,1, lowerRightFront,1,1);
	    
	    // Back
	    addTriangle(upperLeftBack,0,0, upperRightBack,1,0, lowerLeftBack,0,1);
	    addTriangle(upperRightBack,1,0, lowerRightBack,1,1, lowerLeftBack,0,1);
	    
	    // Upper
	    addTriangle(upperLeftBack,0,0, upperLeftFront,0,1, upperRightBack,1,0);
	    addTriangle(upperRightBack,1,0, upperLeftFront,0,1, upperRightFront,1,1);
	    
	    // Lower
	    addTriangle(lowerLeftBack,0,0, lowerRightBack,1,0, lowerLeftFront,0,1);
	    addTriangle(lowerRightBack,1,0, lowerRightFront,1,1, lowerLeftFront,0,1);
	    
	    // Left
	    addTriangle(upperLeftFront,0,0, upperLeftBack,1,0, lowerLeftFront,0,1);
	    addTriangle(upperLeftBack,1,0, lowerLeftBack,1,1, lowerLeftFront,0,1);
	    
	    // Right
	    addTriangle(upperRightFront,0,0, lowerRightFront,0,1, upperRightBack,1,0);
	    addTriangle(upperRightBack,1,0, lowerRightFront, 0,1, lowerRightBack,1,1);

//	    setTexture("base");
	    build();
	    
	}
}
