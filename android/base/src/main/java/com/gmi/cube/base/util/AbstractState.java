package com.gmi.cube.base.util;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import com.gmi.cube.base.wallpaper.GLWallpaperService;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.RGBColor;

public abstract class AbstractState implements GLWallpaperService.Renderer {
	
	public static final Object lock = new Object();
	
	public boolean recursosIniciados;
	
	//id previo al actual 
	//de accion en evento touch
	private int accionTouchAnteriorTouch1 = -1;
	
	//ultimo estado del wallpaper
	//fue preview o modo normal
	private int ultimoScreenWidth;
	private int ultimoScreenHeight;
	
	//dimension de la pantalla
	protected int screenWidth;
	protected int screenHeight;

	//buffer donde es renderizado el mundo
	private FrameBuffer fb = null;
	
	//color de fondo
	private RGBColor back = new RGBColor(0, 0, 0);
	
	//momento segun System.currentTimeMillis cuando
	//fue por ultima vez llamado el metodo onDrawFrame
	public long lastDrawFrame;
	
	//referencia
	public AbstractLiveWallpaper liveWallpaper;
	
	//indica si el estado ya se
	//encuentra inicializado
	protected boolean inicializado;
	
	//indica que el estado debe
	//de ser reiniciado
	private boolean reiniciarEstado;
	
	@Override
	public void onDrawFrame(GL10 gl) {
		
		lastDrawFrame = System.currentTimeMillis();

		if(AppContants.contextoCambiado) {
			reiniciarEstado(gl);
		}
		else {
			
			if(inicializado) {

				//obtenemos el framebuffer
				//donde vamos a renderizar
				FrameBuffer fb = obtenerFramebuffer();
				
				if(fb != null) {
					
					//limpiamos el framebuffer
					//pintandolo completamente
					//de un color
					fb.clear(back);

					//renderiza la implementacion
					render(fb);
					
					//desplegamos lo renderizado
					fb.display();

				}
			}
		}
	}
	
	/**
	 * Llamado cuando sucede un evento touch
	 * 
	 * @param event
	 */
	public void onTouchEvent(MotionEvent event) {
		
		if(inicializado) {
			
			float multiTouchX = 0;
			float multiTouchY = 0;
			
			//multi-touch siendo usado
			if(event.getPointerCount() > 1) {
				multiTouchX = event.getX(1);
				multiTouchY = event.getY(1);
			}
			
			final int pointerIndex = event.getActionIndex();
			
			if(accionTouchAnteriorTouch1 == MotionEvent.ACTION_DOWN && event.getAction() == MotionEvent.ACTION_MOVE) {
				touchDragStart((int)event.getX(), (int)event.getY());
			}
			else if(accionTouchAnteriorTouch1 == MotionEvent.ACTION_MOVE && event.getAction() == MotionEvent.ACTION_MOVE) {
				touchDragged(event.getX(), event.getY(), multiTouchX, multiTouchY);
			}
			else if(accionTouchAnteriorTouch1 == MotionEvent.ACTION_DOWN && event.getAction() == MotionEvent.ACTION_UP) {
				touchClick((int)event.getX(), (int)event.getY());
			}
			else if(accionTouchAnteriorTouch1 == MotionEvent.ACTION_MOVE && event.getAction() == MotionEvent.ACTION_UP) {
				touchDragFinish((int)event.getX(), (int)event.getY());
			}

			//guardamos el id actual como
			//anterior luego de evaluarlo
			if(pointerIndex == 0) {
				accionTouchAnteriorTouch1 = event.getAction();
			}
		}
	}
	
	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		
		synchronized (lock) {
			
			System.out.println("-------------------------------> ON SURFACE CHANGED");
			
			//guardamos referencia de
			//las medidas de la superficie
			screenWidth = width;
			screenHeight = height;

			if(reiniciarEstado || ultimoScreenWidth != screenWidth || ultimoScreenHeight != screenHeight) {
				reiniciarEstado(gl);
			}
			
			//ejecuta la post inicializacion
			postInicializacion();

			//guardamos las ultimas dimensiones
			ultimoScreenWidth = screenWidth;
			ultimoScreenHeight = screenHeight;
			
		}
	}
	
	@Override
	public void onSurfaceCreated(SurfaceHolder holder) {
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
	}
	
	@Override
	public void onSurfaceDestroyed(SurfaceHolder holder) {
	}
	
	public int[] getScreenDimensions() {
		
		Context context = liveWallpaper.getBaseContext();
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

		int[] dimensions = {displayMetrics.widthPixels, displayMetrics.heightPixels};
		return dimensions;
	}
	
	public void reiniciarEstado() {
		synchronized (lock) {
			reiniciarEstado = true;
			recursosIniciados = false;
		}
	}
	
	private void reiniciarEstado(GL10 gl) {

		synchronized (lock) {
			
			//cambiamos flag
			AppContants.contextoCambiado = false;

			//cambiamos flag
			reiniciarEstado = false;
			
			//inicializamos el framebuffer
			inicializarFrameBuffer(gl);

			//configuramos flag para que no
			//se ejecuten cosas sin estar
			//inicializado
			inicializado = false;
			
			if(!recursosIniciados) {
				//inicializamos el estado
				inicializar();
				recursosIniciados = true;
			}
			
			//cambiamos flag
			inicializado = true;

		}
	}
	
	private void inicializarFrameBuffer(GL10 gl) {
		
		if(fb != null) {
			fb.clear();
			fb.dispose();
			fb = null;
		}
		
		fb = new FrameBuffer(gl, screenWidth, screenHeight);
	}
	
	public FrameBuffer obtenerFramebuffer() {
		return fb;
	}

	/**
	 * Evento que indica que el acelerometro
	 * fue movido en alguno/s de los eje/s
	 * @param msX
	 * @param msY
	 */
	public abstract void accelerometerEvent(float msX, float msY, float msZ);
	public abstract void touchDragged(float x, float y, float x2, float y2);
	public abstract void touchDragStart(int x, int y);
	public abstract void touchDragFinish(int x, int y);
	public abstract void touchClick(int x, int y);

	/**
	 * Logica del estado que indica el
	 * tiempo transcurrido desde el ultimo
	 * update
	 * 
	 * @param elapsedTime
	 */
	public abstract void update(long elapsedTime);
	
	/**
	 * Renderiza la implementacion de AbstractState
	 */
	public abstract void render(FrameBuffer fb);

	/**
	 * Limpia recursos antes de salir
	 */
	public abstract void salir();

	/**
	 * Inicializacion
	 */
	public abstract void inicializar();
	
	/**
	 * Finaliza recursos
	 */
	public abstract void finalizarRecursos();
	
	/**
	 * Codigo post inicializacion
	 */
	protected abstract void postInicializacion();

}