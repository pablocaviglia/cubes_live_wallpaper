package com.gmi.cube.base.util;

import android.os.Handler;
import android.view.MotionEvent;

import com.gmi.cube.base.wallpaper.GLWallpaperService;

public abstract class AbstractLiveWallpaper extends GLWallpaperService {

	public static long FPS = 60;
	private long FPS_ORIGINAL = FPS;
	
	private long time;
	private static long fps;
	private long lastime;

	//estado asociado al wallpaper
	public AbstractState state;

	//referencia estatica a la 
	//instancia de esta clase
	public static AbstractLiveWallpaper liveWallpaper;
	public GLEngine engine;

	public AbstractLiveWallpaper(AbstractState state) {
		this.state = state;
	}
	
	public Engine onCreateEngine() {

		//creamos la referencia estatica a esta
		//instancia asi puede ser accedida desde
		//otro hilo, por ejemplo una clase Activity
		liveWallpaper = this;

		LiveWallpaperEngine engine = new LiveWallpaperEngine();
		this.engine = engine;
		
		return engine;
	}
	
	public abstract void onPause();
	public abstract void onResume();
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	public void cambiarFPS(long fps) {
		FPS = fps;
	}
	
	public void volverFPSOriginal() {

		FPS = FPS_ORIGINAL;
		fps = FPS;

	}
	
	class LiveWallpaperEngine extends GLEngine implements Runnable {
		
		private Thread wallpaperThread;
		private boolean suspendFlag;
		private Handler handler = new Handler();
		
		public LiveWallpaperEngine() {
			
			super();

			// handle prefs, other initialization
			setRenderer(state);
			setRenderMode(RENDERMODE_WHEN_DIRTY);
			
			//creamos
			wallpaperThread = new Thread(this);
			wallpaperThread.start();
			
		}
		
		@Override
		public void onVisibilityChanged(boolean visible) {

			//esto lo hacemos para no consumir recursos
			//de cpu mientras no se renderiza nada, y de
			//esta manera ahorramos bateria
			if(visible) {
				resumir();
			}
			else {
				pausar();
			}
			
			super.onVisibilityChanged(visible);
		}
		
		@Override
		public void onTouchEvent(MotionEvent event) {

			super.onTouchEvent(event);

			if (state != null) {
				state.onTouchEvent(event);
			}
		}

		@Override
		public void run() {
			
	    	while(true) {
	    		
		    	if(state != null) {
		    		
		    		long curtime = System.currentTimeMillis();
			    	long diftime = curtime - time;
			    	long dif = curtime - lastime;
			    	
			    	lastime = curtime;
			    	
			    	if (diftime >= 1000) {
//						System.out.println(fps + "fps");
						fps = 0;
						time = System.currentTimeMillis();
					}
					fps++;

			    	//ejecutamos render
			        requestRender();
			        
			        if(state != null && state.inicializado) {
				    	//ejecutamos update y render
				        state.update(dif);
			        }

			        try {
			        	//dormimos el hilo
						Thread.sleep(1000/FPS);
						
						synchronized (this) {
							while (suspendFlag) {
								wait();
							}
						}
					} 
			        catch (InterruptedException e) {
						e.printStackTrace();
					}
		    	}
	    	}
		}
		
		public void pausar() {
			AbstractLiveWallpaper.this.onPause();
			suspendFlag = true;	
		}
		
		public void resumir() {

			suspendFlag = false;
			
			AbstractLiveWallpaper.this.onResume();
			
			try {
				synchronized (this) {
					notify();
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}