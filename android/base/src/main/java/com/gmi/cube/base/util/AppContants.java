package com.gmi.cube.base.util;

public class AppContants {
	
	/**
	 * Codigo de publisher ADMOB
	 */
	public static String ADMOB_BANNER_UNIT_ID;
	public static String ADMOB_INTERSTITIAL_UNIT_ID;
	
	/**
	 * Indica si la funcionalidad de habilitar 
	 * themes a traves del click obligado de 
	 * propagandas esta habilitado o no
	 */
	public final static boolean HABILITA_THEMES_CLIQUEANDO_PROPAGANDAS = true;

	/**
	 * Cantidad de veces que se puede cambiar
	 * de theme antes que sea necesario
	 * cliquear en la propaganda
	 */
	public static final int MAXIMA_CANTIDAD_USO_GRATUITA_THEMES = 3;
	
	/**
	 * Nombre del paquete que identifica a esta aplicacion
	 */
	public static String packageName;
	
	/**
	 * Cambiado a true por rutinas GL cuando
	 * el contexto GL ha cambiado de alguna manera.
	 * Esto lo usamos como fix para cuando cambiamos
	 * la renderizacion de modo live wallpaper preview
	 * a modo real, en donde a veces sucede que quede
	 * renderizado todo de blanco
	 */
	public static boolean contextoCambiado;
	
}