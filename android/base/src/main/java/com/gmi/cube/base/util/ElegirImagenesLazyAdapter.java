package com.gmi.cube.base.util;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gmi.cube.base.R;

public class ElegirImagenesLazyAdapter extends BaseAdapter {
 
    private Activity activity;
    private ArrayList<HashMap<String, Object>> data;
    private static LayoutInflater inflater=null;
 
    public ElegirImagenesLazyAdapter(Activity a, ArrayList<HashMap<String, Object>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    public int getCount() {
        return data.size();
    }
 
    public Object getItem(int position) {
        return position;
    }
 
    public long getItemId(int position) {
        return position;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {

    	View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.elegir_temas_list_row, null);
 
        TextView title = (TextView)vi.findViewById(R.id.elegir_temas_title); // title
        TextView desc = (TextView)vi.findViewById(R.id.elegir_temas_desc); // artist name
        ImageView thumb_image = (ImageView)vi.findViewById(R.id.elegir_temas_list_image); // thumb image
        
        HashMap<String, Object> element = data.get(position);
 
        // Setting all values in listview
        title.setText((String)element.get("title"));
        desc.setText((String)element.get("desc"));
        thumb_image.setImageBitmap((Bitmap)element.get("thumb_bitmap"));
        
        return vi;
    }
}