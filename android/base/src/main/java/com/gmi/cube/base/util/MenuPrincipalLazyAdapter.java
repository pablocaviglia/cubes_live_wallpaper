package com.gmi.cube.base.util;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gmi.cube.base.R;

public class MenuPrincipalLazyAdapter extends BaseAdapter {
 
    private ArrayList<HashMap<String, Object>> data;
    private static LayoutInflater inflater=null;
 
    public MenuPrincipalLazyAdapter(Context context) {
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    public int getCount() {
        return data != null ? data.size() : 0;
    }
 
    public Object getItem(int position) {
        return position;
    }
 
    public long getItemId(int position) {
        return position;
    }
    
    public ArrayList<HashMap<String, Object>> getData() {
		return data;
	}

	public void setData(ArrayList<HashMap<String, Object>> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.boton_list_row, null);
        
        if(data != null) {
            //data recibida
            HashMap<String, Object> element = data.get(position);
            
            //seteamos el estado al tag del
            //objeto asi cuando nos hacen 
            //click tenemos informacion del 
            //objeto de negocio
            vi.setTag(element.get("estado"));
            
            TextView title = (TextView)vi.findViewById(R.id.boton_title); 
            TextView desc = (TextView)vi.findViewById(R.id.boton_desc); 
            ImageView thumb_image = (ImageView)vi.findViewById(R.id.boton_image);
            
            // Setting all values in listview
            title.setText((String)element.get("title"));
            desc.setText((String)element.get("desc"));
            thumb_image.setImageBitmap((Bitmap)element.get("thumb_bitmap"));
        }
        
        return vi;
    }
}