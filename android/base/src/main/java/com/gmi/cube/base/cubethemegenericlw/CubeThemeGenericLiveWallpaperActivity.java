package com.gmi.cube.base.cubethemegenericlw;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.gmi.cube.base.R;
import com.gmi.cube.base.cubethemegenericlw.state.CubeThemeGenericLiveWallpaperState;
import com.gmi.cube.base.database.DatabaseManager;
import com.gmi.cube.base.domain.ThemePackage;
import com.gmi.cube.base.themePackage.ThemePackageReader;
import com.gmi.cube.base.ui.adapter.MenuPagerAdapter;
import com.gmi.cube.base.ui.pager.NonSwipeableViewPager;
import com.gmi.cube.base.util.AppContants;
import com.gmi.cube.base.util.FileUtil;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class CubeThemeGenericLiveWallpaperActivity extends Activity {

	private final int REQUEST_CODE_SELECCIONAR_WALLPAPER = 1;
	
	//admob ads references
	private InterstitialAd interstitial;
	
	//popup que muestra informacion 
	//de los ads
	private PopupWindow popupWindow;
	
	//flag que indica si ha finalizado
	//el relampagueo del banner
	boolean finalizarRelampagueo;
    
	//flag que indica si debemos de
	//salir de la aplicacion cuando
	//se presione el boton de back
	private boolean exitOnBack;
	
	//contexto actual de la aplicacion
	private static final byte CONTEXTO_MENU_PRINCIPAL = 0;
	private static final byte CONTEXTO_THEMES = 1;
	private byte contextoActual; 
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		//seteamos constante con el nombre del paquete
		AppContants.packageName = getApplicationContext().getPackageName();
		
		//set the view
		setContentView(R.layout.menu_swiper);
		
		//get the pager
		NonSwipeableViewPager pager = (NonSwipeableViewPager) findViewById(R.id.menupager);
		
		//create the help view adapter
		MenuPagerAdapter pagerAdapter = new MenuPagerAdapter(this, pager);
		
		// Set the View Pager Adapter into ViewPager
        pager.setOffscreenPageLimit(pagerAdapter.getCount());  
        pager.setAdapter(pagerAdapter);
		
		//setea el estado inicial de
		//renderizacion del cubo
		CubeThemeGenericLiveWallpaperState.cuboHabilitado = cuboHabilitado();
		
        //create the admob banner
		createAdmodBanner();
		
		//load the ads
		loadBanner();
		loadInterstitial();
		
	}
	
	public void createAdmodBanner() {
		if(AppContants.ADMOB_BANNER_UNIT_ID != null && !AppContants.ADMOB_BANNER_UNIT_ID.trim().equals("")) {
		   	final AdView admobBanner = new AdView(this);
	    	admobBanner.setAdUnitId(AppContants.ADMOB_BANNER_UNIT_ID);
	    	admobBanner.setAdSize(AdSize.SMART_BANNER);
	    	
	    	admobBanner.setAdListener(new AdListener() {
	    		@Override
	    		public void onAdFailedToLoad(int errorCode) {
	    			super.onAdFailedToLoad(errorCode);
	    			//retry load
	    			admobBanner.postDelayed(new Runnable() {
						@Override
						public void run() {
							System.out.println("------ RECARGANDO AD DEBIDO A ERROR EN CARGA");
							loadBanner();
						}
					}, 5000);
	    		}
	    		@Override
	    		public void onAdLeftApplication() {
	    			super.onAdLeftApplication();
	    			resetearContadorAds();
	    			if(contextoActual == CONTEXTO_THEMES) {
	    				popupWindow.dismiss();
	    				detenerRelampaguearAdmobView();
	    			}
	    		}
			});
	    	
			//get the adview layout
	    	LinearLayout comicMenuWebLayout = (LinearLayout) findViewById(R.id.adMainContainer);
	    	
	    	//remove possible old views
	    	comicMenuWebLayout.removeAllViews();
	    	
			//show the adview on the UI
			//only if its loading process
			//is correctly finished
			comicMenuWebLayout.addView(admobBanner);
		}
	}
	
	public void loadBanner() {
		if(AppContants.ADMOB_BANNER_UNIT_ID != null && !AppContants.ADMOB_BANNER_UNIT_ID.trim().equals("")) {
			//get the adview layout
	    	LinearLayout comicMenuWebLayout = (LinearLayout) findViewById(R.id.adMainContainer);
	    	AdView bannerAdView = (AdView)comicMenuWebLayout.getChildAt(0);
	    	bannerAdView.loadAd(new AdRequest.Builder().build());
		}
	}
	
	public void loadInterstitial() {
		if(AppContants.ADMOB_INTERSTITIAL_UNIT_ID != null && !AppContants.ADMOB_INTERSTITIAL_UNIT_ID.trim().equals("")) {
	        //create and load the interstitial
	        interstitial = new InterstitialAd(this);
	        interstitial.setAdUnitId(AppContants.ADMOB_INTERSTITIAL_UNIT_ID);
	        interstitial.setAdListener(new AdListener() {
	        	  public void onAdLoaded() {
	        	  }
	        	  public void onAdFailedToLoad(int errorCode) {
	        	  }
	        	  public void onAdOpened() {
	        	  }
	        	  public void onAdClosed() {
        			  exitApplication();
	        	  }
	        	  public void onAdLeftApplication() {
	        	  }
			});
	        
	        //begin loading the interstitial
	        interstitial.loadAd(new AdRequest.Builder().build());
        }
	}
	
	public void displayInterstitial() {
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
		switch (requestCode) {
		case REQUEST_CODE_SELECCIONAR_WALLPAPER:
			moveTaskToBack(true);
			break;
		}
	}
	
	private void resetearContadorAds() {
		//reseteamos el contador para que empiece
		//desde el numero designado en la configuracion
		CubeApplication.data.actualizarValorConfig(DatabaseManager.CONFIGURATION_USOS_DISPONIBLES, String.valueOf(AppContants.MAXIMA_CANTIDAD_USO_GRATUITA_THEMES));
		if(popupWindow != null) {
			popupWindow.dismiss();
		}
	}
	
	public void habilitarCubo(boolean habilitar) {
		try {
			FileUtil.guardarContenido(openFileOutput("cuboHabilitado", Context.MODE_PRIVATE), String.valueOf(habilitar).getBytes());
		} 
		catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public boolean cuboHabilitado() {
		boolean habilitado = true;
		try {
			String contenidoStream = new String(FileUtil.obtenerContenidoStream(openFileInput("cuboHabilitado")));
			contenidoStream = contenidoStream.trim();
			habilitado = contenidoStream == null || (contenidoStream != null && (contenidoStream.equals("") || contenidoStream.equals("true")));
		} 
		catch (Exception e) {
		}
		return habilitado;
	}
	
	public void decrementarCantidadUsoThemesDisponibles() throws NumberFormatException, FileNotFoundException, IOException {
		//obtenemos la cantidad de veces que 
    	//el usuario todavia puede usar themes
    	//sin cliquear los ads
    	byte cantidadThemesDisponibles = Byte.parseByte(CubeApplication.data.obtenerValorConfig(DatabaseManager.CONFIGURATION_USOS_DISPONIBLES));

    	//decrementamos en uno
    	--cantidadThemesDisponibles;
    	
    	//guardamos en el archivo nuevamente
    	CubeApplication.data.actualizarValorConfig(DatabaseManager.CONFIGURATION_USOS_DISPONIBLES, String.valueOf(cantidadThemesDisponibles));
	}
	
	/**
	 * Devuelve la cantidad de veces que todavia
	 * quedan disponibles para usar themes que no
	 * sean el por defecto, antes que sea necesario
	 * presionar nuevamente sobre el ad
	 * @return
	 */
	public int obtenerCantidadUsoThemesDisponibles() {
		int cantidad = Integer.parseInt(CubeApplication.data.obtenerValorConfig(DatabaseManager.CONFIGURATION_USOS_DISPONIBLES));
		System.out.println("---------------> " + cantidad);
		return cantidad;
	}
	
	public void mostrarPopupHabilitarThemes() {
		if(!popupMostrando()) {
			if(AppContants.HABILITA_THEMES_CLIQUEANDO_PROPAGANDAS) {
				
				LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);  
			    View popupView = layoutInflater.inflate(R.layout.mensaje_simple_popup, null);  
			    popupWindow = new PopupWindow(popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);  
			    
			    //cargamos el mensaje a desplegar
			     String mensaje = getResources().getString(R.string.mensajeHabilitarThemes);

			    //layout del popup view
			    LinearLayout popupLayout = (LinearLayout)popupView.findViewById(R.id.linearLayourSImplePopupTxtView);
			    
			    //text field con el texto de pago 
			    //de licencia recibido por parametro
			    TextView mensajeTxtView = (TextView)popupView.findViewById(R.id.mensajeSimplePopupTxtView);
			    mensajeTxtView.setText(mensaje);
			    
			    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			    layoutParams.gravity = Gravity.CENTER;
			    
			    //creamos el layout contenedor de
			    //las imagenes de cada tema
			    LinearLayout themesCubeImagesLinearLayout = new LinearLayout(getApplicationContext());
			    themesCubeImagesLinearLayout.setLayoutParams(layoutParams);
			    
			    for(ThemePackage themePackage : ThemePackageReader.getThemePackages(getApplicationContext())) {
			    	
				    LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				    imageParams.gravity = Gravity.CENTER;
				    
				    //creamos el image view
				    ImageView androidImageView = new ImageView(getApplicationContext());
				    androidImageView.setPadding(0, 10, 10, 10);
				    androidImageView.setImageBitmap(obtenerThemePackageIcon(themePackage));
				    androidImageView.setLayoutParams(imageParams);
				    
				    //agregamos las imagenes al layout
				    themesCubeImagesLinearLayout.addView(androidImageView);
			    }
			    
			    //agregamos el layout con los image view
			    popupLayout.addView(themesCubeImagesLinearLayout, 1);
			    
			    //hacemos que relampaguee el adview asi
			    //el usuario nota donde hay que cliquear
			    relampaguearAdview();
			    
			    //boton para salir del popup
			    Button btnDismiss = (Button)popupView.findViewById(R.id.dismiss);
			    btnDismiss.setOnClickListener(new Button.OnClickListener(){
			    	@Override
			    	public void onClick(View v) {
				    	//detenemos el relampagueo
				    	detenerRelampaguearAdmobView();
			    		//cerramos el popup
			    		popupWindow.dismiss();
			    }});
			    
			    //mostramos el popup en la vista actual
			    popupWindow.showAtLocation(findViewById(R.id.menupager), Gravity.CENTER, 0, 0);
			    
			}
			else {
				
				LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);  
			    View popupView = layoutInflater.inflate(R.layout.mensaje_simple_popup, null);  
			    popupWindow = new PopupWindow(popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);  
			    
			    //layout del view
			    LinearLayout mensajeSimplePopupLayout = (LinearLayout)popupView.findViewById(R.id.linearLayoutMensajeSimplePopup);
			    
			    //layout del popup view
			    LinearLayout popupLayout = (LinearLayout)popupView.findViewById(R.id.linearLayourSImplePopupTxtView);
			    
			    //obtnemos el mensaje i18n
			    String mensaje = getResources().getString(R.string.mensajeThemesNoHabilitadoVersionFree);
			    
			    //text field con el texto de pago 
			    //de licencia recibido por parametro
			    TextView mensajeTxtView = (TextView)popupView.findViewById(R.id.mensajeSimplePopupTxtView);
			    mensajeTxtView.setText(mensaje);
			    
			    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			    layoutParams.gravity = Gravity.CENTER;
			    
			    //creamos el layout contenedor de
			    //las imagenes de cada tema
			    LinearLayout themesCubeImagesLinearLayout = new LinearLayout(getApplicationContext());
			    themesCubeImagesLinearLayout.setLayoutParams(layoutParams);
			    
			    for(ThemePackage themePackage : ThemePackageReader.getThemePackages(getApplicationContext())) {
			    	
				    LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				    imageParams.gravity = Gravity.CENTER;
				    
				    //creamos el image view
				    ImageView androidImageView = new ImageView(getApplicationContext());
				    androidImageView.setPadding(0, 10, 10, 10);
				    androidImageView.setImageBitmap(obtenerThemePackageIcon(themePackage));
				    androidImageView.setLayoutParams(imageParams);
				    
				    //agregamos las imagenes al layout
				    themesCubeImagesLinearLayout.addView(androidImageView);
			    }
			    
			    //agregamos el layout con los image view
			    popupLayout.addView(themesCubeImagesLinearLayout, 1);
			    
			    //boton para salir del popup
			    Button btnDismiss = (Button)popupView.findViewById(R.id.dismiss);
			    btnDismiss.setOnClickListener(new Button.OnClickListener(){
			    	@Override
			    	public void onClick(View v) {
			    		//cerramos el popup
			    		popupWindow.dismiss();
			    }});
			    
			    //mostramos el popup en la vista actual
			    popupWindow.showAtLocation(findViewById(R.id.menupager), Gravity.CENTER, 0, 0);
			    
			}			
		}
	}
	
	private Bitmap obtenerThemePackageIcon(ThemePackage themePackage) {
		
		//obtenemos la uri de la primer imagen
		//que conforma el cubo del tema
		//obtenemos el stream de datos
		Uri path = Uri.parse("android.resource://" + AppContants.packageName + themePackage.cubeImageUris[0]);
		InputStream in = null;
		try {
			in = getContentResolver().openInputStream(path);
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		
		//obtenemos el bitmap reducido en tamano
		Options opts = new Options();
		opts.inSampleSize = 8; //512/8 = 64 (bitmap de 64x64)
		Bitmap themePackageIcon = BitmapFactory.decodeStream(in, null, opts);
		
		return themePackageIcon;
	}

	@Override
	public void onBackPressed() {
		int vistaActual = obtenerVistaPagerActual();
		if(vistaActual == MenuPagerAdapter.MENU_MAIN) {
			//creamos un handler para que espere
			//un tiempo antes de cambiar el flag
			//que indica si la proxima vez que se
			//presione el boton back se sale de la
			//aplicacion
		    Handler backButtonTimerHandler = new Handler();
			backButtonTimerHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					exitOnBack = false;
				}
			}, 4000);
			
			if(!exitOnBack) {
				//mandamos mensaje 
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.back_to_exit), Toast.LENGTH_SHORT).show();
				//cambiamos flag 
				exitOnBack = true;
				//dont exit
				return;
			}
			else {
				if(interstitial != null && interstitial.isLoaded()) {
					displayInterstitial();
				}
				else {
					exitApplication();	
				}
				
				//cargamos un nuevo interstitial
				loadInterstitial();
				
			}
		}
		else if(vistaActual == MenuPagerAdapter.MENU_THEMES) {
			cambiarVista(MenuPagerAdapter.MENU_MAIN);
		}
	}
	
	private void exitApplication() {
		moveTaskToBack(true);
	}
	
	public void cambiarVista(final int vista) {
		if(vista == MenuPagerAdapter.MENU_MAIN) {
			contextoActual = CONTEXTO_MENU_PRINCIPAL;	
		}
		else if(vista == MenuPagerAdapter.MENU_THEMES) {
			contextoActual = CONTEXTO_THEMES;	
		}
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				//get the pager
				NonSwipeableViewPager pager = (NonSwipeableViewPager) findViewById(R.id.menupager);
				//move to the view
				pager.setCurrentItem(vista, true);
			}
		});
	}
	
	private int obtenerVistaPagerActual() {
		NonSwipeableViewPager pager = (NonSwipeableViewPager) findViewById(R.id.menupager);
		return pager.getCurrentItem();
	}
	
	private void relampaguearAdview() {
		
		DisplayMetrics outMetrics = getResources().getDisplayMetrics(); 
		int SCREEN_WIDTH = outMetrics.widthPixels;
		
		LinearLayout redArrowLayout = (LinearLayout) findViewById(R.id.arrowsContainer);
		redArrowLayout.setVisibility(View.VISIBLE);
		ImageView redArrow = (ImageView) redArrowLayout.getChildAt(0);
		
		TranslateAnimation translateAnimation = new TranslateAnimation(0, SCREEN_WIDTH/8, 0, 0);
		translateAnimation.setRepeatCount(Animation.INFINITE);
		translateAnimation.setRepeatMode(Animation.REVERSE);
		translateAnimation.setFillAfter(true);
		translateAnimation.setDuration(400);
		
		ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 1.3f, 1f, 1f);
		scaleAnimation.setFillAfter(true);
		scaleAnimation.setDuration(400);
		scaleAnimation.setRepeatCount(Animation.INFINITE);
		scaleAnimation.setRepeatMode(Animation.REVERSE);
		
		AnimationSet animationSet = new AnimationSet(true);
		animationSet.setInterpolator(new LinearInterpolator());
		animationSet.addAnimation(translateAnimation);
		animationSet.addAnimation(scaleAnimation);
		
		redArrow.startAnimation(animationSet);
		
	}
	
	public void seleccionarLiveWallpaper() {
		detenerRelampaguearAdmobView();
		Intent i = new Intent();
		if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN){
		    i.setAction(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
		    String p = AppContants.packageName;
		    String c = CubeThemeGenericLiveWallpaperService.class.getCanonicalName();
		    i.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, new ComponentName(p, c));
		}
		else{
		    i.setAction(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
		}
		startActivityForResult(i, REQUEST_CODE_SELECCIONAR_WALLPAPER);
	}
	
	public void compartirAplicacion() {
		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		String shareBody = getResources().getString(R.string.compartirContigo) + " market://details?id=" + AppContants.packageName;
		sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
		sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
		startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.compartirEn)));
	}
	
	private void detenerRelampaguearAdmobView() {
		LinearLayout redArrowLayout = (LinearLayout) findViewById(R.id.arrowsContainer);
		redArrowLayout.setVisibility(View.GONE);
	}
	
	public boolean popupMostrando() {
		if(popupWindow != null && popupWindow.isShowing()) {
			return true;
		}
		else if(popupWindow != null && !popupWindow.isShowing()) {
			return false;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Obtiene el id del paquete de usuario.
	 * En caso de que todavia no exista uno
	 * definido, devuelve el primero disponible
	 * y lo graba en el sistema 
	 * 
	 * @return
	 */
	public int obtenerPaqueteIdUsuario() {
		
		try {
			
			//obtenemos el flujo
			byte[] paqueteIdUsuarioData = FileUtil.obtenerContenidoStream(openFileInput("paqueteIdUsuario"));
			
			//transformamos el byte array
			//en el id que contiene el archivo
			int paqueteIdUsuario = Integer.parseInt(new String(paqueteIdUsuarioData));
			
			return paqueteIdUsuario;
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		//si llegamos aqui es porque dio
		//una excepcion tratando de leer
		//el archivo, por ende tenemos que
		//generar uno nuevo con un id predefinido
		int paqueteIdUsuarioPredefinido = ThemePackageReader.getThemePackages(getApplicationContext()).get(0).id;
		
		try {
			//guardamos el id
			FileUtil.guardarContenido(openFileOutput("paqueteIdUsuario", Context.MODE_PRIVATE), String.valueOf(paqueteIdUsuarioPredefinido).getBytes());
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return paqueteIdUsuarioPredefinido;
	}
}