package com.gmi.cube.base.util;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public class ImageUtil {

	public static Bitmap obtenerBitmapTamanoTextura(Bitmap bitmap, int limiteWidth, int limiteHeight) {
		
		int limiteWidthCalculado = get2Fold(bitmap.getWidth());
		int limiteHeightCalculado = get2Fold(bitmap.getHeight());
		
		if(limiteWidthCalculado > limiteWidth) {
			limiteWidthCalculado = limiteWidth;
		}
		
		if(limiteHeightCalculado > limiteHeight) {
			limiteHeightCalculado = limiteHeight;
		}
		
		//redimensionamos la imagen para que
		//las dimensiones de la misma sean
		//base 2 (256x256,512x512,1024x1024)
		bitmap = getResizedBitmap(bitmap, limiteWidthCalculado, limiteHeightCalculado);

		return bitmap;
	}

	/**
	 * Redimensiona el bitmap recibido al tamano
	 * indicado por parametro, y devuelve una 
	 * nueva instancia de bitmap con la imagen
	 * redimensionada
	 * 
	 * @param bm
	 * @param newHeight
	 * @param newWidth
	 * @return
	 */
	public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
	   
		int width = bm.getWidth();
	    int height = bm.getHeight();
	    float scaleWidth = ((float) newWidth) / width;
	    float scaleHeight = ((float) newHeight) / height;
	    // CREATE A MATRIX FOR THE MANIPULATION
	    Matrix matrix = new Matrix();
	    // RESIZE THE BIT MAP
	    matrix.postScale(scaleWidth, scaleHeight);

	    // "RECREATE" THE NEW BITMAP
	    Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
	    return resizedBitmap;
	}
	
    public static int get2Fold(int fold) {
        int ret = 2;
        while (ret < fold) {
            ret *= 2;
        }
        return ret;
    }
}