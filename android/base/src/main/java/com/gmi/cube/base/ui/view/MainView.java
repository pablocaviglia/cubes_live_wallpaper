package com.gmi.cube.base.ui.view;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.gmi.cube.base.R;
import com.gmi.cube.base.cubethemegenericlw.CubeThemeGenericLiveWallpaperActivity;
import com.gmi.cube.base.cubethemegenericlw.state.CubeThemeGenericLiveWallpaperState;
import com.gmi.cube.base.domain.EstadoBoton;
import com.gmi.cube.base.ui.adapter.MenuPagerAdapter;
import com.gmi.cube.base.util.MenuPrincipalLazyAdapter;

public class MainView extends CustomView {
	
	public MainView(final Context context) {
		
		super(context, R.layout.menu_principal);
		
        ListView list = (ListView)findViewById(R.id.menu_principal_list);
        MenuPrincipalLazyAdapter adapter = new MenuPrincipalLazyAdapter(context);
        list.setAdapter(adapter);
        
        //carga el contenido en el menu
        cargarContenidoListaMenu();
        
        //listener para cuando se presiona 
		//algun elemento de la lista
		list.setOnItemClickListener(new OnItemClickListener() {
			
			//obtenemos la actividad
			CubeThemeGenericLiveWallpaperActivity activity = (CubeThemeGenericLiveWallpaperActivity) getContext();
			
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				EstadoBoton estado = (EstadoBoton)view.getTag();
				if(estado.id == "boton_elegir_temas") {
					((CubeThemeGenericLiveWallpaperActivity)context).cambiarVista(MenuPagerAdapter.MENU_THEMES);
				}
				else if(estado.id == "boton_cubo_habilitado") {
					
					boolean cuboHabilitado = activity.cuboHabilitado();
					activity.habilitarCubo(!cuboHabilitado);
					
					int idMensajeToast = cuboHabilitado ? R.string.cuboDeshabilitado : R.string.cuboHabilitado;
					Toast.makeText(context, getResources().getString(idMensajeToast), Toast.LENGTH_SHORT).show();
					
					//recargamos vista
					cargarContenidoListaMenu();
					
					//cambia el estado de la
					//renderizacion del cubo
					CubeThemeGenericLiveWallpaperState.cuboHabilitado = !cuboHabilitado;
					
				}
				else if(estado.id == "boton_seleccionar_live_wallpaper") {
					activity.seleccionarLiveWallpaper();
				}
				else if(estado.id == "boton_compartir_live_wallpaper") {
					activity.compartirAplicacion();
				}
			}
		});
		
	}
	
	private void cargarContenidoListaMenu() {
		
	    //carga la lista con la data
		ArrayList<HashMap<String, Object>> elementsList = new ArrayList<HashMap<String, Object>>();
		
		//boton elegir imagenes
		HashMap<String, Object> elegirImagenesMap = new HashMap<String, Object>();
	    elegirImagenesMap.put("title", getResources().getString(R.string.elegirTemas));
	    elegirImagenesMap.put("desc", getResources().getString(R.string.elegirTemasDesc));
        elegirImagenesMap.put("thumb_bitmap", null);
		elegirImagenesMap.put("estado", new EstadoBoton("boton_elegir_temas", true));
        
        elementsList.add(elegirImagenesMap);

		//boton seleccionar live wallpaper
		HashMap<String, Object> seleccionarLiveWallpaperMap = new HashMap<String, Object>();
		seleccionarLiveWallpaperMap.put("estado", new EstadoBoton("boton_seleccionar_live_wallpaper", true));
		seleccionarLiveWallpaperMap.put("title", getResources().getString(R.string.seleccionarLiveWallpaper));
		seleccionarLiveWallpaperMap.put("desc", getResources().getString(R.string.seleccionarLiveWallpaperDesc));
		seleccionarLiveWallpaperMap.put("thumb_bitmap", null);
        elementsList.add(seleccionarLiveWallpaperMap);

		//boton cubo habilitado
		HashMap<String, Object> cuboHabilitadoMap = new HashMap<String, Object>();
		cuboHabilitadoMap.put("estado", new EstadoBoton("boton_cubo_habilitado", true));
		
		//obtenemos la actividad
		CubeThemeGenericLiveWallpaperActivity activity = (CubeThemeGenericLiveWallpaperActivity) getContext();
		boolean cuboHabilitado = activity.cuboHabilitado();
		if(cuboHabilitado) {
			cuboHabilitadoMap.put("title", getResources().getString(R.string.deshabilitarCubo));
			cuboHabilitadoMap.put("desc", getResources().getString(R.string.deshabilitarCuboDesc));
		}
		else {
			cuboHabilitadoMap.put("title", getResources().getString(R.string.habilitarCubo));
			cuboHabilitadoMap.put("desc", getResources().getString(R.string.habilitarCuboDesc));
		}
        elementsList.add(cuboHabilitadoMap);
        
        //boton compartir live wallpaper
		HashMap<String, Object> compartirLiveWallpaperMap = new HashMap<String, Object>();
		compartirLiveWallpaperMap.put("estado", new EstadoBoton("boton_compartir_live_wallpaper", true));
		compartirLiveWallpaperMap.put("title", getResources().getString(R.string.compartir));
		compartirLiveWallpaperMap.put("desc", getResources().getString(R.string.compartirDesc));
		compartirLiveWallpaperMap.put("thumb_bitmap", BitmapFactory.decodeResource(getResources(), R.drawable.share_icon));
        elementsList.add(compartirLiveWallpaperMap);
        
//        if(!AppContants.aplicacionPaga) {
//        	HashMap<String, Object> comprarLiveWallpaperMap = new HashMap<String, Object>();
//    		comprarLiveWallpaperMap.put("estado", new EstadoBoton("boton_comprar_live_wallpaper", true));
//    		comprarLiveWallpaperMap.put("title", getResources().getString(R.string.comprar));
//    		comprarLiveWallpaperMap.put("desc", getResources().getString(R.string.comprarDesc));
//    		comprarLiveWallpaperMap.put("thumb_bitmap", BitmapFactory.decodeResource(getResources(), R.drawable.shop_icon));
//            elementsList.add(comprarLiveWallpaperMap);
//        }
        
        //obtenemos la lista contenedora del menu
        ListView list = (ListView)findViewById(R.id.menu_principal_list);
        MenuPrincipalLazyAdapter adapter = (MenuPrincipalLazyAdapter)list.getAdapter();
        adapter.setData(elementsList);
        
	}
}
