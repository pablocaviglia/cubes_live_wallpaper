package com.gmi.cube.base.themePackage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.net.Uri;

import com.gmi.cube.base.domain.ThemePackage;
import com.gmi.cube.base.util.AppContants;

public class ThemePackageReader {
	
	private static List<ThemePackage> themePackages;
	
	/**
	 * Comienza la definicion de los paquetes
	 */
	private static void addDefinition(String line) {
		
		//dividimos la linea por comas
		String[] camposDefinicion = line.split(",");
		
		String themeId = camposDefinicion[0].trim();
		String themeName = camposDefinicion[1].trim();
		String themeDescription = camposDefinicion[2].trim();
		String themeBackround = camposDefinicion[3].trim();
		
		String themesUnparsedDiceFaces = camposDefinicion[4];
		String[] themesDiceFacesArray = themesUnparsedDiceFaces.split("@");
		for(int i=0; i<themesDiceFacesArray.length; i++) {
			themesDiceFacesArray[i] = themesDiceFacesArray[i].trim();
		}
		
		themePackages.add(new ThemePackage(Integer.parseInt(themeId), themeName, themeDescription, themeBackround, themesDiceFacesArray));
	}
	
	public static List<ThemePackage> getThemePackages(Context applicationContext) {
		
		if(themePackages == null) {
			
			themePackages = new ArrayList<ThemePackage>();
			
			try {

				Uri path = Uri.parse("android.resource://" + AppContants.packageName + "/raw/packages_definition");
				InputStream packagesDefinitionStream = applicationContext.getContentResolver().openInputStream(path);
				
				//para leer de a una linea
				BufferedReader br = new BufferedReader(new InputStreamReader(packagesDefinitionStream));
			
				String line = null;
				while((line = br.readLine()) != null) {
				    
					//procesamos la linea
					addDefinition(line);
				}
			} 
			catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		
		return themePackages;
	}
}