package com.gmi.cube.base.domain;

public class ThemePackage {

	public int id;
	public String nameId;
	public String descriptionId;
	public String backgroundUri;
	public String[] cubeImageUris;
	
	public ThemePackage() {
	}
	
	public ThemePackage(int id, String nameId, String descriptionId, String backgroundUri, String[] cubeImageUris) {
		this.id = id;
		this.nameId = nameId;
		this.descriptionId = descriptionId;
		this.backgroundUri = backgroundUri;
		this.cubeImageUris = cubeImageUris;
	}
}