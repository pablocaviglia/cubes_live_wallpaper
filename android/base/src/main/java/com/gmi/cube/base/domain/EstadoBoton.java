package com.gmi.cube.base.domain;

public class EstadoBoton {

	public String id;
	private boolean habilitado;

	public EstadoBoton(String id, boolean habilitado) {
		this.id = id;
		this.habilitado = habilitado;
	}
}
