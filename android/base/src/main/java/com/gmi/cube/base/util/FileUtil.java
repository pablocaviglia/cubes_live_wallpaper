package com.gmi.cube.base.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtil {

	public static byte[] obtenerContenidoStream(InputStream inputStream) throws IOException {
	
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		int i = -1;
		byte[] buffer = new byte[1024];
		while((i = inputStream.read(buffer)) != -1) {
			baos.write(buffer,0 , i);
		}
		
		baos.flush();
		baos.close();
		inputStream.close();
		
		return baos.toByteArray();
	}
	
	public static void guardarContenido(OutputStream outputStream, byte[] data) throws IOException {
		
		outputStream.write(data);
		outputStream.flush();
		outputStream.close();
	}
}
