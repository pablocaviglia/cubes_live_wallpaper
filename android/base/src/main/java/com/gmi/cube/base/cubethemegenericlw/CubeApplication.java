package com.gmi.cube.base.cubethemegenericlw;

import android.app.Application;

import com.gmi.cube.base.database.DatabaseManager;

public class CubeApplication extends Application {
	
	//persistence
	public static DatabaseManager data;
	
	@Override
	public void onCreate() {
		super.onCreate();
		data = new DatabaseManager(this.getApplicationContext());
	}
}