package com.gmi.cube.base.cubethemegenericlw.state;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.SensorManager;
import android.net.Uri;

import com.gmi.cube.base.domain.ThemePackage;
import com.gmi.cube.base.themePackage.ThemePackageReader;
import com.gmi.cube.base.threed.SimpleCube;
import com.gmi.cube.base.threed.TexturedFacesCube;
import com.gmi.cube.base.util.AbstractState;
import com.gmi.cube.base.util.AppContants;
import com.gmi.cube.base.util.FileUtil;
import com.gmi.cube.base.util.Timer;
import com.gmi.cube.base.wallpaper.GLWallpaperService;
import com.threed.jpct.Camera;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Light;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;

public class CubeThemeGenericLiveWallpaperState extends AbstractState {
	
	//flag que indica si el cubo
	//debe de renderizarse
	public static boolean cuboHabilitado = true;
	
	//variables usadas para la logica
	//del mantenimiento del tamano del
	//cubo a traves del delay
	private boolean delayEscaladoCubo;
	private Timer delayEscaladoCuboTimer = new Timer(0);

	//coordenadas de drag 
	//anterior al actual
	private float oldDraggedX;
	private float oldDraggedY;
	private float oldDraggedX2;
	private float oldDraggedY2;

	private Random random = new Random();
	
	private Timer giroAleatorioTimer = new Timer(3000);
	private boolean girarCuboX = true;
	private boolean girarCuboY = true;
	private boolean girarCuboZ = true;
	private float tiempoGiroCubo = 5000f;
	
	private long accelerometerLastUpdate;

	public World world = null;
	public TexturedFacesCube photosCube = null;
	private Light sun = null;

	//indica si el cubo esta siendo
	//manejado por el usuario
	private boolean manejandoCubo;

	//grados de rotacion que lleva
	//cada eje del cubo
	private double gradosGiradosX;
	private double gradosGiradosY;
	private double gradosGiradosZ;

	//variables usadas para el efecto
	//de rotacion rapida del cubo
	private boolean rotacionRapidaCubo;
	private Timer efectoRotacionRapidaCuboTimer = new Timer(1800);

	//variables relacionadas al calculo
	//de los bordes de la pantalla
	private boolean moverCuboBordes;
	private float medioLimiteHorizontal;
	private float medioLimiteVertical;
	private boolean mediosLimitesCalculados;
	private boolean trasladandoCuboXDerecha;
	private boolean trasladandoCuboYAbajo;
	private Timer traslacionTimer = new Timer(5);
	private Timer moverCuboBordesTimer = new Timer(1500);
	private SimpleCube borderHorizontalCheckCube;
	private SimpleCube borderVerticalCheckCube;
	private SimpleCube borderHorizontalHelperCheckCube;
	private SimpleCube borderVerticalHelperCheckCube;

	//variables de centrado de cubo en ejes x,y
	private boolean centrarCubo;
	private Timer centradoCuboTimer = new Timer(400);
	private float traslacionTotalAnteriorX;
	private float traslacionTotalAnteriorY;
	private float traslacionOriginalX;
	private float traslacionOriginalY;
	
	private String[] cubeSides;

	//bitmap del fondo estatico
	private Texture fondoTexture;
	private int fondoTextureWidth;
	private int fondoTextureHeight;
	
	//cantidad de pantallas virtuales
	//que existen en el escritorio
	private int cantidadPantallasVirtuales;
	private float xOffsetPantallasVirtuales;

	//util
	private SimpleVector touchVector;

	//escalamiento del cubo
	private final float ESCALAMIENTO_MAXIMO_CUBO = 0.5f;
	private boolean accionEscalamientoCuboAcercado = false;
	private boolean escalarCubo;
	private Timer escalarCuboTimer = new Timer(250);

	public void render(FrameBuffer fb) {
		
		if(world != null) {

			if(cuboHabilitado) {
				photosCube.setVisibility(Object3D.OBJ_VISIBLE);
			}
			else {
				photosCube.setVisibility(Object3D.OBJ_INVISIBLE);
			}

			//renderiza el fondo
			renderizarFondo(fb);
			
			//calculamos los bordes del mundo
			calcularBordes();
			
			//dibujamos el mundo
			world.draw(fb);

			//renderizamos la escena
			world.renderScene(fb);
			
		}
	}
	
	public void update(long elapsedTime) {

		//logica que aproxima y aleja
		//el cubo, usado cuando se 
		//manipula el cubo por el usuario
		logicaEscalarCubo(elapsedTime);
		
		//logica que evalua si el cubo debe
		//de mantenerse escalado en grande 
		//por el delay configurado
		logicaDelayEscaladoCubo(elapsedTime);
		
		//logica que mueve los cubos que
		//ayudan a calcular cuales son los
		//bores de la pantalla
		logicaMoverCuboBordes(elapsedTime);
		
		//logica de rotacion aleatoria
		logicaRotacionAleatoriaCubo(elapsedTime);
		
		//logica de centrado de cubo
		logicaCentradoCubo(elapsedTime);
		
	}
	
	private void renderizarFondo(FrameBuffer fb) {
		
		int srcX = 0;
		int srcY = 0;
		int dstX = 0;
		int dstY = 0;
		int srcWidth = fondoTextureWidth;
		int srcHeight = fondoTextureHeight;
		int dstWidth = fondoTextureWidth;
		int dstHeight = fondoTextureHeight;
		
		//ajustamos el alto y el ancho 
		//a la dimension del dispositivo
		float porcentajeAjuste = ((float)screenHeight) / ((float)fondoTextureHeight);
		dstWidth = (int)(((float)dstWidth) * porcentajeAjuste);
		dstHeight = (int)(((float)dstHeight) * porcentajeAjuste);
		
		//contiene los pixels a mover debido
		//al scrolling del escritorio
		if(cantidadPantallasVirtuales > 1) {
			//calculamos el movimiento
			dstX = (int)((dstWidth - screenWidth) * -xOffsetPantallasVirtuales);
		}
		else {
			//solo una pantalla virtual,
			//centramos la imagen
			dstX = (dstWidth - GLWallpaperService.width) / -2;
		}
		
		if(fondoTexture != null) {
			try {
				fb.blit(fondoTexture, srcX, srcY, dstX, dstY, srcWidth, srcHeight, dstWidth, dstHeight, -1, false);
			} 
			catch (OutOfMemoryError e) {
				descargarTexturaFondo();
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Toda la logica relacionada al giro
	 * aleatorio del cubo
	 * 
	 * @param elapsedTime
	 */
	private void logicaRotacionAleatoriaCubo(long elapsedTime) {
		
		if(!manejandoCubo) {
			
			float giroGrados = (float)elapsedTime * 360f / tiempoGiroCubo;
			
			if(girarCuboX) {
				rotarCubo((rotacionRapidaCubo ? (random.nextFloat() * 1.2f * giroGrados) : giroGrados), (byte)0);
			}
			
			if(girarCuboY) {
				rotarCubo((rotacionRapidaCubo ? (random.nextFloat() * 1.2f * giroGrados) : giroGrados), (byte)1);
			}
			
			if(girarCuboZ) {
				rotarCubo((rotacionRapidaCubo ? (random.nextFloat() * 1.2f * giroGrados) : giroGrados), (byte)2);
			}
			
			if(rotacionRapidaCubo) {
				
				//cambiamos el tiempo aleatorio de giro
				tiempoGiroCubo = 300;
				
				//aleatoriamente elegimos que
				//ejes vamos a girar
				girarCuboX = true;
				girarCuboY = true;
				girarCuboZ = true;
				
				//si se cumple el tiempo del
				//efecto lo finalizamos y seguimos
				//la rotacion estandar
				if(efectoRotacionRapidaCuboTimer.action(elapsedTime)) {

					rotacionRapidaCubo = false;
					
					//cambiamos el tiempo aleatorio de giro
					tiempoGiroCubo = (15 + (random.nextFloat() * 5f)) * 1000f;
					giroAleatorioTimer.setCurrentTick(giroAleatorioTimer.getDelay()-100);
				}
			}
			else {
				
				if(giroAleatorioTimer.action(elapsedTime)) {
					
					//cambiamos el tiempo aleatorio de giro
					tiempoGiroCubo = (15 + (random.nextFloat() * 5f)) * 1000f;
					
					//aleatoriamente elegimos que
					//ejes vamos a girar
					girarCuboX = random.nextBoolean();
					girarCuboY = random.nextBoolean();
					girarCuboZ = random.nextBoolean();
					
					//si ningun eje gira, elegimos uno
					if(!girarCuboX && !girarCuboY && !girarCuboZ) {
						girarCuboX = true;
					}
					
					giroAleatorioTimer.setDelay((long)tiempoGiroCubo);
					
				}
			}
		}
	}
	
	private void calcularBordes() {
		
		if(borderHorizontalCheckCube != null) {

			if(borderHorizontalCheckCube.wasVisible()) {

				trasladandoCuboXDerecha = true;

				borderHorizontalCheckCube.translate(1f, 0, 0);
				medioLimiteHorizontal = borderHorizontalCheckCube.getTranslation().x - (photosCube.size * 2);
			}
			else {
				if(!mediosLimitesCalculados) {
					mediosLimitesCalculados = true;
				}
			}
		}
		
		if(borderVerticalCheckCube != null) {
			
			if(borderVerticalCheckCube.wasVisible()) {

				trasladandoCuboYAbajo = true;

				borderVerticalCheckCube.translate(0, 1f, 0);
				medioLimiteVertical = borderVerticalCheckCube.getTranslation().y - (photosCube.size * 2);
			}
			else {
				if(!mediosLimitesCalculados) {
					mediosLimitesCalculados = true;
				}
			}
		}

		//este solo es para que quede bien el
		//efecto de que salgan 4 cubos
		if(borderHorizontalHelperCheckCube != null) {
			if(borderHorizontalHelperCheckCube.wasVisible()) {
				borderHorizontalHelperCheckCube.translate(-1f, 0, 0);
			}
		}
		
		if(borderVerticalHelperCheckCube != null) {
			if(borderVerticalHelperCheckCube.wasVisible()) {
				borderVerticalHelperCheckCube.translate(0, -1f, 0);
			}
		}
	}
	
	/**
	 * Logica que evalua si el cubo debe de
	 * mantenerse escalado el cubo en grande
	 * un tiempo determinado luego de que el
	 * cubo ha dejado de arrastrarse por el 
	 * usuario
	 * 
	 * @param elapsedTime
	 */
	private void logicaDelayEscaladoCubo(long elapsedTime) {

		if(delayEscaladoCubo && !manejandoCubo) {
			
			if(delayEscaladoCuboTimer.action(elapsedTime)) {
				
				//seteamos flag
				delayEscaladoCubo = false;
				
				//achicamos el cubo nuevamente
				escalarCubo(false, 0);
			}
		}
	}
	
	/**
	 * Logica de escalamiento de cubo cuando
	 * el usuario empieza y termina de 
	 * interactuar con el cubo
	 */
	private void logicaEscalarCubo(long elapsedTime) {
		
		if(escalarCubo) {
			
			//obtenemos el tiempo transcurrido
			long tiempoTranscurrido = escalarCuboTimer.getCurrentTick();
			long tiempoTotal = escalarCuboTimer.getDelay();
			
			//calculamos el escalamiento
			float escalamientoActual = (tiempoTranscurrido * ESCALAMIENTO_MAXIMO_CUBO) / tiempoTotal;
			
			if(accionEscalamientoCuboAcercado) {
				escalamientoActual = 1 + escalamientoActual;
			}
			else {
				escalamientoActual = 1 + ESCALAMIENTO_MAXIMO_CUBO - escalamientoActual;
			}
			
			//seteamos el escalamiento
			photosCube.setScale(escalamientoActual);

			if(escalarCuboTimer.action(elapsedTime)) {
				
				//ajustamos el escalamiento
				if(accionEscalamientoCuboAcercado) {
					photosCube.setScale(1 + ESCALAMIENTO_MAXIMO_CUBO);
				}
				else {
					photosCube.setScale(1);
				}
				
				//setea en false en flag asi
				//no se sigue escalando
				escalarCubo = false;
				
			}
		}
	}
	
	
	private void escalarCubo(boolean acercar, long delayEscalamiento) {
		
		//si la nueva accion de escalamiento es
		//diferente a la que ya se encuentra el cubo
		if(accionEscalamientoCuboAcercado != acercar) {
			escalarCuboTimer.refresh();
			escalarCubo = true;
			accionEscalamientoCuboAcercado = acercar;
		}
		
		//si la accion es acercar entonces
		//mantenemos el escalamiento con delay
		//porque puede ser que el usuario desee
		//mantener el cubo grande
		if(acercar) {
			mantenerEscalaCuboDelay(delayEscalamiento);
		}
	}
	
	private void mantenerEscalaCuboDelay(long delayEscalamiento) {
		
		if(delayEscalamiento > 0) {
			delayEscaladoCuboTimer.setDelay(delayEscalamiento);
			delayEscaladoCuboTimer.refresh();
			delayEscaladoCubo = true;
		}
	}

	/**
	 * Logica de movimiento del cubo hasta los bordes
	 */
	private void logicaMoverCuboBordes(long elapsedTime) {
		
		if(moverCuboBordes) {
			
			if(traslacionTimer.action(elapsedTime)) {
				
				//si ya tenemos los limites calculados
				//procedemos a mover el cubo
				if(medioLimiteHorizontal != 0 && medioLimiteVertical != 0) {
					
					if(trasladandoCuboXDerecha) {
						
						if(photosCube.getTranslation().x < medioLimiteHorizontal) {
							photosCube.translate((float)Math.random(), 0, 0);
						}
						else {
							trasladandoCuboXDerecha = false;
						}
					}
					else {
						
						if(photosCube.getTranslation().x > -medioLimiteHorizontal) {
							photosCube.translate((float)-Math.random(), 0, 0);
						}
						else {
							trasladandoCuboXDerecha = true;
						}
					}

					if(trasladandoCuboYAbajo) {
						
						if(photosCube.getTranslation().y < medioLimiteVertical) {
							photosCube.translate(0, (float)Math.random(), 0);
						}
						else {
							trasladandoCuboYAbajo = false;
						}
					}
					else {
						
						if(photosCube.getTranslation().y > -medioLimiteVertical) {
							photosCube.translate(0, (float)-Math.random(), 0);
						}
						else {
							trasladandoCuboYAbajo = true;
						}
					}
				}
			}

			if(moverCuboBordesTimer.action(elapsedTime)) {
				
				//setea en false en flag asi
				//no se sigue moviendo
				moverCuboBordes = false;
				
				//ejecuta la logica q centra 
				//el cubo en los ejes x,y
				centrarCubo();

			}
		}
	}
	
	/**
	 * Logica relacionada al centrado del cubo
	 */
	private void logicaCentradoCubo(long elapsedTime) {
		
		if(centrarCubo) {
			
			//obtenemos el vector de 
			//traslacion del cubo
			SimpleVector translation = photosCube.getTranslation();
			
			float tiempoTraslacionTotal = centradoCuboTimer.getDelay();
			float tiempoTraslacionActual = centradoCuboTimer.getCurrentTick();

			float traslacionXActual = (tiempoTraslacionActual * traslacionOriginalX) / tiempoTraslacionTotal;
			float traslacionYActual = (tiempoTraslacionActual * traslacionOriginalY) / tiempoTraslacionTotal;
			
			float difx = 0;
			float dify = 0;
			
			if(traslacionTotalAnteriorX != 0) {
				difx = (traslacionTotalAnteriorX - traslacionXActual);
			}
			
			if(traslacionTotalAnteriorY != 0) {
				dify = (traslacionTotalAnteriorY - traslacionYActual);
			}

			//trasladamos el cubo
			photosCube.translate(difx, dify, translation.z);
			
			//guardamos la distancia para calcular
			//la diferencia de traslacion en la 
			//proxima iteracion
			traslacionTotalAnteriorX = traslacionXActual;
			traslacionTotalAnteriorY = traslacionYActual;
			
			//tiempo de centrado cumplido
			if(centradoCuboTimer.action(elapsedTime)) {
				
				liveWallpaper.volverFPSOriginal();
				photosCube.translate(-translation.x, -translation.y, translation.z);
				
				centrarCubo = false;
				traslacionTotalAnteriorX = 0;
				traslacionTotalAnteriorY = 0;

			}
		}
	}
	
	/**
	 * Vuelve a centrar el cubo en los ejes X,Y
	 */
	private void centrarCubo() {
		
		traslacionOriginalX = photosCube.getTranslation().x;
		traslacionOriginalY = photosCube.getTranslation().y;
		centradoCuboTimer.refresh();
		centrarCubo = true;
	}
	
	private void moverCuboBordes() {
		traslacionTimer.refresh();
		moverCuboBordesTimer.refresh();
		centrarCubo = false;
		moverCuboBordes = true;
	}
	
	private void rotarCubo(float grados, byte eje) {
		
		//transformamos a radianes
		float radians = (float)Math.toRadians(grados);
		
		if(eje == 0) {
			
			photosCube.rotateX(radians);

			//guardamos referencia de cuantos 
			//grados giro en el eje
			gradosGiradosX += grados;
			if(gradosGiradosX > 360) {
				gradosGiradosX -= 360;
			}
		}
		else if(eje == 1) {
			
			photosCube.rotateY(radians);

			//guardamos referencia de cuantos 
			//grados giro en el eje
			gradosGiradosY += grados;
			if(gradosGiradosY > 360) {
				gradosGiradosY -= 360;
			}
		}
		else if(eje == 2) {

			photosCube.rotateY(radians);

			//guardamos referencia de cuantos 
			//grados giro en el eje
			gradosGiradosZ += grados;
			if(gradosGiradosZ > 360) {
				gradosGiradosZ -= 360;
			}
		}
	}
	
	private boolean intersectCube(int x, int y) {
		
		if(photosCube != null) {
			
			//transformamos las coordenadas
			//2d del evento al mundo 3d
			x = x - (screenWidth / 2);
			y = y - (screenHeight / 2);
			
			SimpleVector cubePosition = photosCube.getTransformedCenter();
			
			if(touchVector == null) {
				touchVector = new SimpleVector();
			}
			
			touchVector.x = x;
			touchVector.y = y;
			touchVector.z = cubePosition.z;

			return photosCube.sphereIntersectsAABB(touchVector, 100);
		}
		else {
			
			return false;
		}
	}
	
	
	/**
	 * Inicia la logica para rotar rapidamente
	 * el cubo
	 */
	private void iniciarRotacionRapidaCubo() {
		
		liveWallpaper.cambiarFPS(200);
		efectoRotacionRapidaCuboTimer.refresh();
		giroAleatorioTimer.refresh();
		rotacionRapidaCubo = true;
	}

	@Override
	public void touchDragged(float x, float y, float x2, float y2) {

		//indicamos que el cubo lo
		//maneja el usuario con el touch
		manejandoCubo = true;
		
		//esto lo hacemos para que el primer
		//drag no haga un movimiento brusco
		//durante la rotacion del cubo
		if(oldDraggedX == 0) {
			oldDraggedX = x;
		}
		if(oldDraggedY == 0) {
			oldDraggedY = y;
		}
		if(oldDraggedX2 == 0) {
			oldDraggedX2 = x2;
		}
		if(oldDraggedY2 == 0) {
			oldDraggedY2 = y2;
		}
		
		//calculamos la diferencia en px
		//entre el drag anterior y el actual
		float difx = x - oldDraggedX;
		float dify = y - oldDraggedY;
		float difz = (x2+y2) - (oldDraggedX2 + oldDraggedY2);
		
		//little fix, no quiero arreglarlo bien
		//quiero q salga y venderlo de una puta vez
		if(Math.abs(difz) > 30) {
			difz = 0;
		}
		if(Math.abs(dify) > 30) {
			dify = 0;
		}
		if(Math.abs(difx) > 30) {
			difx = 0;
		}
		
		//si existe rotacion en eje z verificamos
		//hacia que lado debemos rotar en base a 
		//la posicion en la pantalla en la que se
		//encuentra el dedo z-rotador
		boolean zRotaIzquierda = true;
		if(difz != 0 && x2 > (screenWidth / 2)) {
			zRotaIzquierda = false;
		}
		
		//rotamos el cubo en los ejes en
		//base a la diferencia de px
		float rotz = (float)Math.toRadians(difz * (zRotaIzquierda ? 1.03f : -1.03f));
		float roty = (float)Math.toRadians(difx * 1.03f);
		float rotx = (float)Math.toRadians(dify * 1.03f);
		photosCube.rotateZ(rotz);
		photosCube.rotateY(-roty);
		photosCube.rotateX(-rotx);

		//guardamos las coordenadas 
		//anteriores de dragging
		oldDraggedX = x;
		oldDraggedY = y;
		oldDraggedX2 = x2;
		oldDraggedY2 = y2;
		
	}

	@Override
	public void touchDragStart(int x, int y) {
		
		//si se comienza a realizar dragging
		//sobre el cubo, realizamos el 
		//escalamiento del mismo
		if(intersectCube((int)x, (int)y)) {
			escalarCubo(true, 2300);
		}
	}

	@Override
	public void touchDragFinish(int x, int y) {
		
		oldDraggedX = 0;
		oldDraggedY = 0;
		oldDraggedX2 = 0;
		oldDraggedY2 = 0;
		
		//se deja de manejar el 
		//cubo por el usuario
		manejandoCubo = false;
			
	}

	@Override
	public void touchClick(int x, int y) {

		if(intersectCube(x, y)) {

			//movemos rapido por toda la pantalla
			moverCuboBordes();
			
			//iniciamos la rotacion rapida del cubo
			iniciarRotacionRapidaCubo();

		}
	}
	
	@Override
	public void inicializar() {

		System.out.println("--------------------- INICIALIZANDO LIVE WALLPAPER");
		
		if(world != null) {
			
			//removemos todos los objetos
			//del mundo y lo finalizamos
			world.removeAll();
			world.dispose();
			world = null;
		}
		
		//creamos el mundo JPCT
		world = new World();
		world.setAmbientLight(120, 120, 120);
		
		sun = new Light(world);
		sun.setIntensity(255, 255, 255);
		
		//quitamos todas las textuas
		//del manager ya que las vamos
		//a cargar nuevamente
		FrameBuffer fb = obtenerFramebuffer();
		TextureManager textureManager = TextureManager.getInstance();
		Iterator<String> textureNamesIt = textureManager.getNames().iterator();
		while(textureNamesIt.hasNext()) {
			textureManager.removeAndUnload(textureNamesIt.next(), fb);
		}
		
		//agregamos el cubo con la textura
		inicializarPhotoCube();
		
		//configuramos la posicion del sol
		SimpleVector sv = new SimpleVector();
		sv.set(photosCube.getTransformedCenter());
		sv.y = 0;
		sv.z -= 150;
		sun.setPosition(sv);
		
		//inicializamos el fondo
		inicializarFondo();
		
	}
	
	private void inicializarCubosCalculoBordes() {

		if(world.getObjectByName("borderHorizontalCheckCube") != null) {
			
			Object3D obj = world.getObjectByName("borderHorizontalCheckCube");
			obj.getXAxis().x = photosCube.getXAxis().x;
			obj.getYAxis().y = photosCube.getYAxis().y;
		}
		else {
			
			borderHorizontalCheckCube = new SimpleCube(photosCube.size);
			borderHorizontalCheckCube.setName("borderHorizontalCheckCube");
			borderHorizontalCheckCube.setTransparency(0);
			
			//agregamos el cube principal al mundo
			world.addObject(borderHorizontalCheckCube);
		}
		
		if(world.getObjectByName("borderVerticalCheckCube") != null) {
			
			Object3D obj = world.getObjectByName("borderVerticalCheckCube");
			obj.getXAxis().x = photosCube.getXAxis().x;
			obj.getYAxis().y = photosCube.getYAxis().y;
		}
		else {
			
			borderVerticalCheckCube = new SimpleCube(photosCube.size);
			borderVerticalCheckCube.setName("borderVerticalCheckCube");
			borderVerticalCheckCube.setTransparency(0);
			
			//agregamos el cube principal al mundo
			world.addObject(borderVerticalCheckCube);
		}
		
		if(world.getObjectByName("borderHorizontalHelperCheckCube") != null) {
			
			Object3D obj = world.getObjectByName("borderHorizontalHelperCheckCube");
			obj.getXAxis().x = photosCube.getXAxis().x;
			obj.getYAxis().y = photosCube.getYAxis().y;
		}
		else {
			
			borderHorizontalHelperCheckCube = new SimpleCube(photosCube.size);
			borderHorizontalHelperCheckCube.setName("borderHorizontalHelperCheckCube");
			borderHorizontalHelperCheckCube.setTransparency(0);
			
			//agregamos el cube principal al mundo
			world.addObject(borderHorizontalHelperCheckCube);
		}
		
		if(world.getObjectByName("borderVerticalHelperCheckCube") != null) {
			
			Object3D obj = world.getObjectByName("borderVerticalHelperCheckCube");
			obj.getXAxis().x = photosCube.getXAxis().x;
			obj.getYAxis().y = photosCube.getYAxis().y;
		}
		else {
			
			borderVerticalHelperCheckCube = new SimpleCube(photosCube.size);
			borderVerticalHelperCheckCube.setName("borderVerticalHelperCheckCube");
			borderVerticalHelperCheckCube.setTransparency(0);
			
			//agregamos el cube principal al mundo
			world.addObject(borderVerticalHelperCheckCube);
		}
		
		mediosLimitesCalculados = false;
	}
	
	public void inicializarFondo() {
		
		try {
			
			// obtenems el manejador de texturas
			TextureManager textureManager = TextureManager.getInstance();

			//tmp
			InputStream fondoUsuarioStream = null;
			Bitmap fondoBitmap = null;
			
			//obtenemos el theme package actual
			ThemePackage themePackageUsuario = obtenerPaqueteActual();
			
			try {

				//obtenemos el stream de datos
				Uri path = Uri.parse("android.resource://" + AppContants.packageName + themePackageUsuario.backgroundUri);
				fondoUsuarioStream = liveWallpaper.getContentResolver().openInputStream(path);
				
			}
			catch (FileNotFoundException e) {
			}
			
			//el fondo del usuario existe 
			//por lo tanto cargamos ese 
			if(fondoUsuarioStream != null) {
				
				//cargamos desde el stream
				//el fondo del usuario
				fondoBitmap = BitmapFactory.decodeStream(fondoUsuarioStream);
				fondoUsuarioStream.close();
			}
			
			//descargamos la textura anterior
			//en caso de que existiera
			descargarTexturaFondo();
			
			//obtenemos las medidas del bitmap
			int fondoWidth = fondoBitmap.getWidth();
			int fondoHeight = fondoBitmap.getHeight();
			
			//calculamos las medidas de la textura
			fondoTextureWidth = fondoWidth;
			fondoTextureHeight = fondoHeight;
			
			//creamos la textura
			fondoTexture = new Texture(fondoBitmap);
			
			//cargamos la textura en el manager
			textureManager.addTexture("fondoTexture");
			
			//removemos de memoria
			fondoBitmap.recycle();
			fondoBitmap = null;
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void inicializarPhotoCube() {
		
		//obtenemos el framebuffer
		FrameBuffer fb = obtenerFramebuffer();
		
		// obtenems el manejador de texturas
		TextureManager textureManager = TextureManager.getInstance();

		//lados del cubo con el string
		//de referencia de la textura
		cubeSides = new String[6];

		//removemos la textura si ya existia
		for(int i=0; i<6; i++) {
			if (textureManager.containsTexture("imagen_" + i)) {
				textureManager.removeAndUnload("imagen_" + i, fb);
			}
		}
		
		//obtenemos el theme package actual
		ThemePackage themePackageUsuario = obtenerPaqueteActual();

		//agregamos las imagenes a 
		//las caras del cubo
		for(int i=0; i<6; i++) {
		
			//en base a la cantidad de imagenes
			//disponibles y a la cara siendo
			//recorrida actualmente, calculamos
			//cual imagen corresponde colocar
			int numeroImagenSecuencial = i % obtenerPaqueteActual().cubeImageUris.length;
			
			//configuramos para el lado del cubo
			//actual el id de textura que asociado
			cubeSides[i] = "imagen_" + numeroImagenSecuencial;
			
			if (!textureManager.containsTexture(cubeSides[i])) {
				
				try {
					
					//obtenemos el stream de datos
					Uri path = Uri.parse("android.resource://" + AppContants.packageName + themePackageUsuario.cubeImageUris[i]);
					InputStream in = liveWallpaper.getContentResolver().openInputStream(path);
					
					//obtenemos el bitmap
					Bitmap bitmap = BitmapFactory.decodeStream(in);
					
					//cerramos el flujo
					in.close();
					
					//creamos la textura con la
					//data de la nueva imagen
					Texture imagenTexture = new Texture(bitmap);
					textureManager.addTexture(cubeSides[i], imagenTexture);
					
					//removemos el bitmap de memoria
					bitmap.recycle();
					bitmap = null;


				} 
				catch (FileNotFoundException e) {
				} 
				catch (IOException e) {
				}
				catch (RuntimeException e) {
				}
			}
		}
		
		//crea e inicializa el cubo
		inicializarCubo();
		
	}
	
	/**
	 * Crea e inicializa el cubo
	 */
	private void inicializarCubo() {

		try {
			if(photosCube != null) {
				world.removeObject(photosCube.getID());
			}
		} 
		catch (RuntimeException e) {
		}

		if(cubeSides != null) {
			
			// creamos el cubo de photos
			photosCube = new TexturedFacesCube(12, cubeSides[0], cubeSides[1], cubeSides[2], cubeSides[3], cubeSides[4], cubeSides[5], 0, 0, 0);
		
			//agregamos el cubo al mundo
			world.addObject(photosCube);
		}
	}
	
	@Override
	protected void postInicializacion() {
		
		//recalcula la posicion de la camara
		calcularPosicionCamara();

		//inicializa los cubos que se mueven
		//a los bordes para calcular los limites
		//de la pantalla
		inicializarCubosCalculoBordes();

	}
	
	/**
	 * Calcula la posicion de la camara en base
	 * a la orientacion del dispositivo
	 */
	private void calcularPosicionCamara() {
		
		if(photosCube != null) {
			
			boolean posicionVertical = (GLWallpaperService.width < GLWallpaperService.height) ? true : false;

			world.newCamera();
			Camera cam = world.getCamera();
			
			if(posicionVertical) {
				
				cam.moveCamera(Camera.CAMERA_MOVEOUT, 50);
				cam.lookAt(photosCube.getTransformedCenter());

			}
			else {

				cam.moveCamera(Camera.CAMERA_MOVEOUT, 85);
				cam.lookAt(photosCube.getTransformedCenter());

			}
		}
	}

	@Override
	public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {

		cantidadPantallasVirtuales = (int)((1f/xOffsetStep)+1f);
		xOffsetPantallasVirtuales = xOffset;
	}

	@Override
	public void accelerometerEvent(float msX, float msY, float msZ) {
		
		boolean accelerometerMoved = accelerometerMoved(msX, msY, msZ, 3);
		
		if(accelerometerMoved && !manejandoCubo) {
			
			//movemos rapido por toda la pantalla
			moverCuboBordes();
			
			//iniciamos la rotacion rapida del cubo
			iniciarRotacionRapidaCubo();

		}
	}

	private boolean accelerometerMoved(float msX, float msY, float msZ, int precision) {
		
		float accelationSquareRoot = (msX * msX + msY * msY + msZ * msZ) / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
		
		long actualTime = System.currentTimeMillis();
		if (accelationSquareRoot >= precision) //
		{
			if (actualTime - accelerometerLastUpdate < 200) {
				return false;
			}
			
			accelerometerLastUpdate = actualTime;

			return true;
		}
		
		return false;
	}

	@Override
	public void finalizarRecursos() {
	
		System.out.println("-------------------------------- FINALIZAR RECURSOS");
		
		//sino no se elimina el wallpaper
		//de memoria una vez que es destruido
		liveWallpaper = null;

		//obtenemos el frame buffer
		FrameBuffer fb = obtenerFramebuffer();

		//manejador de texturas
		TextureManager textureManager = TextureManager.getInstance();
		
		//removemos textura y 
		//liberamos memoria
		for(int i=0; i<6; i++) {
			if(textureManager.containsTexture("imagen_" + i)) {
				textureManager.removeAndUnload("imagen_" + i, fb);
			}
		}
		
		if(fb != null) {
			fb.freeMemory();
			fb.clear();
			fb.dispose();
			fb = null;
		}

		if(world != null) {
			world.removeAll();
			world.dispose();
			world = null;
		}
		
		photosCube = null;
		inicializado = false;
		
	}
	@Override
	public void salir() {
		
	}

	private void descargarTexturaFondo() {

		try {
			//removemos el fondo
			TextureManager.getInstance().removeAndUnload("fondoTexture", obtenerFramebuffer());
			fondoTexture = null;
		} 
		catch (RuntimeException e) {
		}
	}
	
	/**
	 * Obtiene el id del paquete de usuario.
	 * En caso de que todavia no exista uno
	 * definido, devuelve el primero disponible
	 * y lo graba en el sistema 
	 * 
	 * @return
	 */
	public int obtenerPaqueteIdUsuario() {
		
		try {
			
			//obtenemos el flujo
			byte[] paqueteIdUsuarioData = FileUtil.obtenerContenidoStream(liveWallpaper.openFileInput("paqueteIdUsuario"));
			
			//transformamos el byte array
			//en el id que contiene el archivo
			int paqueteIdUsuario = Integer.parseInt(new String(paqueteIdUsuarioData));
			
			return paqueteIdUsuario;
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		//si llegamos aqui es porque dio
		//una excepcion tratando de leer
		//el archivo, por ende tenemos que
		//generar uno nuevo con un id predefinido
		int paqueteIdUsuarioPredefinido = ThemePackageReader.getThemePackages(liveWallpaper.getApplicationContext()).get(0).id;
		
		try {
			//guardamos el id
			FileUtil.guardarContenido(liveWallpaper.openFileOutput("paqueteIdUsuario", Context.MODE_PRIVATE), String.valueOf(paqueteIdUsuarioPredefinido).getBytes());
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return paqueteIdUsuarioPredefinido;
	}
	
	/**
	 * Devuelve el paquete actual en uso
	 * @return
	 */
	public ThemePackage obtenerPaqueteActual() {
		
		//obtenemos el id de paquete
		//elegido por el usuario
		int paqueteIdUsuario = obtenerPaqueteIdUsuario();
		
		ThemePackage themePackage = null;
		
		for(ThemePackage themePackageActual : ThemePackageReader.getThemePackages(liveWallpaper.getApplicationContext())) {
			if(themePackageActual.id == paqueteIdUsuario) {
				themePackage = themePackageActual;
				break;
			}
		}
		
		return themePackage;
	}
}