package com.gmi.cube.base.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public class CustomView extends FrameLayout {
	
	private int layout = -1;
	
    public CustomView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CustomView(Context context) {
        super(context);
        initView();
    }
    
    public CustomView(Context context, int layout) {
        super(context);
        this.layout = layout;
        initView();
    }
    
    public int getLayout() {
		return layout;
	}

	public void setLayout(int layout) {
		this.layout = layout;
	}

	private void initView() {
        View view = inflate(getContext(), layout, null);
        addView(view);
    }
}
