package com.gmi.cube.base.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;

import com.gmi.cube.base.ui.view.MainView;
import com.gmi.cube.base.ui.view.ThemesView;

public class MenuPagerAdapter extends PagerAdapter {
	
    //constantes
    public static final int MENU_MAIN = 0;
    public static final int MENU_THEMES = 1;
    
	public List<View> views;
	
	public MenuPagerAdapter(Activity activity, ViewPager pager) {
		views = new ArrayList<View>();
		views.add(MENU_MAIN, new MainView(activity));
		views.add(MENU_THEMES, new ThemesView(activity));
	}
	
	public View obtenerVista(int position) {
		return views.get(position);
	}
	
	public int getCount() {
		return views.size();
	}
	
	public Object instantiateItem(View collection, int position) {
		View view = views.get(position);
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		((ViewPager) collection).addView(view, 0);
		return view;
	}
	
	@Override
	public void destroyItem(View arg0, int arg1, Object view) {
		((ViewPager) arg0).removeView((View) view);
	}
	
	@Override
	public boolean isViewFromObject(View arg0, Object view) {
		return arg0 == ((View) view);
	}
	
	@Override
	public Parcelable saveState() {
		return null;
	}
}