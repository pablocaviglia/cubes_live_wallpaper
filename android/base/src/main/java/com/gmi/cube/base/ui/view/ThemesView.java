package com.gmi.cube.base.ui.view;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.gmi.cube.base.R;
import com.gmi.cube.base.cubethemegenericlw.CubeThemeGenericLiveWallpaperActivity;
import com.gmi.cube.base.cubethemegenericlw.state.CubeThemeGenericLiveWallpaperState;
import com.gmi.cube.base.domain.EstadoBoton;
import com.gmi.cube.base.domain.ThemePackage;
import com.gmi.cube.base.themePackage.ThemePackageReader;
import com.gmi.cube.base.util.AbstractLiveWallpaper;
import com.gmi.cube.base.util.AppContants;
import com.gmi.cube.base.util.FileUtil;
import com.gmi.cube.base.util.MenuPrincipalLazyAdapter;

public class ThemesView extends CustomView {
	
	public ThemesView(final Context context) {
		
		super(context, R.layout.elegir_temas);
		
        ListView list = (ListView)findViewById(R.id.menu_principal_list);
        MenuPrincipalLazyAdapter adapter = new MenuPrincipalLazyAdapter(context);
        list.setAdapter(adapter);
        
        //carga el contenido en el menu
        cargarContenidoListaMenu();
        
		//listener para cuando se presiona 
		//algun elemento de la lista
		list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				//obtenemos la actividad
				CubeThemeGenericLiveWallpaperActivity activity = (CubeThemeGenericLiveWallpaperActivity) getContext();

				if(!activity.popupMostrando()) {
					//obtenemos el id de tema a configurar
					EstadoBoton estadoThemePackage = (EstadoBoton)view.getTag();
					
					int paqueteActual = activity.obtenerPaqueteIdUsuario();
					if(paqueteActual == Integer.parseInt(estadoThemePackage.id)) {
						//mandamos mensaje 
						Toast.makeText(activity, getResources().getString(R.string.theme_already_set), Toast.LENGTH_LONG).show();
					}
					else {
						if(activity.obtenerCantidadUsoThemesDisponibles() > 0) {
							try {
								//guardamos el id de tema elegido
								FileUtil.guardarContenido(context.openFileOutput("paqueteIdUsuario", Context.MODE_PRIVATE), estadoThemePackage.id.getBytes());
								
								//decrementamos la cantidad de usos disponibles de themes
								activity.decrementarCantidadUsoThemesDisponibles();

								//reiniciamos el cubo
								if(AbstractLiveWallpaper.liveWallpaper != null) {
									//mostramos toast
									Toast.makeText(getContext(), getResources().getString(R.string.wallpaper_set), Toast.LENGTH_SHORT).show();
									//obtenemos el estado del servicio
									CubeThemeGenericLiveWallpaperState cubeThemeGenericLiveWallpaperState = (CubeThemeGenericLiveWallpaperState) AbstractLiveWallpaper.liveWallpaper.state;
									//indicamos que un cambio fue hecho
									//y que debemos reinicializar el wallpaper
									cubeThemeGenericLiveWallpaperState.reiniciarEstado();
									activity.moveTaskToBack(true);
								}
								else {
									//mostramos toast
									Toast.makeText(getContext(), getResources().getString(R.string.seleccionarLiveWallpaperDesc), Toast.LENGTH_LONG).show();
									//mostramos actividad para
									//que el usuario elija el
									//live wallpaper
									activity.seleccionarLiveWallpaper();
								}
							} 
							catch (Exception e) {
								e.printStackTrace();
							}
						}
						else {
							activity.mostrarPopupHabilitarThemes();
						}
					}
				}
			}
		});
	}
	
	private void cargarContenidoListaMenu() {
		
		//obtenemos la cantidad de temas
		//disponibles en la coleccion
		int themePackagesSize = ThemePackageReader.getThemePackages(getContext()).size();
		
		//carga la lista con la data
		ArrayList<HashMap<String, Object>> elementsList = new ArrayList<HashMap<String, Object>>();
		for(int i=0; i<themePackagesSize; i++) {
			
			// obtenemos el theme package
			// siend recorrido
			ThemePackage themePackage = ThemePackageReader.getThemePackages(getContext()).get(i);
			Bitmap themeCubeImage = null;

			// como bitmap cargamos la primer
			// imagen que conforma el cubo
			try {
				// obtenemos el stream de datos
				Uri path = Uri.parse("android.resource://" + AppContants.packageName + themePackage.cubeImageUris[0]);
				InputStream themeCubeImageStream = getContext().getContentResolver().openInputStream(path);

				// obtenemos la imagen reducida
				// 8 veces, asi quedan imagenes
				// de 64x64 px
				Options opts = new Options();
				opts.inSampleSize = 8; // 512/8 = 64

				themeCubeImage = BitmapFactory.decodeStream(themeCubeImageStream, null, opts);

			} 
			catch (Exception e) {
				e.printStackTrace();
			}

			// obtenemos el id de recurso del nombre
			// y la descripcion del theme package
			int nameResourceId = getResources().getIdentifier(themePackage.nameId, "string", getContext().getPackageName());
			int descriptionResourceId = getResources().getIdentifier(themePackage.descriptionId, "string", getContext().getPackageName());

			HashMap<String, Object> map = new HashMap<String, Object>();

			map.put("title", getResources().getString(nameResourceId));
			map.put("desc", getResources().getString(descriptionResourceId));
			map.put("thumb_bitmap", themeCubeImage);
			map.put("estado", new EstadoBoton(String.valueOf(themePackage.id), true));
			
			elementsList.add(map);
		}
		
        //obtenemos la lista contenedora del menu
        ListView list = (ListView)findViewById(R.id.menu_principal_list);
        MenuPrincipalLazyAdapter adapter = (MenuPrincipalLazyAdapter)list.getAdapter();
        adapter.setData(elementsList);
        
	}
}