package com.gmi.cube.livewallpaper;

import com.gmi.cube.base.cubethemegenericlw.CubeThemeGenericLiveWallpaperActivity;
import com.gmi.cube.base.util.AppContants;

public class MainActivity extends CubeThemeGenericLiveWallpaperActivity {
	static {
		AppContants.ADMOB_BANNER_UNIT_ID = "ca-app-pub-9022829907836233/9010302901";
		AppContants.ADMOB_INTERSTITIAL_UNIT_ID = "ca-app-pub-9022829907836233/1133782504";
	}
}